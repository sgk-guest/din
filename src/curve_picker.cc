/*
* curve_picker.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "curve_picker.h"
#include "curve_editor.h"
#include "ui_list.h"
#include "input.h"
#include "log.h"
#include "console.h"

#include <vector>
#include <fstream>
using namespace std;

extern ui_list uis;
extern int mousex, mousey, mouseyy;
extern curve_picker_t curve_picker;
extern const char SPC;

extern void make_family (widget* parent, widget* children, int n);

void curve_picker_t::setup () {
  l_title.set_text ("Pick what?");
  widget* chld [] = {&b_pick, &b_cancel, &ol_components};
	make_family (&l_title, chld, 3);
  widget* w [] = {&l_title, &b_pick, &b_cancel, &ol_components};
  widget_load ("d_curve_picker", w, 4);
  b_pick.set_listener (this);
  b_cancel.set_listener (this);
  ol_components.set_listener (this);
}

curve_picker_t::~curve_picker_t () {
  widget* w [] = {&l_title, &b_pick, &b_cancel, &ol_components};
  widget_save ("d_curve_picker", w, 4);
}

void curve_picker_t::update () {
  l_title.update ();
  b_pick.set_text ("Pick");
  b_cancel.set_text ("Cancel");
  ol_components.update ();
}

int curve_picker_t::handle_input () {
  widget* w [] = {&l_title, &b_pick, &b_cancel, &ol_components};
	if (keypressed (SDLK_ESCAPE)) clicked (b_cancel);
	int r = 0;
  for (int i = 0; i < 4;++i) {
		r = w[i]->handle_input ();
		if (r) break;
	}
  return r;
}

void curve_picker_t::draw () {
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f (0, 0, 0, 0.8f);
    glRecti (bg.left, bg.bottom, bg.right, bg.top);
  glDisable (GL_BLEND);
  widget* w [] = {&l_title, &b_pick, &b_cancel, &ol_components};
  for (int i = 0; i < 4;++i) w[i]->draw ();
}

void curve_picker_t::show () {
  widget::show ();
  int cx = l_title.extents.left, cy = l_title.extents.bottom;
  l_title.move (mousex - cx, mouseyy - cy); // move picker to where mouse is 
  id = 0;
	n_1 = uis.crved->hitlist.size () - 1;
	hit_t::name_only = (uis.crved->todo == curve_editor::PICK_CURVE);
  hit_t& top = uis.crved->hitlist[id];
  stringstream ss; ss << SPC << top;
  ol_components.set_text (ss.str());
  calc_bg ();
}

void curve_picker_t::calc_bg () {
  const int delta = 1;
  bg.left = l_title.extents.left - delta;
  bg.top = l_title.extents.top + delta;
  bg.right = max (b_cancel.extents.right, ol_components.option.extents.right) + delta;
  bg.bottom = b_pick.extents.bottom - delta;
	set_focus (this);
}

void curve_picker_t::hide () {
  widget::hide ();
	defocus (this);
}

void curve_picker_t::clicked (button& b) {
  if (&b == &curve_picker.b_pick) uis.crved->picked_using_picker (id); 
  curve_picker.hide ();
}

void curve_picker_t::picked (label& l, int dir) {
  id += dir;
	wrap (0, id, n_1);
  stringstream ss; ss << SPC << uis.crved->hitlist [id];
  l.set_text (ss.str());
  calc_bg ();
}
