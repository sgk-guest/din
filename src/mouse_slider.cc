/*
* mouse_slider.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "mouse_slider.h"
#include "console.h"
#include "viewwin.h"
#include "container.h"
#include "ui_list.h"
#include "log.h"

extern int lmb;
extern int mousex, mousey, mouseyy;
extern void warp_mouse (int x, int y);
extern viewport view;
extern int wheel;

typedef std::list<mouse_slider_listener*>::iterator mouse_slider_listener_iterator;

mouse_slider_listener::mouse_slider_listener () {orient = Y;}

mouse_slider::mouse_slider () {
	initx = inity = inityy = lowx = lowy = midx = midy = highx = highy = prevx = prevy = nmslx = nmsly = lmb_clicked = 0;
	int dirs [] = {arrow_button::left, arrow_button::right, arrow_button::down, arrow_button::up};
	for (int i = 0; i < 4; ++i) {
		arrow_button& abi = ab[i];
		abi.set_color (0.5f, 0.5f, 0.5f);
		abi.set_dir (dirs[i]);
		abi.set_size (22);
	}
	sz = ab[0].size;
	sz_2 = sz / 2;
	szn = sz;
	szn_1 = szn + sz;
}

void mouse_slider::update_x_arrows () {
	arrow_button& abl = ab[0];
	arrow_button& abr = ab[1];
	int dy = mouseyy - sz_2;
	abl.set_pos (mousex - szn_1, dy);
	abr.set_pos (mousex + szn, dy);
}

void mouse_slider::update_y_arrows () {
	arrow_button& abd = ab[2];
	arrow_button& abu = ab[3];
	int dx = mousex - sz_2;
	abd.set_pos (dx, mouseyy - szn_1);
	abu.set_pos (dx, mouseyy + szn);
}

void mouse_slider::draw () {
	glColor3f (0.5, 0.5, 0.5);
	pts[0]=initx; pts[1]=inity;
	pts[2]=mousex; pts[3]=mouseyy;
	glVertexPointer (2, GL_INT, 0, pts);
	glDrawArrays (GL_LINES, 0, 2);
	if (nmslx) {
		ab[0].draw ();
		ab[1].draw ();
	} 
	if (nmsly) {
		ab[2].draw ();
		ab[3].draw ();
	}
}

int mouse_slider::handle_input () {

	if (mousex < lowx || mousex > highx) {
		warp_mouse (midx, mousey);
		initx = prevx = mousex;
	}

	if (mousey < lowy || mousey > highy) {
		warp_mouse (mousex, midy);
		inity = prevy = mouseyy; 
	}

	int dx = 0, dy = 0, ua = 0;
	if (nmslx) {
		dx = mousex - prevx;
		dx += wheel;
		HOVER = 1;
		if (dx != 0) {
			for (mouse_slider_listener_iterator i = mslx.begin (), j = mslx.end (); i != j; ++i) (*i)->moused (dx);
			ua = 1;
		}
	}

	if (nmsly) {
		dy = mouseyy - prevy;
		dy += wheel;
		HOVER = 1;
		if (dy != 0) {
			for (mouse_slider_listener_iterator i = msly.begin (), j = msly.end (); i != j; ++i) (*i)->moused (dy);
			ua =1;
		}
	}

	if (ua) {
		update_x_arrows ();
		update_y_arrows ();
	}

	prevx = mousex;
	prevy = mouseyy;

	if (is_lmb (this)) {
		lmb_clicked = 1;
	} else {
		if (lmb_clicked) {
			deactivate ();
			lmb_clicked = 0;
		}
	}


	return 1;

}

void mouse_slider::add (mouse_slider_listener* msl) {
	if (msl->orient == mouse_slider_listener::X) { push_back (mslx, msl); ++nmslx; } else { push_back (msly, msl); ++nmsly; }
}

void mouse_slider::remove (mouse_slider_listener* msl) {
	if (msl->orient == mouse_slider_listener::X) { erase (mslx, msl); --nmslx; } else { erase (msly, msl); --nmsly; }
	if (nmslx == 0 && nmsly == 0) deactivate ();
}

int mouse_slider::activate () {

	if (nmslx == 0 && nmsly == 0) return active;

	const int margin = 5;
	lowx = margin; highx = view.xmax - lowx;
	lowy = margin; highy = view.ymax - lowy;
	midx = view.xmax / 2;
	midy = view.ymax / 2;
	prevx = mousex;
	prevy = mouseyy;
	initx = mousex;
	inity = mouseyy;
	inityy = mousey;

	string xchange, ychange;
	if (nmslx) {
		for (mouse_slider_listener_iterator i = mslx.begin (), j = mslx.end (); i != j; ++i) {
			mouse_slider_listener* msl = *i;
			xchange = xchange + msl->name + ", ";
		}
		update_x_arrows ();
	}

	if (nmsly) {
		for (mouse_slider_listener_iterator i = msly.begin (), j = msly.end (); i != j; ++i) {
			mouse_slider_listener* msl = *i;
			ychange = ychange + msl->name + ", ";
		}
		update_y_arrows ();
	}

	active = 1; 
	lmb_clicked = 0;
	is_lmb.tie = this;

	cons << GREEN << "Move";
	if (xchange != "") cons << " Left or Right or Wheel to change " << xchange;
	if (ychange != "") cons << " Up or Down or Wheel to change " << ychange;
	cons << "ESC or Click to stop" << eol;

	return active;

}

extern void draw_slit_cutter (int);

void mouse_slider::deactivate () {
	if (nmslx) {for (mouse_slider_listener_iterator i = mslx.begin (), j = mslx.end (); i != j; ++i) { (*i)->after_slide(); }}
	if (nmsly) {for (mouse_slider_listener_iterator i = msly.begin (), j = msly.end (); i != j; ++i) { (*i)->after_slide(); }}
	mslx.clear ();
	msly.clear ();
	nmslx = nmsly = 0;
	erase (uis.widgets_of[uis.current], &mouse_slider0);
	active = 0;
	lmb_clicked = 0;
	is_lmb.clear (this);

	draw_slit_cutter (0);

	warp_mouse (initx, inityy);

	cons << RED << "Stopped mouse slider" << eol;
}

void activate_mouse_slider () {
	if (MENU.show) MENU.toggle ();
	if (mouse_slider0.activate ()) {
		if (uis.is_widget_on_screen (&mouse_slider0, uis.current) == 0) 
			uis.widgets_of [uis.current].push_back (&mouse_slider0);
	}
}

int is_mouse_slider_active () {
	return mouse_slider0.active;
}

void cant_mouse_slide () {
	cons << RED << "Sorry, mouse slider is already active!" << eol;
}

mouse_slider::~mouse_slider () {}
