/*
* trail.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "dingl.h"
#include "trail.h"

void trail_t::alloc (int n) {
  if (tpts) delete[] tpts;
  n_tpts = n;
  tpts = new float [2 * n_tpts];
}

trail_t::trail_t (int tp) {
	total_points = tp;
	num_points = 0;
}

void trail_t::add (float x, float y) {
	if (x != last.x || y != last.y) {
		last.x = x; last.y = y;
		int dt = num_points - total_points;
		if (dt >= 0) {
			int npops = dt + 1;
			for (int i = 0; i < npops; ++i) tq.pop_front ();
			num_points -= npops;
		} 			
		tq.push_back (point<float>(x, y));
		++num_points;
		if (n_tpts < num_points) alloc (2 * num_points);
	}
}

void trail_t::change_size (int delta) {
	total_points += delta;
	if (total_points < 1) total_points = 1;
}

void trail_t::draw () {
	int m = 0, n = 0;	
	for (std::list< point<float> >::iterator iter = tq.begin (), jter = tq.end (); iter != jter; ++iter) {
		point<float>& p = *iter;
		tpts[m++] = p.x; tpts[m++]=p.y;
		++n;
	}
	glVertexPointer (2, GL_FLOAT, 0, tpts);
	glDrawArrays (GL_LINE_STRIP, 0, n);
}
