/*
* authors_note.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "authors_note.h"
#include "console.h"
#include "font.h"
#include "viewwin.h"
using namespace std;

extern void load_instrument ();

authors_note::authors_note () {
  name = "Author's Note";
  buy_now.set_listener (this);
  buy_later.set_listener (this);
  buy_now.set_color (0.5f, 1.0f, 0.5f);
  buy_later.set_color (1.0f, 0.5f, 0.5f);
}

void authors_note::enter () {
  rollup = cons.rollup ();
	cons.rollup (0);
  cons ("show-eval-notice");
}

void authors_note::leave () {
  cons.clear ();
  cons.rollup (rollup);
}

int authors_note::handle_input () {
  int r = buy_now.handle_input ();
  r |= buy_later.handle_input ();
  return r;
}

void authors_note::draw () {

  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glOrtho (0, view.xmax, 0, view.ymax, -1, 1);

  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity ();

  buy_now.draw ();
  buy_later.draw ();

}

void authors_note::setup () {

	extern int line_height;

  buy_now.set_text ("Click here to Buy a License.");
  buy_now.set_pos (20, 5 * line_height);

  buy_later.set_text ("Click here to Evaluate.");
  buy_later.set_pos (20, 3 * line_height);

}

void authors_note::clicked (button& b) {

  if (&b == &buy_now) {
  #if defined (__UNIX_JACK__) || defined (__LINUX_ALSA__)
    #define BUY_COMMAND "buy-license Linux"
  #elif defined __MACOSX_CORE__
    #define BUY_COMMAND "buy-license Darwin"
  #elif defined __WINDOWS_DS__
    #define BUY_COMMAND "buy-license Windows"
  #endif
    cons (BUY_COMMAND);
  } else {
    load_instrument ();
  }
}

