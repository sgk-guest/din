/*
* spiraler.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "spiraler.h"
#include "vector2d.h"
#include "ui_list.h"
#include <vector>
#include <fstream>

using namespace std;

extern const int MILLION;

extern const float PI_BY_180;
extern const float TWO_PI;

spiraler::spiraler () : scr (this, "spiraler.scr", "spiraler_sin.crv", "spiraler_cos.crv", "spiraler_radius.crv", "spiraler_sin.ed", "spiraler_cos.ed", "spiraler_radius.ed") {
  name = "Spiraler";
  center.x = 0.5;
  center.y = 0;
  load_params ();
}

void spiraler::setup () {

  plugin::setup ();

  widget* _ctrls [] = {&sp_radius, &sp_turns, &sp_num_points, &scr};
  for (int i = 0; i < 4; ++i) ctrls.push_back (_ctrls[i]); 
  num_ctrls = ctrls.size ();
  
	sp_num_points.set_text ("Points");
	sp_num_points.set_limits (0, MILLION);
	sp_num_points.set_value (num_points);
	sp_num_points.set_delta (1);
	sp_num_points.set_listener (this);

  sp_turns.set_text ("Turns");
  sp_turns.set_limits (0.0f, MILLION);
  sp_turns.set_value (1.0f);
	sp_turns.set_delta (1.0f);
	sp_turns.set_listener (this);

  sp_radius.set_text ("Radius");
  sp_radius.set_limits (0.0f, MILLION);
  sp_radius.set_delta (0.01f);
  sp_radius.set_value (1.0f);
  sp_radius.set_listener (this);

  widget_load ("d_spiraler", ctrls);

  scr.set_pos (cb_auto_apply.posx, cb_auto_apply.posy);
  scr.setup ();

  render ();


}

spiraler::~spiraler () {
  widget_save ("d_spiraler", ctrls);
  save_params ();
}

void spiraler::load_params () {
  ifstream f (make_fname().c_str(), ios::in);
  string ignore;
  f >> ignore >> radius >> ignore >> turns >> ignore >> num_points;
}

void spiraler::save_params () {
  ofstream f (make_fname().c_str(), ios::out);
  string ignore;
  f << "radius " << radius << endl << "turns " << turns << endl << "num_points " << num_points << endl;
}

void spiraler::render () {

  funktion& f_radius = *scr.pf_radius;
  funktion& f_sin = *scr.pf_sin;
  funktion& f_cos = *scr.pf_cos;

	// get values from sliders
  radius = sp_radius.f_value;
  turns = sp_turns.f_value;

	num_points = sp_num_points.f_value;

  if (radius == 0 || turns == 0 || num_points < 2) return;
  float theta = 0;
  float end_theta = turns * TWO_PI;
  float _1_by_end_theta = 1.0f / end_theta;
  float dtheta = end_theta / num_points;

  float current_radius = 0;
  float fr_theta = 0;
	int j = num_points + 1;
	points.resize (j);
  for (int i = 0; i < j; ++i) {
    current_radius = f_radius (fr_theta * _1_by_end_theta * TWO_PI) * radius;
		point<float>& pi = points[i];
    pi.x = center.x + current_radius * f_cos (theta); 
    pi.y = center.y + current_radius * f_sin (theta);
		theta += dtheta; 
    fr_theta += dtheta;
    while (theta > TWO_PI) theta -= TWO_PI;
  }

  ss.str("");
  ss << "spiral_" << turns << "_" << num_points;
	gen_pts ();

}

void spiraler::sin_cos_radius_edited () {
  render ();
}

void spiraler::sin_cos_radius_optioned () {
  render ();
}
