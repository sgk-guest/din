set set-beat(name) set-beat
set set-beat(short) sbt
set set-beat(purpose) {set beat of component}
set set-beat(invoke) {set-beat OR sbt <list of components> <beat>}
set set-beat(help) {component can be:
gr - gater
am - amplitude modulator
fm - frequency modulator
os - octave shifter
beat is beat}
set set-beat(examples) {set-beat gr 0 ;# beat to 0 for gater
set-beat {fm am} 1 ;# beat to 1 for fm & am
sbt {fm am} 1 ;# ditto but short form
sbt {fm am} {0 1} ;# beat of fm to 0 and beat of am to 1}
