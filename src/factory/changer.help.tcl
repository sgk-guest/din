set changer(name) N/A
set changer(short) N/A
set changer(purpose) {Automagically change the key of a DIN instrument every n seconds. 
	
	scale remains the same.
	
	When you load this patch, by default, the key will shift up a 5th and 
	down a 5th (ie back to starting key) every 5 seconds forever.}

set changer(invoke) {load-patch changer ;# loads this patch

	set variable instrument to set the instrument the key change targets.
  
  set variable changes to the changes you would like to do:
  
  set changes {+5 -5} ;# up a 5th, down a 5th
  
  + means key goes up. - means key goes down.  + or - is compulsory.
  
  set variable duration to the number of seconds after which you want a
  change:
  
  set duration 5 ;#  change key every 5 seconds
  
  After performing all changes, DIN starts again with the first change.}
set changer(help) {}
set changer(examples) {;# default 
	set changes {+5 -5} ;#  up a 5th, then down a 5th
	set duration 5 ;# change once every 5 seconds
	
	;# circle of fifths forever
	set changes +5
	set changes -5
	
	;# up an octave forever
	set changes {+5 +4} ;# up a 5th then a 4th
	
	;# up major 3rd, minor 3rd ; down minor 3rd, major 3rd.
	;# ie up 5th, down 5th 
	
	set changes {+3 +3b -3b -3}}
