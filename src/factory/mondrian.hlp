DIN Is Noise 
Copyright (c) 2006-2017 Jagannathan Sampath <jag@dinisnoise.org>.

---------------
| Mondrian |--|
---------------

Inspired by the work of Dutch artist Piet Mondrian.

Balls bounce in boxes.
Pass thru slits.
Sound on impact.
Pitch from point of impact.
Initial volume from Menu > Parameters > Min Voices
  More voices means less volume per voice and vice-versa.

Horizontal goes from Key note to octave above.
Vertical goes Key note to octave above.

Set Key note and choose notes of the scale from Settings screen.

Boxes:

Create boxes within boxes.

r - split box under cursor into two new vertical boxes
    Menu > Tools > Split box vertically into 2 boxes 

SHIFT + r - split box under cursor into vertical boxes 
            on the notes of the scale. Or Menu > Tools > 
            Split box vertically at notes

CTRL + r -  split box under cursor vertically into N boxes. 
            or Menu > Tools > Split box vertically into N boxes

            Set N from Tools menu.

f - split box under cursor into two new horizontal boxes
    Or Menu > Tools > Split box horizontally into 2 boxes

SHIFT + f - split box under cursor into horizontal boxes 
            on the notes of the scale Or Menu > Tools > 
            Split box horizontally at notes

CTRL + f  - split box under cursor horizontally into N boxes.
            or Menu > Tools > Split box horizontally into N boxes

            Set N from tools menu.

v - delete box under cursor or Menu > Tools > Delete box

SHIFT + v - delete all boxes in Mondrian
            or Menu > Tools > Delete all boxes

t - make a grid of boxes on the notes of the scale
    or Menu > Tools > Make note grid

SHIFT + t - make an N x N grid of boxes. 
            or Menu > Tools > Make N x N grid

            Set N from tools menu.

Click on a box's edge and just move mouse to move the edge. 
Click again or ESC to stop.

Slits:

  Slits let balls of a box pass thru its walls or ceilings into
  other boxes.

  You can make or close slits or let wrecking and healing balls 
  [see below] make and close them automatically.

  You can also animate slits ie open and close them automatically

  / - Click on an edge of a box to slit. If a slit exists already, 
      it will close.

  Cannot slit if balls will escape the entire playing area.
  Cannot slit corners.

  NUM_PAD + - Increase default slit size
  NUM_PAD - - Decrease default slit size
  Or Menu > Parameters > Change slit size

  To select slits, first change the selection target from balls 
  to slits and then you can use the same shortcuts you use for 
  selecting balls.

  g - change selection target from balls to slits and back.

  When slits are the selection target,

  l - select all slits
  i - invert slit selection
  k - select slits of box under cursor
        hold SHIFT or CTRL to toggle selection
  n - clear slit selection

  c - delete selected slits

  h - toggle slit animation

  INSERT: decrease slit open+close time of selected slits
  DELETE: increase slit open+close time of selected slits

  Or from Menu > Tools or Menu > Parameters.

  To edit an existing slit, edit one of its two lips:
  Click once on a lip, just move mouse to move the lip and click 
  again to stop.

  Also,

  F9  - remove all slits on the edge under cursor
  F10 - remove all slits in the box under cursor
	F11 - remove all slits in the box with a [selected] ball
  F12 - remove all slits

Balls:

b - to launch bouncers or Menu > Tools > Add bouncers
SHIFT + b - to launch wreckers or Menu > Tools > Add wreckers
CTRL + b - to launch healers or Menu > Tools > Add healers

Click, drag and release to launch with an initial velocity

The shortcuts below affect selected balls, balls in box under cursor 
or if cursor is outside all boxes, all the balls in Mondrian. 

[ - decrease speed
] - increase speed

; - decrease attack time
' - increase attack time
SHIFT + ; - decrease attack time increment
SHIFT + ' - increase attack time increment

, - decrease decay time
. - increase decay time
SHIFT + , - decrease decay time increment
SHIFT + . - increase decay time increment

o - rotate direction anti-clockwise
p - rotate direction clockwise
SHIFT + o - decrease angle of rotation
SHIFT + p - increase angle of rotation
CTRL + o - toggle auto rotate direction anti-clockwise
CTRL + p - toggle auto rotate direction clockwise

j - flip direction of travel

SPACE - freeze or thaw  balls
SHIFT + SPACE - freeze balls
CTRL + SPACE - thaw balls

- - decrease trail length
= - increase trail length

Ball types:

  A wrecking ball slits walls and ceilings. Colored red.

  A healing ball closes slits. Colored blue.

  A bouncing ball bounces when its hits wall or ceiling.
  Coloring depends on modulation on the ball. See below.

  F3  - turns selected balls into wrecking balls
        or wrecking balls into bouncing balls.
  
  F4  - turns selected balls into healing balls
        or healing balls into bouncing balls.
  
  F5 -  turns selected balls into bouncing balls.

  F6 -  change all wrecking balls into healing balls
        and vice-versa.
  
  F7 -  select all wrecking balls.

  F8 -  select all healing balls.

Ball selection:

Click once and just move mouse to draw a box to select 
some balls. Click again to stop.

Hold down SHIFT while boxing to add to existing selection
Hold down CTRL while boxing to toggle existing selection

l - select all balls in all boxes
i - invert current selection
k - select all balls in box under cursor
n - clear selected balls

to browse selected balls,

LEFT_ARROW - select previous ball
RIGHT_ARROW - select next ball

Ball manipulation:

c - delete selected balls

m - move selected balls
      just move mouse to move balls, click or ESC or m to stop.

Miscellaneous:

1 - switch instrument or Menu > Instrument
2 - edit waveform 
3 - edit attack curve
4 - edit decay curve

z - shift instrument or selected balls an octave down
x - shift instrument or selected balls an octave up
      balls are gray if they trigger notes without modulation (default)
      balls are black if they trigger notes shifted down
      balls are white if they trigger notes shifted up

y - decrease number of voices
u - increase number of voices

Visual:

Mondrian draws a regular polygon when & where it triggers a note.

9 - decrease points in note polygon
0 - increase points in note polygon
SHIFT + 9 - decrease radius of note polygon
SHIFT + 0 - increase radius of note polygon

F2  - Turn the UI ON or OFF
See Menu > Misc for more visual options.

w, a, s, d - move viewpoint

q, e - zoom in and out

Recording:

CTRL + ENTER - to start/stop sound recording
or Click on the Record button at the bottom of the microtonal-keyboard

ALT + ENTER - clear existing recording
or choose Menu > File > Clear

Menu > File > Save to save the recording as a .WAV file on the desktop.

Console:

TAB : enter/leave command mode
UP_ARROW / DOWN_ARROW: scroll up/down
SHIFT + UP_ARROW / DOWN_ARROW: scroll up/down command history

In command mode, type help to reveal available commands. Or type
help command_name to get help for a particular command.

PgUp, PgDn: page up, page down
BACKSPACE : delete all text in console
` : fold/unfold console
