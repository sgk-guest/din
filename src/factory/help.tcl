if 0 {
  /*
    * This file is part of din.
    *
    * din is copyright (c) 2006 - 2017 S Jagannathan <jag@dinisnoise.org>
    * For more information, please visit http://dinisnoise.org
  */
  command help display
}

proc help {{cmd ""}} {

  global color

  set-var fold_console 0

  global user
  if {[file exists $user/$cmd.help.tcl] == 0} {
    if {$cmd ne ""} {
      eval $color(error)
      echo "$cmd: not found."
    }
    eval $color(header)
    echo "Available commands are:"
      eval $color(text)
      nago [lsort [lsuserdir help.tcl]] 4
    eval $color(header)
    echo "Type help <command_name> for help on a command."
    return
  }

  source $user/$cmd.help.tcl
  set headers {NAME: {SHORT NAME:} PURPOSE: {HOW TO USE:} NOTES: EXAMPLES:}
  set keys {name short purpose invoke help examples}
  for {set i 0; set j [llength $keys]} {$i < $j} {incr i} {
    set text [lindex [array get $cmd [lindex $keys $i]] 1]
    if {[llength $text] > 0} {
      eval $color(header)
      echo [lindex $headers $i]
      eval $color(text)
      foreach l [split $text \n] {echo $l}
    }
  }

}

proc nago {target n} {
  set j 0
  foreach i [lrange $target 0 end-1] {
    if {$j < $n} {incr j} else {
      echo $out
      unset out
      set j 0
    }
    append out " $i, ";
  }

  append out "and [lindex $target end]"
  echo $out

}
