set set-curve-editor(name) set-curve-editor
set set-curve-editor(short) sced
set set-curve-editor(purpose) {assign a curve editor to a screen}
set set-curve-editor(invoke) {set-curve-editor OR sced <name> <screen>}
set set-curve-editor(help) {8 screens are available. Each screen can have 1 editor.
name can be waveform, modulation, gater, delay,
octave-shift, compressor, drone & morse-code.
screen 0 is the instrument. Not changeable.}
set set-curve-editor(examples) {set-curve-editor modulation 1 ;# press 1 to switch to modulation editor
set-curve-editor waveform 7 ;# press 7 to switch to waveform editor}
