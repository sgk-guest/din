set write-svg(name) {write-svg}
set write-svg(short) {write-svg}
set write-svg(purpose) {write current curve on editor to disk as SVG file}
set write-svg(invoke) {write-svg <height> <stroke-thickness> <filename>}
set write-svg(help) {}
set write-svg(examples) {}
