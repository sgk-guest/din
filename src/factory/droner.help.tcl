set droner(name) N/A
set droner(short) N/A
set droner(purpose) {assign drones to midi sliders and fade them in & out}
set droner(invoke) {load-patch droner}
set droner(help) {select a bunch of drones and assign to midi knob or slider with command:
  assign-drones OR asd <id>
  id is the slider/knob id

  keep the slider/knobs at max (ie 127) when making assignment
  so you can fade them out and back in smoothly instead of jumping to a value

  command assign-drones without any arguments prints the current assignments}
set droner(examples) {
  assign-drones ;# print current assignments
  asd 1 ;# assign selected drones to midi slider/knob with id = 1}
