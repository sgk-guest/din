set get-var(name) get-var
set get-var(short) gv
set get-var(purpose) {get value of variables in din}
set get-var(invoke) {get-var <variable1> <variable2> ... <variableN>}
set get-var(help) {prints the value of requested variables. available variables are:
voice_volume or wv -- lead voice volume
note_volume or nv -- note volume on keyboard-keyboard
attack_time or at -- attack time in seconds for a note on keyboard-keyboard
decay_time or dt -- decay time in seconds for a note on keyboard-keyboard
sustain or su -- position on attack curve where note starts to sustain
drone_master_volume or dmv -- drone master volume
fm_depth or fmd -- FM depth
am_depth or amd -- AM depth
num_sine_samples or nss -- number of samples used when converting sine waveform to bezier waveform
drone_handle_size or dhs -- handle size of drones used for display/selection
snap_drones or sd -- drones snapped to notes? 1 = yes or 0 = no
scroll or sc -- scroll rate, amount in x and amount in y for wsad key press
zoom -- zoom of all curve editors. returns rate and amount
pan -- pan on all curve editors. returns rate and amount
show_pitch_volume or spv -- show pitch/volume under mouse cursor on microtonal-keyboard? 1 = yes, 0 = no
fps -- number of times din refreshes the ui every second
usleep -- number of microseconds din sleeps to allow processor do other tasks
scale -- name of current scale
tuning or tu -- name of current tuning}
set get-var(examples) {get-var scroll ;# print scroll parameters of din board
get-var fmd amd ;# get both fm_depth & am_depth
gv fmd amd ;# short form
get-var fmd amd tuning ;# returns 2 numbers (fmd & amd) & 1 string (name of tuning)}
