
set f [open $user/settings.tcl w]
set set_cmd {set-bpm set-style set-var set-delay}
set get_cmd {get-bpm get-style get-var get-delay}
set runs { {fm am gr os} {fm am gr} {am_depth fm_depth} {left right}}
set ncmds [llength $set_cmd]
for {set i 0} {$i < $ncmds} {incr i} {
  set run [lindex $runs $i]
  foreach j $run {
    set setj [lindex $set_cmd $i]
    set getj [lindex $get_cmd $i]
    set str "$setj $j [$getj $j]"
    puts $f $str
  }
}

foreach i {
	{"set-var location" "format {{%s}} [get-var location]"}
	{notation "get-var notation"}
  {"tuning set" "tuning get"}
  {"load-scale microtonal_keyboard" "get-var microtonal_keyboard.scale"} 
	{"load-scale keyboard_keyboard" "get-var keyboard_keyboard.scale"}
	{"load-scale mondrian" "get-var mondrian.scale"}
	{"load-scale binaural_drones" "get-var binaural_drones.scale"}
	{"key microtonal_keyboard" "key microtonal_keyboard value"} 
	{"key keyboard_keyboard" "key keyboard_keyboard value"}
	{"key mondrian" "key mondrian value"}
	{"key binaural_drones" "key binaural_drones value"}
  {"set taptarget" "format {{%s}} [set taptarget]"}
  {"set-var scroll" "get-var scroll"}
  {"set-var zoom" "get-var zoom"}
  {"set-var pan" "get-var pan"}
  {"set-var fps" "get-var fps"}
	{"set-var ips" "get-var ips"}
  {"set-var drone_handle_size" "get-var drone_handle_size"}
  {"set-var trail_length" "get-var trail_length"}
  {"set-var voice_volume" "get-var voice_volume"}
  {"set-var sustain" "get-var sustain"}
  {"set-var instrument" "get-var instrument"}
	{"set-var midi_in" "get-var midi_in"}
  {"set-scope left" "format {{%s}} [get-scope left]"}
  {"set-scope right" "format {{%s}} [get-scope right]"}
  {"set-var show_parameters" "get-var show_parameters"}
  {"set-var delta_drone_master_volume" "get-var delta_drone_master_volume"}
  {set-kb-layout set-kb-layout}
  {"set-var fft" "get-var fft"}
  {"set-var current_plugin" "get-var current_plugin"}
  {"set-var fold_plugins" "get-var fold_plugins"}
  {"set-var mondrian.delta_speed" "get-var mondrian.delta_speed"}
	{"set-var mondrian.delta_rotate_velocity" "get-var mondrian.delta_rotate_velocity"}
  {"set-var mondrian.delta_attack_time" "get-var mondrian.delta_attack_time"}
  {"set-var mondrian.delta_decay_time" "get-var mondrian.delta_decay_time"}
	{"set-var mixing_time" "get-var mixing_time"}
	{"set-var binaural_drones.master_volume" "get-var binaural_drones.master_volume"}
	{"set-var binaural_drones.justification" "get-var binaural_drones.justification"}
  } {
  set setc [lindex $i 0]
  set getc [lindex $i 1]
  puts $f "$setc [eval $getc]"
}

foreach i {microtonal_keyboard_editors keyboard_keyboard_editors mondrian_editors binaural_drones_editors} {
  puts $f "array set $i {[array get $i]}"
}

close $f
