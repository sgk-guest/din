set get-style(name) get-style
set get-style(short) gs
set get-style(purpose) {get style of beat progression}
set get-style(invoke) {get-style OR gs <list of components>}
set get-style(help) {component can be:
gr - gater
am - amplitude modulator
fm - frequency modulator
os - octave shifter}
set get-style(examples) {get-style gr ;# get style the gater
pong ;# sample output from get-style

gs gr ;# ditto but short form
get-style [list-bpms];# get style of all available components
get-style {fm am} ;# get style of fm & am}
