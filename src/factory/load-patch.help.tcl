set load-patch(name) load-patch
set load-patch(short) lop
set load-patch(purpose) {load a patch in din}
set load-patch(invoke) {load-patch <patch_name>}
set load-patch(help) {}
set load-patch(examples) {list-patches ;# lists all available patches on 1 line
load-patch midimap ;# loads patch midimap}
