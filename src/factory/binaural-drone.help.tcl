set binaural-drone(name) binaural-drone
set binaural-drone(short) bd
set binaural-drone(purpose) {to edit binaural drone pairs}
set binaural-drone(invoke) {binaural-drone <cmd> <args>}
set binaural-drone(help) {
	// FOR INTERNAL / DEVELOPER USE ONLY
	// PLEASE USE GUI TO CREATE AND EDIT BINAURAL DRONES
	binaural-drone create <l_hz> <r_hz> <vol> <justification> <separation>
	binaural-drone edit <id> [volume | left | right | both | separation | modulate] <value>
	binaural-drone delete <id>
	binaural-drone sync <ids>
	binaural-drone update-list
	binaural-drone n
	binaural-drone get [left | right | separation | volume | selected | selected_volumes]
	binaural-drone list
}
set binaural-drone(examples) {see factory/binaural-drones.tcl}
