set set-midi(name) set-midi
set set-midi(short) sm
set set-midi(purpose) {set the midi input port to receive MIDI input from}
set set-midi(invoke) {
  set-midi <port>}
set set-midi(help) {port number can be found using list-midi.
    This command is only available on Mac OS X.} 
set set-midi(examples) {set-midi 2 ;# accept MIDI input from MIDI input port number 2}
  
