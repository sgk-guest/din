set key(name) key
set key(short) key
set key(purpose) {set the key of the instrument}
set key(invoke) {key
key instrument note_name
key instrument note_name octave_shift
key instrument note
key instrument value}
set key(examples) {key ;# prints key value in Hz, nearest note name & distance in Hz from that note
key microtonal_keyboard C; # set key of the microtonal keyboard to note middle C
key mondrian C -1; # set key of Mondrian to note C an octave below middle C
key keyboard_keyboard C 2; # set key of Keyboard-Keyboard to note C 2 octaves above middle C
key mondrian 440; # set key of Mondrian to 440 Hz which is the note A
key mondrian note; # prints note name nearest to the key of Mondrian
key microtonal_keyboard value; # prints the key frequency in Hz of Microtonal Keyboard}
