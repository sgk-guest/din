set get-bpm(name) get-bpm
set get-bpm(short) gb
set get-bpm(purpose) {get the bpm of a component}
set get-bpm(invoke) {get-bpm OR gb <list of components>}
set get-bpm(help) {component can be:
  gr - gater
  fm - frequency modulator
  am - amplitude modulator
  os - octave shifter}
set get-bpm(examples) {get-bpm fm ;# get bpm of frequency modulator
  get-bpm gr ;# get bpm of gater
  gb am ;# get bpm of amplitude modulator}
