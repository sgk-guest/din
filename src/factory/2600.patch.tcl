if {0} {

  /*
    * This file is part of din.
    *
    * din is copyright (c) 2006 - 2013 S Jagannathan <jag@dinisnoise.org>
    * For more information, please visit http://dinisnoise.org
    *
    *
  */

  turn a list of very offensive English language words
  picked using 4 MIDI sliders/knobs into morse code
  and apply as gater patterns

}

set ::redact 0 ;# print fuck as f*ck if ::redact is 1

proc read-word-list {} {
  ;# read banned words list compiled by 2600 magazine: http://www.2600.com/googleblacklist/
  global wordlist
  set wordlist {}
  set f [open user/2600 r]
  while {[gets $f line] >= 0} { lappend wordlist $line }
  close $f
}

proc midi-cc {status cc value} {

  global wordlist id redact

  set sliders {1 2 3 4} ;# slider ids. change these values to match ids of your midi slider/knobs
  set j 0
  foreach i $sliders {
    if {$cc eq $i} { # matched 1 of the (4) banks of words

      ;# find word
      set step [expr $j * 127]
      set id [expr $step+$value]
      set word [lindex $wordlist $id]

      if {$word ne ""} {
        ;# print word in pretty random text color
        set-text-color [expr rand()] [expr rand()] [expr rand()]
        if {$redact eq 1} {
          ;# redact is 1 so simply replace the vowels with a special char
          set word [string map "a * e # i & o % u !" $word]
          echo $word
        } else {
          echo $word
        }
        do-morse-code $word ;# word -> morse code -> gaters
      }
      return
    }
    incr j
  }

}

proc do-morse-code {str} {
  morse-code [string toupper $str] ;# create morse code bezier curves
  paste-gater ;# paste the curves into gater
}

read-word-list
