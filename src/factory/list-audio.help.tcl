set list-audio(name) list-audio
set list-audio(short) la
set list-audio(purpose) {lists the audio devices connected to this system.}
set list-audio(invoke) {list-audio} 
set list-audio(help) {list format is:
        device_number company:device_name @ list_of_sample_rates
        use the device number to set audio output device using command set-audio
        
        This command is only available on Mac OS X.}
        
set list-audio(examples) {list-audio}
