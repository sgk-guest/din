DIN Is Noise Copyright (c) 2006-2019 Jagannathan Sampath <jag@dinisnoise.org>.

Binaural Drones

Launch with Menu > Instrument > Binaural drones.

A binaural drone pair is a pair of drones, one drone goes into your left
ear (the L drone) and the other drone goes into your right ear (the R
drone).  So, you need to hear this on headphones :) The L drone and the R
drone each vibrate at their own pitch (in Hz).  You hear the difference
between the pitches (the Separation) as a beat at that pitch. eg., if the
difference is 1 Hz, you hear a beat every second.  These beats are called
binaural beats.

DIN can make binaural drone pairs starting at a pitch (default @ 100 Hz)
with any separation between the drones of a pair, default @ 1 Hz.

DIN can also make binaural drone pairs at the notes of the scale starting
at the key note. The key note can be the starting pitch or the pitch or
note you set on the Settings page. You can also set the other notes of the
scale there.

Turn on Close Octave to create a binaural pair on the octave note too.

Turn on Resize separation to vary the separation between L and R drones of
each pair so that their absolute pitch always falls on the notes of the
scale.

Justification determines the drone that gets the starting pitch. Left sets
it to the L drone, Right sets it to the R drone. eg., for a Starting pitch
100 Hz and Separation 1 Hz and Justification = Left, L drone is @ 100 Hz, R
drone is @ 101 Hz. If Justification = Right, R drone is @ 100 Hz, L drone
is @ 99 Hz. If Justification = Center, the L drone is @ 99.5 Hz and R drone
is @ 100.5 Hz.

Spacing (in Hz) determines the spacing between two successive binaural
drone pairs, default @ 50 Hz.

Press 2 or Menu > Editors > Waveform to edit the waveform that drives all
binaural drone pairs.

Menu > Edit to edit the binaural drones.

Master volume in % determines the overall loudness of all binaural drone
pairs.  100% is loudest. 0% is silence. > 100% is valid too.  Careful :) 

Volume fade time in seconds is the time taken to fade in or out when
creating, destroying, syncing, changing the volume of a drone pair or when
changing the master volume.  

Pitch fade time in seconds is the time taken to change the L drone or the R
drone of any binaural drone pair from one pitch to another.  You can see
this when you modulate, flip or change L, R pitches or separation of
selected drone pairs.

When fading pitch or volume, you can press ESC to abort.  Useful when the
fade times are long, also useful when you have unexpectedly reached a more
interesting sound...:)

DIN lists the binaural drone pairs in a list.  Click an element to select
or deselect, hold SHIFT button down and click to select a group, and clear,
invert selection. You can also select based on L, R, Separation Hz or
Volume: Select L, R, Separation or Volume from the options, choose the
operator:  >=, <=, =, <> (the range operator) or id (id numbers of drone
pairs listed between the []), enter the value and press ENTER or click on
Select to execute. DIN selects the matching pairs.  The range operator
takes two values, the low and high.  The id operator takes 3 values
(3rd value is optional and is 1 when not present), the start id, the end id
and the increment. 

You can edit the pitch of the L drone, the R drone, Separation,
Justification and the Volume % of a selected pair in the fields to the
right.  When you select more than one pair, the L and R pitch fields accept
relative instead of absolute values ie you change the L and R Hz rather
than set it.

Flip to swap the pitches of selected drone pairs.  ESC to abort.

Modulate [actually Multiply] the pitches of L and R drone of selected pairs
by an Amount. Press LEFT_ARROW to modulate down, RIGHT_ARROW to modulate
up. ESC to abort. With default Amount @ 2.0, you octave shift when you
modulate the selected pairs :) You can modulate up or down an interval
using the value of DIN's interval variables.  eg., $1, $2b, $2, $3b $3, $4,
$5b, $5, $6b, $6, $7b, $7 or $8: eg., type $2b, press ENTER and click
LEFT_ARROW or RIGHT_ARROW to modulate down or up by a semitone :) $1 is 1.0
and $8 is 2.0 which is the octave shift again :)   

Delete deletes the selected binaural drone pairs. DIN first fades out the
volume of the pairs and then deletes them.  ESC to abort delete when
fading out.

Sync syncs ie aligns the phase of the selected binaural drone pairs.  DIN 
first fades out the pairs, aligns their phase in silence and then restores 
volume.  ESC to abort sync on fade out or in.

Recommend keep the Compressor and Delays off.

Recording:

CTRL + ENTER - to start/stop sound recording or Click on the Record button
at the bottom of the microtonal-keyboard

ALT + ENTER - clear existing recording or choose Menu > File > Clear

Menu > File > Save to save the recording as a .WAV file on the desktop.

Console:

TAB : enter/leave command mode UP_ARROW / DOWN_ARROW: scroll up/down SHIFT
+ UP_ARROW / DOWN_ARROW: scroll up/down command history

In command mode, type help to reveal available commands. Or type help
command_name to get help for a particular command.

PgUp, PgDn: page up, page down BACKSPACE : delete all text in console ` :
fold/unfold console
