if {0} {

  /*
    * This file is part of din.
    *
    * din is copyright (c) 2006 - 2015 S Jagannathan <jag@dinisnoise.org>
    * For more information, please visit http://dinisnoise.org
    *
    *
  */

}

set start $timenow

;# defaults
set instrument . ;# change key of the keyboard-keyboard
set duration 5 ;# change every 5 seconds
set changes {+5 -5} ;# change key up a 5th then down a 5th ie back to tonic
set index -1

proc loop {} { ;# called once per audio loop

  global timenow start
  global changes duration
  global index
	global instrument

  if {($timenow - $start) > $duration} { ;# time to change

    set len [llength $changes]

    incr index
    if {$index >= $len} {
      set index 0
    }

    set change [lindex $changes $index] ;# pick the index'th change
    set dir [string index $change 0] ;# what is the direction? + is go up, - is go down
    set amount [string range $change 1 end] ;# amount to change - this is interval variable defined in din

    global $amount ;# interval variables are defined in global namespace

    if {$dir eq "+"} { ;# change up
      key $instrument [expr [key $instrument value] * [set $amount]] ;# changed key
    } elseif {$dir eq "-"} { ;# change down
      set val [set $amount]
      if {$val != 0} { ;# to avoid / by 0
        key $instrument [expr [key $instrument value] / [set $amount]] ;# changed key
      }
    }
    set start $timenow ;# prep for next change
    echo "changed key of $instrument by $dir$amount to reach [key $instrument value] hz / [key $instrument note]" ;# inform key change to user
  }
}
