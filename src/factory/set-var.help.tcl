set set-var(name) set-var
set set-var(short) sv
set set-var(purpose) {set value of variables in din}
set set-var(invoke) {set-var <variable1> <value1> <variable2> <value2>... <variableN> <valueN>}
set set-var(help) {sets the value of variables. available variables are:
location or loc - change displayed location on DIN Is Noise window titlebar
fps - frames per second, DIN tries to update UI fps times every second
ips - inputs per second, DIN reads keyboard/mouse ips times every second
scroll or sc -- scroll rate, amount in x and amount in y for wsad key press
zoom -- zoom of all curve editors. set rate and amount
pan -- pan on all curve editors. set rate and amount
num_sine_samples or nss -- number of samples used when converting sine waveform 
                           to bezier waveform
show_pitch_volume or spv -- show pitch volume on microtonal keyboard
drone_handle_size or dhs -- size of drone handles on microtonal keyboard
trail_length or tl -- length of drone trails on microtonal keyboard
voice_volume or vv -- voice volume on microtonal-keyboard
note_volume or nv -- note volume on keyboard-keyboard
attack_time or at -- attack time in seconds for a note on keyboard-keyboard
decay_time or dt -- decay time in seconds for a note on keyboard-keyboard
sustain or su -- position on attack curve where note starts to sustain on keyboard-keyboard
drone_master_volume or dmv -- drone master volume for drones on microtonal-keyboard 
fm_depth or fmd -- FM depth for voice on microtonal-keyboard
am_depth or amd -- AM depth for voice on microtonal-keyboard
drone_handle_size or dhs -- handle size of drones used for display/selection}
set set-var(examples) {
set-var fps 120; # try to update DIN UI 120 times every second
set-var ips 120; # try to read keyboard/mouse 120 times every second
# wasd scroll 25 units in x & 3 units in y upto 100 times a second
set-var scroll rate 100 x 25 y 3
set-var zoom rate 120 amount 0.03 ;# zoom 120 times a second at 0.03 units per zoom in all curve editors
set-var pan rate 120 amount 0.03;# pan 120 times a second at 0.03 units per pan in all curve editors
set-var num_sine_samples 100 ;# number of samples for converting sine waveform -> bezier waveform
set-var show_pitch_volume 1; # show pitch volume info on microtonal keyboard
set-var drone_handle_size 14 ;# twice the size of default drone handles which is 7
set-var trail_length 100; # long drone trails
set-var voice_volume 0.1 ;# volume of voice on microtonal-keyboard
set-var attack_time 1 ;# note on keyboard-keyboard attacks in 1 second
set-var decay_time 10 ;# note on keyboard-keyboard decays in 10 seconds
set-var note_volume [expr 0.75 * [get-var voice_volume]] ;# max note volume is 75% of voice
set-var sustain 1.0 ;# position on attack curve where sustain begins ie at 1.0 (default)
set-var dmv 0 ;# silence all drones
set-var drone_master_volume 0.9 ;# very loud drones. careful.
set-var amd 0 ;# set am_depth of voice to 0
set-var fmd 0 ;# set fm_depth of voice to 0
set-var am_depth -0.5 fm_depth 100 ;# set am_depth to -0.5 and fm_depth of voice to 100

More examples, see file settings.tcl in factory or user directory}
