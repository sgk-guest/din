/*
* minus_button.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "minus_button.h"

void minus_button::draw () {
	const color& c = clr;
	const box<int>& e = extents;
	glColor3f (c.r, c.g, c.b);
  int pts [4]={0};
  glVertexPointer (2, GL_INT, 0, pts);
  pts[0]=e.left;pts[1]=e.midy;pts[2]=e.right;pts[3]=e.midy;
  glDrawArrays (GL_LINES, 0, 2);
  pts[0]=e.right;pts[1]=e.midy;
  glDrawArrays (GL_LINES, 0, 1);
}






