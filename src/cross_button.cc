/*
* cross_button.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "cross_button.h"

void cross_button::draw () {
	box<int>& e = extents;
	glColor3f (clr.r, clr.g, clr.b);
	{
		int pts[8] = {e.left, e.bottom, e.right, e.top, e.left, e.top, e.right, e.bottom};
		glVertexPointer (2, GL_INT, 0, pts);
		glDrawArrays (GL_LINES, 0, 4);
	}
	{
		int pts[4]= {e.right, e.top, e.right, e.bottom};
		glVertexPointer (2, GL_INT, 0, pts);
		glDrawArrays (GL_POINTS, 0, 2);
	}
}
