/*
* rose_milker.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "rose_milker.h"
#include "vector2d.h"
#include "ui_list.h"
#include "console.h"
#include <vector>
#include <fstream>
using namespace std;

extern ofstream dlog;

extern const int MILLION;

extern const float TWO_PI;
extern const float PI_BY_180;

extern curve_library sin_lib, cos_lib;

rose_milker::rose_milker () : scr (this, "rose_milker.scr", "rose_milker_sin.crv", "rose_milker_cos.crv", "rose_milker_radius.crv", "rose_milker_sin.ed", "rose_milker_cos.ed", "rose_milker_radius.ed", 0) {
  name = "Rose_Milker";
  center.x = 0.5;
  center.y = 0;
  load_params (); 
}

void rose_milker::load_params () {
	ifstream f (make_fname().c_str(), ios::in);
	string ignore;
	f >> ignore >> N >> ignore >> D >> ignore >> num_points >> ignore >> end_angle;
}

void rose_milker::save_params () {
	ofstream f (make_fname().c_str(), ios::out);
	string ignore;
	f << "N " << N << endl << "D " << D << endl << "num_points " << num_points << endl << "end_angle " << end_angle << endl;
}

rose_milker::~rose_milker () {
	widget_save ("d_rose_milker", ctrls);
	save_params ();
}

void rose_milker::setup () {

	plugin::setup ();

	widget* _ctrls [] = {&sp_n, &sp_d, &sp_num_points, &sp_end_angle, &scr};
	for (int i = 0; i < 5; ++i) ctrls.push_back (_ctrls[i]);
	num_ctrls = ctrls.size ();

	spinner<int>* spn [] = {&sp_n, &sp_d, &sp_num_points};
	const char* lbl [] = {"N", "D", "Points"};
	int val [] = {N, D, num_points};
	for (int i = 0; i < 3; ++i) {
		spinner<int>* spi = spn[i];
		spi->set_text (lbl[i]);
		spi->set_limits (0, MILLION);
		spi->set_delta (1);
		spi->set_listener (this);
		spi->set_value (val[i]);
	}
	sp_end_angle.set_text ("End angle");
	sp_end_angle.set_limits (0, MILLION);
	sp_end_angle.set_value (end_angle);
	sp_end_angle.set_delta (1.0f);
	sp_end_angle.set_listener (this);

	widget_load ("d_rose_milker", ctrls);

	scr.setup ();
	scr.set_pos (cb_auto_apply.posx, cb_auto_apply.posy);
	sin_cos_radius_optioned ();
	render ();

}

void rose_milker::render () {

	funktion& f_sin = *scr.pf_sin;
	funktion& f_cos = *scr.pf_cos;

	N = sp_n.f_value; 
	D = sp_d.f_value;
	if (D == 0) return;
	K = N * 1. / D;

	num_points = sp_num_points.f_value;
	if (num_points == 0) return;

	end_angle = sp_end_angle.f_value;
	float dtheta = end_angle / num_points * PI_BY_180;
	float theta = 0, theta_k = 0;
	int j = num_points + 1; 
	points.resize (j);
	float R;
	for (int i = 0; i < j; ++i) {
		float ktheta = K * theta_k;
		while (ktheta > TWO_PI) ktheta -= TWO_PI;
		R = f_sin (ktheta);
		point<float>& pi = points[i];
		pi.x = center.x + R * f_cos (theta);
		pi.y = center.y + R * f_sin (theta);
		theta += dtheta;
		theta_k += dtheta;
		while (theta > TWO_PI) theta -= TWO_PI;
	}

	ss.str("");
	ss << "rose_" << N << "by" << D << "_" << (int)end_angle;

	gen_pts ();

}

void rose_milker::sin_cos_radius_optioned () {
  render ();
}

void rose_milker::sin_cos_radius_edited () {
  render ();
}
