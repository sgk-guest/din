/*
* plus_button.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "plus_button.h"

void plus_button::draw () {
	const color& c = clr;
	const box<int>& e = extents;
	glColor3f (c.r, c.g, c.b);
  int pts[8]={0};
  pts[0]=e.left;pts[1]=e.midy;
  pts[2]=e.right;pts[3]=e.midy;
  pts[4]=e.midx;pts[5]=e.bottom;
  pts[6]=e.midx;pts[7]=e.top;
	glVertexPointer (2, GL_INT, 0, pts);
  glDrawArrays (GL_LINES, 0, 4);
  pts[0]=e.right;pts[1]=e.midy;
  pts[2]=e.midx;pts[3]=e.top;
  glDrawArrays (GL_POINTS, 0, 2);
}






