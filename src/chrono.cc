/*
* chrono.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "chrono.h"
#include "log.h"

using namespace boost::gregorian;
using namespace boost::posix_time;

audio_clock::audio_clock () {
  secs = delta_secs = 0;
  ticks = delta_ticks = 0;
	dlog << "+++ created audio clock +++" << endl;
}

audio_clock::~audio_clock () {
	dlog << "--- destroyed audio clock ---" << endl;
}

audio_clock& audio_clock::operator++ () {
  ticks += delta_ticks;
  secs += delta_secs;
  return *this;
}

ui_clock::ui_clock () {
  reset ();
	dlog << "+++ created UI clock +++" << endl;
}

ui_clock::~ui_clock () {
	dlog << "--- destroyed UI clock ---" << endl;
}

void ui_clock::reset () {
  start = microsec_clock::local_time ();
  elapsed = seconds (0);
}

double ui_clock::operator() () { // advances clock and returns seconds elapsed from last reset
  now = microsec_clock::local_time ();
  time_duration delta_elapsed (now - start);
  double delta_secs = delta_elapsed.total_microseconds () / 1000000.0;
  elapsed += delta_elapsed;
  secs_ += delta_secs;
  start = now;
  return secs_;
}

