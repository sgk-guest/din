/*
* alarm.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "alarm.h"
#include "chrono.h"

alarm_t::alarm_t (double tt) {
  triggert = tt;
  startt = 0;
  active = 0;
}

int alarm_t::operator() (double t) {
  float elapsed = t - startt;
  float dt = elapsed - triggert;
  if (dt >= 0)  {
    startt = t;
    return 1; 
  } else return 0;
}

void alarm_t::start () {
  startt = ui_clk ();
  active = 1;
}

void alarm_t::stop () {
  active = 0;
}
