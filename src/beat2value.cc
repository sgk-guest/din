/*
* beat2value.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "beat2value.h"
#include "utils.h"
#include "log.h"
#include "console.h"
#include <string>
using namespace std;

extern int SAMPLE_RATE;

beat2value::beat2value (const string& n, const string& c) : name (n), crvname (c)  { 
	crv = new multi_curve (c);
	crv_made_here = 1;
	setup (); 
}

beat2value::beat2value () { crv = 0; crv_made_here = 0;}

void beat2value::setup (int delta_mult) {
	min_bpm = 0;
	bpm = 45;
	delta = 0;
	set_bpm (bpm, delta_mult);
	style = "loop";
	xmin = &_loopmin;
	xmax = &_loopmax;
	sol (crv);
	now = sol.firstx;
}

beat2value::~beat2value () {
	if (crv_made_here) {
		crv->save (crvname);
		delete crv;
	}
}

int beat2value::gen_and_mix (float* soln, float* mixb, float* mixa, int n) {
  sol (now, delta, n, soln, *xmin, *xmax);
	if (mixer.active) {
		mixer.gen_mix (mixb, n, *xmin, *xmax);
		mixer.do_mix (soln, mixb, mixa, n);
	}
	return mixer.active;
}

int beat2value::modulate_and_mix (float* in, float* mixb, float* mixa, int n, float depth) {
  sol (now, delta, n, in, *xmin, *xmax);
	multiply (in, n, depth);
	if (mixer.active) {
		mixer.gen_mix (mixb, n, *xmin, *xmax);
		multiply (mixb, n, depth);
		mixer.do_mix (in, mixb, mixa, n);
	}
	return mixer.active;
}

int beat2value::modulate_and_mix (float* in, float* mixb, float* mixa, int n, float* depth) {
  sol (now, delta, n, in, *xmin, *xmax);
  multiply (in, depth, n);
  if (mixer.active) {
    mixer.gen_mix (mixb, n, *xmin, *xmax);
    multiply (mixb, depth, n);
    mixer.do_mix (in, mixb, mixa, n);
  }
  return mixer.active;
}


float beat2value::set_bpm (float n, int nsamples) {

  if (n < min_bpm) n = min_bpm;

	bpm = n;
	float bps = bpm / 60.0f; 
		
	// delta used in solvers
	int sgn = 1; if (delta < 0) sgn = -1;
	delta = sgn * bps * 1.0f / SAMPLE_RATE * nsamples;

	return bpm;

}

float beat2value::get_bpm () { 
  return bpm; 
}

void beat2value::set_mix (multi_curve& c) {
	mixer.setup (c, now, delta);
}
