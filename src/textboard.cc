/*
* textboard.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "textboard.h"
#include "dingl.h"
#include "font.h"
#include "basic_editor.h"
#include "mondrian.h"

#include <iostream>
using namespace std;

extern viewport view;
extern int line_height;

void textboard::draw (int shapeform) {
  glMatrixMode (GL_PROJECTION);
  	glPushMatrix ();
  		glLoadIdentity ();
  		glOrtho (0, view.xmax, 0, view.ymax, -1, 1);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
	for (list<text>::iterator i = texts.begin (), j = texts.end (); i != j; ++i) {
		text& t = *i;
		if (t.type == text::normal) {
			glColor3f (t.r, t.g, t.b);
			draw_string (t.txt, t.vx, t.vy);
		} else if (t.type == text::vline && !shapeform) { // draw vertical line with label
			draw_line (t.vx, 0, t.vx, view.ymax);
			glColor3f (t.r, t.g, t.b);
			draw_string (t.txt, t.vx, t.vy);
		} else { // draw horizontal line with label
			draw_line (0, t.vy, view.xmax, t.vy);
			glColor3f (t.r, t.g, t.b);
			draw_string (t.txt, t.vx, t.vy);
		} 
	}
	glPopMatrix ();
	glMatrixMode (GL_PROJECTION);
  glPopMatrix ();
	glMatrixMode (GL_MODELVIEW);
}

void textboard::draw_line (int x1, int y1, int x2, int y2) {
  static int pts [4] = {0};
  glColor3f (0.3f, 0.3f, 0.3f);
  pts[0]=x1;pts[1]=y1;
  pts[2]=x2;pts[3]=y2;
  glVertexPointer (2, GL_INT, 0, pts);
  glDrawArrays (GL_LINES, 0, 2);
}

void textboard::clear () { 
	for (list<text>::iterator i = texts.begin (), j = texts.end (); i != j;) {
    text& ti = *i;
    if (ti.state == text::temporary) {
			i = texts.erase (i); 
			j = texts.end ();
		} else ++i;
  }
}

void textboard::refresh (basic_editor* b, float dwx, float dwy, float dvx, float dvy) {
  static const int spc = 3;
	extern viewport view;
	float wx, wy;
	for (list<text>::iterator i = texts.begin (), j = texts.end (); i != j; ++i) {
    text& ti = *i;
    b->obj2win (ti.wx, ti.wy, wx, wy); wx += dwx; wy += dwy;
    win2view (wx, wy, ti.vx, ti.vy, b->win, view); ti.vx += dvx; ti.vy += dvy;
    if (ti.type == text::hline) ti.vx = spc; else if (ti.type == text::vline) ti.vy = line_height;
  }
}

void textboard::refresh (mondrian* b) {
	for (list<text>::iterator i = texts.begin (), j = texts.end (); i != j; ++i) {
    text& ti = *i;
    float wx, wy; b->obj2win (ti.wx, ti.wy, wx, wy);
    win2view (wx, wy, ti.vx, ti.vy, b->win, view);
  }
}

void textboard::load (ifstream& file) {
  string ignore, txt;
  float r, g, b, wx, wy;
  int type, ntexts;
  file >> ignore >> ntexts;
  for (int i = 0; i < ntexts; ++i) {
    file >> txt >> wx >> wy >> r >> g >> b >> type;
    add (text (txt, wx, wy, r, g, b, text::permanent, type));
  }
}

void textboard::save (ofstream& file) {
  static const char spc = ' ';
  int ntexts = texts.size ();
  file << "num_labels " << ntexts << endl;
	for (list<text>::iterator i = texts.begin (), j = texts.end (); i != j; ++i) {
    text& ti = *i;
    if (ti.type != text::normal) {
      file << ti.txt << spc << ti.wx << spc << ti.wy << spc << ti.r << spc << ti.g << spc << ti.b << spc << ti.type << endl;
    }
  }
}
