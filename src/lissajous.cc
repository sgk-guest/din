/*
* lissajous.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "lissajous.h"
#include "vector2d.h"
#include "ui_list.h"
#include "mouse_slider.h"
#include <vector>
using namespace std;

extern ui_list uis;

extern const float PI_BY_180;
extern const int MILLION;

lissajous::lissajous () {
  name = "Lissajous";
  center_x = 0.5;
  center_y = 0;
  load_params ();
}

lissajous::~lissajous () {
  widget_save ("d_lissajous", ctrls);
  save_params ();
}

void lissajous::load_params () {
  ifstream f (make_fname().c_str(), ios::in);
  string ignore;
  f >> ignore >> x_speed >> ignore >> y_speed >> ignore >> num_points;
}

void lissajous::save_params () {
  ofstream f (make_fname().c_str(), ios::out);
  string ignore;
  f << "x_speed " << x_speed << endl << "y_speed " << y_speed << endl << "num_points " << num_points << endl;
}


void lissajous::setup () {

  plugin::setup ();

  widget* _ctrls [] = {&sp_x_speed, &sp_y_speed, &sp_num_points};
  for (int i = 0; i < 3; ++i) ctrls.push_back (_ctrls[i]); 
  num_ctrls = ctrls.size ();

	spinner<float>* spn [] = {&sp_x_speed, &sp_y_speed};
  const char* lbl [] = {"X Speed", "Y Speed"};
	int ori [] = {mouse_slider_listener::X, mouse_slider_listener::Y};
	float val [] = {x_speed, y_speed};
  for (int i = 0; i < 2; ++i) {
    spinner<float>* spi = spn[i];
    spi->set_text (lbl[i]);
    spi->set_limits (0, MILLION);
    spi->set_delta (0.01);
    spi->set_value (val[i]);
    spi->set_listener (this);
    //spi->set_color (clr.r, clr.g, clr.b);
		spi->orient = ori[i];
  }

	sp_num_points.set_text ("Points");
	sp_num_points.set_limits (0, MILLION);
	sp_num_points.set_delta (1);
	sp_num_points.set_value (num_points);
	sp_num_points.set_listener (this);
	//sp_num_points.set_color (clr.r, clr.g, clr.b);

  widget_load ("d_lissajous", ctrls);

  render ();

  //for (int i = 0; i < num_ctrls; ++i) ctrls[i]->set_moveable (1);

}

void lissajous::render () {

	x_speed = sp_x_speed.f_value;
  y_speed = sp_y_speed.f_value;

  num_points = sp_num_points.f_value;
  if (num_points == 0) return;

  theta = 0;
  dtheta = 360.0 / num_points;

	int j = num_points + 1;
	points.resize (j);
  for (int i = 0; i < j; ++i) {
		float rtheta = theta * PI_BY_180;
    float rtheta1 = x_speed * rtheta;
		float rtheta2 = y_speed * rtheta;
		point<float>& pi = points[i];
    pi.x = center_x + sin (rtheta1);
    pi.y = center_y + sin (rtheta2);
    theta += dtheta;
  }

  ss.str("");
  ss << "lissajous_" << x_speed << '_' << y_speed;
	gen_pts ();

}



