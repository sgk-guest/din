/*
* arrow_button.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "arrow_button.h"

void arrow_button::set_dir (int _dir) {
  dir = _dir;
  const box<int>& e = extents;
  switch (dir) {
    case right:
      v[0] = e.left;
      v[1] = e.top;
      v[2] = e.right;
      v[3] = e.midy;
      v[4] = e.left;
      v[5] = e.bottom;
      set_name ("right");
      break;

    case left:
      v[0] = e.right;
      v[1] = e.top;
      v[2] = e.left;
      v[3] = e.midy;
      v[4] = e.right;
      v[5] = e.bottom;
      set_name ("left");
      break;

    case down:
      v[0] = e.left;
      v[1] = e.top;
      v[2] = e.midx;
      v[3] = e.bottom;
      v[4] = e.right;
      v[5] = e.top;
      set_name ("down");
      break;

    case up:
      v[0] = e.left;
      v[1] = e.bottom;
      v[2] = e.midx;
      v[3] = e.top;
      v[4] = e.right;
      v[5] = e.bottom;
      set_name ("up");
      break;

  }
}

void arrow_button::draw () {
  glColor3f (clr.r, clr.g, clr.b);
  glVertexPointer (2, GL_INT, 0, v);
  glDrawArrays (GL_LINE_LOOP, 0, 3);
}

void arrow_button::toggle () {
	if (dir == down) dir = right; else if (dir == right) dir = down;
}
