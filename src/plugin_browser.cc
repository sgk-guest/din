/*
* plugin_browser.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "sine_mixer.h"
#include "fractaliser.h"
#include "starrer.h"
#include "spiraler.h"
#include "rose_milker.h"
#include "circler.h"
#include "plugin_browser.h"
#include "lissajous.h"
#include "superformula.h"
#include "ui_list.h"
#include "warper.h"
#include "countries.h"
#include "morpher.h"
#include "number.h"
#include "log.h"

struct widget;

using namespace std;

extern sine_mixer sinemixer;
extern fractaliser fractaliser_;
extern circler circler_;
extern starrer starrer_;
extern spiraler spiraler_;
extern rose_milker rosemilker;
extern lissajous lissajous_;
extern superformula superformula_;
extern warper warper_;
extern countries countries_;
extern morpher morpher_;
extern number number_;

plugin_browser::~plugin_browser () {
  widget_save ("d_plugin_browser", ctrls, num_ctrls);
}

void plugin_browser::setup () {
  for (int i = 0; i < num_plugins; ++i) {
    plugin* pi = plugins [i];
    add_children_of (pi);
		il_plugins.add (pi->name);
  }

  l_title.set_moveable (1);
  ab_fold.set_listener (this);
  fb_list.set_listener (this);
  il_plugins.set_listener (this);

  cur = 0;
  plugins[cur]->unfold ();
  il_plugins.hide ();

  ctrls[0]=&l_title;ctrls[1]=&ab_fold;ctrls[2]=&fb_list;ctrls[3]=&il_plugins;
  widget_load ("d_plugin_browser", ctrls, num_ctrls);
  for (int i = 1; i < num_ctrls; ++i) l_title.add_child (ctrls[i]);

}

int plugin_browser::handle_input () {
  int r = 0;
  for (int i = 0; i < num_ctrls_; ++i) r |= ctrls[i]->handle_input ();
  if (il_plugins.visible) r |= il_plugins.handle_input (); else r |= plugins[cur]->handle_input ();
  return r;
}

void plugin_browser::update () {
  for (int i = 0; i < num_ctrls; ++i) ctrls[i]->update ();
}

void plugin_browser::draw () {
  for (int i = 0; i < num_ctrls_; ++i) ctrls[i]->draw ();
	plugin* pcur = plugins[cur];
  if (il_plugins.visible) il_plugins.draw (); else pcur->draw ();
}

void plugin_browser::add_children_of (plugin* p) {
  vector<widget*>& pctrls = p->ctrls;
  for (int i = 0, j = pctrls.size (); i < j; ++i) l_title.add_child (pctrls[i]);
}

void plugin_browser::clicked (button& b) {
  if (&b == &fb_list) {
    if (il_plugins.visible) {
      il_plugins.hide ();
      if (!folded()) plugins[cur]->unfold ();
    } else {
      il_plugins.show ();
      plugins[cur]->fold ();
    }
  } else if (&b == &il_plugins) {
		il_plugins.clicked (il_plugins);
    if (il_plugins.cur != -1) {
			set_cur (il_plugins.cur);
			il_plugins.select (0);
		}
  } else {
    if (folded()) {
      plugins[cur]->unfold (); 
      ab_fold.set_dir (arrow_button::down);
    } else {
      ab_fold.set_dir (arrow_button::right);
      plugins[cur]->fold ();
    }
  }
}

void plugin_browser::set_cur (int c) {
  il_plugins.hide ();
  plugins[cur]->fold ();
  if (c < 0) c = num_plugins - 1; else if (c >= num_plugins) c = 0;
  cur = c;
  plugin* pcur = plugins[cur];
  l_title.set_text (pcur->name);
  ab_fold.set_dir (arrow_button::down);
  pcur->unfold ();
}

void plugin_browser::set_fold (int f) {
  if (f) {
    ab_fold.set_dir (arrow_button::right);
    plugins[cur]->fold ();
  } else {
    ab_fold.set_dir (arrow_button::down);
    plugins[cur]->unfold ();
  }
}

int plugin_browser::folded () {
  return (ab_fold.dir == arrow_button::right);
}

void plugin_browser::draw (curve_editor* ed) {
  if (folded() == 0) plugins[cur]->draw (ed);
}

plugin* plugin_browser::get_cur () {
  return plugins[cur];
}

void plugin_browser::set_ed (curve_editor* e) {
	for (int i = 0; i < num_plugins; ++i) plugins[i]->set_ed (e);
}
