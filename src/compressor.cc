/*
* compressor.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "compressor.h"
using namespace std;

compressor::compressor (const string& f) : crv (f), lis (*this) {
  fname = f;
}

void compressor_listener::edited (curve_editor* ed, int i) {
	c.apply.update ();
}

compressor::~compressor () {
  crv.save (fname);
}
