/*
* help.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

 /*
  * DIN Is Noise is copyright (c) 2006-2015 Jagannathan Sampath
  *
  *
 */

#include "help.h"
#include "console.h"
#include <fstream>
using namespace std;
extern console cons;
extern string user_data_dir;
help::help (const string& fname) {
  string s;
  ifstream file ((user_data_dir + fname).c_str(), ios::in);
  if (!file) text.push_back (fname); else while (getline(file, s)) text.push_back (s);
}
void help::operator() () {
  cons.rollup (0);
  for (int i = 0, j = text.size(); i < j; ++i) cons << console::cyan << text[i] << eol;
  cons.last ();
}






