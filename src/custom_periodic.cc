/*
* custom_periodic.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "custom_periodic.h"
#include "console.h"

custom_periodic::custom_periodic (const std::string& name) : fname (name), crv (fname) {
  sol (&crv);
}

custom_periodic::~custom_periodic () {
  crv.save (fname);
}

float custom_periodic::operator() (float a, float ea) {
  extern float TWO_PI;
  return sol (a / TWO_PI);
}






