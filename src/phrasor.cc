/*
* phrasor.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "phrasor.h"
#include "dingl.h"

void phrasor::draw () {
  int i;
  float r, g, b;
  if (size) {
    if (state == phrasor::recording) {
      i = 0; 
      r = 1; g = b = 0;
    } else {
      i = cur;
      r = b = 0;
      g = 1;
    }
    glColor3f (r, g, b); 
    draw_marker (data[i].x, data[i].y);
  }
}

void phrasor::draw_marker (int x, int y) {
	const int msize = 20;
  int mk [8];
  mk[0]=x-msize;mk[1]=y;
  mk[2]=x+msize;mk[3]=y;
  mk[4]=x;mk[5]=y-msize;
  mk[6]=x;mk[7]=y+msize;
  glVertexPointer (2, GL_INT, 0, mk);
  glDrawArrays (GL_LINES, 0, 4);
}

int phrasor::next () {
  int c = cur + 1;
  if (c >= size) {
    cur = 0; 
  } else cur = c;
  amount = cur * last_1;
  return 1;
}

int phrasor::validate () {
  size = data.size ();
  if (size == 0) return 0;
  last = size - 1;
  last_1 = 1.0f / last;
  return 1;
}

void phrasor::play () {
  if (cur < size) state = playing;
}

void phrasor::set_cur (float amt) {
  if (last != -1) {
    amount = amt;
    cur = amount * last;
  }
}



