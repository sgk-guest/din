/*
* note.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "note.h"
#include "main.h"
#include "scale_info.h"
#include <math.h>
#include <map>

extern char BUFFER [];
extern const char SPC;

note::note () {
  hz = 0;
  step = 0;
	octave_position = 0;
	scale_pos = 0;
	octave = 0;
}

void note::set_freq (float f) {
  hz = f;
  hz2step (hz, step);
	sprintf (BUFFER, "%0.3f", hz);
	hz_name = BUFFER;
}

void note::set_freq (float key, float interval) {
	set_freq (key * interval * pow (2.0, octave));
}

void note::set_name (const std::string& n) {
  name = n;
}

void note::set_name (const std::string& interval, int offset) {
	extern int NOTATION;
	if (NOTATION == WESTERN) {
		extern std::map<std::string, int> NOTE_POS;
		extern const char* WESTERN_FLAT [];
		int np = NOTE_POS[interval];
		name = WESTERN_FLAT[(offset + np) % 12];
	} else {
		set_name (interval);
	}
}

void note::change_scale_pos (int j, scale_info& si) {
	scale_pos += j;
	if (scale_pos < 0) {
		scale_pos = si.second_last_note; 
		--octave;
	}
	else if (scale_pos > si.last_note) {
		scale_pos = 1;
		++octave;
	}
}

std::ofstream& note::save (std::ofstream& f) {
	f << name << SPC << hz_name << SPC << hz << SPC << step << SPC << scale_pos << SPC << octave << SPC;
	return f;
}

std::ifstream& note::load (std::ifstream& f) {
	f >> name >> hz_name >> hz >> step >> scale_pos >> octave;
	return f;
}
