/*
* starrer.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "starrer.h"
#include "vector2d.h"
#include "ui_list.h"
#include "multi_curve.h"

#include <vector>
#include <fstream>
using namespace std;

extern ofstream dlog;

extern const int MILLION;

extern ui_list uis;

starrer::starrer () {
  name = "Starrer";
  load_params ();
}

starrer::~starrer () {
  widget_save ("d_starrer", ctrls);
  save_params ();
}

void starrer::load_params () {
  ifstream f (make_fname().c_str(), ios::in);
	string ignore;
	f >> ignore >> step;
}

void starrer::save_params () {
  ofstream f (make_fname().c_str(), ios::out);
	f << "steps " << step << endl;
}

void starrer::setup () {

  plugin::setup ();

  ctrls.push_back (&sp_num_steps);
  num_ctrls = ctrls.size ();

	sp_num_steps.set_text ("Steps");
  sp_num_steps.set_value (step);
  sp_num_steps.set_limits (1, MILLION);
  sp_num_steps.set_delta (1);
  sp_num_steps.set_listener (this);
  
  widget_load ("d_starrer", ctrls);

}

int starrer::apply (multi_curve& crv) {
	int n = 0;
	vector<crvpt> pts; 
	crv.get_profile_points (pts);
	n = pts.size () - 1;
	points.clear ();
	int i = 0, j = i;
	step = sp_num_steps.f_value;
	do {
		if (i < n) {
			crvpt& pi = pts[i];
			points.push_back (point<float>(pi.x, pi.y));
			i += step;
		} else {
			i -= n;
		}
	} while (i != j);
	crvpt& p0 = pts[0];
	points.push_back (point<float>(p0.x, p0.y));
	ss.str("");
	ss << "starrer_" << step;
	return plugin::apply (crv);
}



