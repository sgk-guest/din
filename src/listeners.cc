/*
* listeners.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "din.h"
#include "listeners.h"
#include "beat2value.h"
#include "delay.h"
#include "keyboard_keyboard.h"
#include "mondrian.h"
#include "fft.h"
#include "ui_list.h"
#include "main.h"

extern fft fft0;
extern din din0;
extern keyboard_keyboard keybd2;
extern mondrian mondrian0;

void curve_listener::edited (curve_editor* ed, int i) {
  ed->render_curve_samples ();
	ed->hlabel_only = ed->curveinfo[i].curve->shapeform;
	if (ed->mix.num_vertices) ed->mix.clear ();
}

void wave_listener::edited (curve_editor* ed, int i) {
  switch (which) {
    case MICROTONAL_KEYBOARD:
      din0.wavsol.update ();
			if (ed->mix.num_vertices) din0.wavplay.set_mix (ed->mix);
      break;
    case KEYBOARD_KEYBOARD:
      keybd2.update_waveform (ed->mix);
      break;
    case DRONE:
      din0.update_drone_solvers (ed->mix);
      break;
    case MONDRIAN:
      mondrian0.update_waveform (ed->mix);
  }
  if (fft0.folded () == 0) fft0.go (ed->curveinfo[i].curve);
  curve_listener::edited (ed, i);
}

beat2value_listener::beat2value_listener () : bv (0) {}

void beat2value_listener::edited (curve_editor* ed, int i) {
  bv->sol.update ();
	if (ed->mix.num_vertices) bv->set_mix (ed->mix);
  curve_listener::edited (ed, i);
}

void drone_mod_lis::edited (curve_editor* ed, int i) {
	din0.update_drone_mod_solvers (i, ed->mix);
	curve_listener::edited (ed, i);
}

void morse_code_listener::edited (curve_editor* ed, int i) {}

void delay_listener::edited (curve_editor* ed, int i) {
  sol->update ();
  float dx = 1.0f / (dly->nsamples - 1), x = -dx;
  (*sol)(x, dx, dly->nsamples, result);
}

void attack_listener::edited (curve_editor* ed, int i) {
  inst->update_attack ();
}

void decay_listener::edited (curve_editor* ed, int i) {
  inst->update_decay ();
}

void velocity_listener::edited (curve_editor* ed, int i) {
  keybd2.velsol.update ();
}
