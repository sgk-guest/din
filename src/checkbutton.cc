/*
* checkbutton.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "checkbutton.h"
using namespace std;

checkbutton::checkbutton () : lsnr (0), colorize_ (1) {
  set_state (0);
  button::set_listener (this);
}

void checkbutton::clicked (button& b) {
  toggle ();
}


void checkbutton::set_state (int s, int cl) {
  if (s) turn_on (cl); else turn_off (cl);
}

void checkbutton::turn_on (int call) {
  if (colorize_) set_color (on_color);
  if (state != 1) {
    state = 1;
    if (lsnr && call) lsnr->changed (*this);
  }
}

void checkbutton::turn_off (int call) {
  if (colorize_) set_color (off_color);
  if (state != 0) {
    state = 0;
    if (lsnr && call) lsnr->changed (*this);
  }
}

void checkbutton::toggle () {
  if (state) turn_off (); else turn_on ();
}

void checkbutton::blend_on_off_color (float blend) {

  color result;

  if (state)
    ::blend_color (off_color, on_color, result, blend);
  else
    ::blend_color (on_color, off_color, result, blend);

  set_color (result);

}

void state_button::draw () {
	draw_bbox ();
}
