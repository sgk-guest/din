#include "multi_curve.h"
#include "modulator.h"
#include "audio.h"

mod_params::mod_params (multi_curve* c, const std::string& n) {
	bv.name = n;
	bv.crv = c;
	bv.setup (aout.samples_per_channel);
	depth = result = initial = 0;
}

void mod_params::clear (int reset) {
  depth = 0;
	result = 0;
	initial = 0;
  bv.set_bpm (bv.bpm, aout.samples_per_channel);
}

void mod_params::calc () {
  result = depth * bv.sol (bv.now, bv.delta);
}
