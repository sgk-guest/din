/*
* circler.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "circler.h"
#include "vector2d.h"
#include "ui_list.h"
#include "console.h"

#include <vector>
#include <fstream>
using namespace std;

extern ofstream dlog;

extern const int MILLION;

extern ui_list uis;

extern string user_data_dir;

extern const float PI_BY_180;
extern const float TWO_PI;

extern curve_library sin_lib, cos_lib;

circler::circler () : scr (this, "circler.scr", "circler_sin.crv", "circler_cos.crv", "circler_radius.crv", "circler_sin.ed", "circler_cos.ed", "circler_radius.ed", 1) {
  name = "Circler";
  center.x = 0.5;
  center.y = 0;
  load_params ();
}

circler::~circler () {
  widget_save ("d_circler", ctrls);
  save_params ();
}

void circler::load_params () {
  string ignore;
  ifstream f (make_fname().c_str(), ios::in);
  f >> ignore >> radius >> ignore >> phase >> ignore >> num_points;
}

void circler::save_params () {
  ofstream f (make_fname().c_str(), ios::out);
  f << "radius " << radius << endl;
  f << "phase " << phase << endl;
  f << "num_points " << num_points << endl;
}

void circler::setup () {

  plugin::setup ();

  widget* _ctrls [] = {&sp_radius, &sp_phase, &sp_num_points, &scr};
  for (int i = 0; i < 4; ++i) ctrls.push_back (_ctrls[i]); 
  num_ctrls = ctrls.size ();

  spinner<int>* spn [] = {&sp_phase, &sp_num_points};
  const char* lbl [] = {"Phase", "Points"};
  int lim [] = {-MILLION, 2};
  int val [] = {phase, num_points};
  int dta [] = {1, 1};
  for (int i = 0; i < 2; ++i) {
    spinner<int>* spi = spn[i];
    spi->set_text (lbl[i]);
    spi->set_limits (lim[i], MILLION);
    spi->set_delta (dta[i]);
    spi->set_value (val[i]);
    spi->set_listener (this);
  }

  sp_radius.set_text ("Radius");
  sp_radius.set_delta (0.01f);
  sp_radius.set_value (1.0f);
  sp_radius.set_limits (0.0f, MILLION);
  sp_radius.set_listener (this);

  widget_load ("d_circler", ctrls);

  scr.setup ();
  scr.set_pos (cb_auto_apply.posx, cb_auto_apply.posy);

  sin_cos_radius_optioned ();

  //for (int i = 0; i < num_ctrls; ++i) ctrls[i]->set_moveable (1);

}

void circler::render () {

  funktion& f_radius = *scr.pf_radius;
  funktion& f_sin = *scr.pf_sin;
  funktion& f_cos = *scr.pf_cos;

  radius = sp_radius.f_value;
  phase = sp_phase.f_value;
  num_points = sp_num_points.f_value;

  theta =  phase % 360;
  dtheta = 360.0 / num_points;

  theta *= PI_BY_180;
  dtheta *= PI_BY_180;

	int j = num_points + 1;
  points.resize (j);
  for (int i = 0; i < j; ++i) {
		point<float>& p = points[i];
    p.x = center.x + f_radius (theta) * radius * f_cos (theta);
    p.y = center.y + f_radius (theta) * radius * f_sin (theta);
    theta += dtheta;
    if (theta > TWO_PI) theta -= TWO_PI;
  }

	gen_pts ();

  ss.str("");
  ss << "circle_" << num_points;


}

/*void circler::write () {
  extern string user_data_dir;
  string fname (user_data_dir + "circler.pts");
  ofstream fout (fname.c_str(), ios::out);
  if (fout) {
    fout << "center 0.5 0" << endl;
    fout << "num_points " << num_points << endl;
    for (int i = 0, n = points.size () - 1; i < n; ++i) {
      point<float>& pti = points[i];
      fout << pti.x << ' ' << pti.y << endl;
    }
  }
}*/

void circler::sin_cos_radius_optioned () {
  render ();
}

void circler::sin_cos_radius_edited () {
  render ();
}



