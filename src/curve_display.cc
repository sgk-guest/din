/*
* curve_display.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "curve_display.h"
#include "viewwin.h"
#include "multi_curve.h"

#include <vector>
using namespace std;

extern viewport view;

void curve_display::calc_bbox () {
  crv->calc_bbox (bbox);
  add_gutter ();
}

void curve_display::draw () {
  widget::draw ();
  glMatrixMode (GL_PROJECTION);
  glPushMatrix ();
  glLoadIdentity ();
  glOrtho (bbox.left, bbox.right, bbox.bottom, bbox.top, -1, 1);
  glViewport (posx, posy, extents.width, extents.height);
  glMatrixMode (GL_MODELVIEW);
    draw_curve ();
  glMatrixMode (GL_PROJECTION);
  glPopMatrix ();
  glViewport (0, 0, view.width, view.height);
}

void curve_display::draw_curve () {
	if (!crv) return;
  int n = 0, r = 0;
  vector<curve>& curv = crv->curv;
  for (int i = 0, j = curv.size(); i < j; ++i) n += curv[i].vpts.size ();
  if (n_glpts < n) {
    if (gl_pts) delete[] gl_pts;
    gl_pts = new float [2 * n];
    n_glpts = n;
  }
  for (int i = 0, j = curv.size(); i < j; ++i) {
    vector<crvpt>& vpts = curv[i].vpts;
    for (int p = 0, q = vpts.size (); p < q; ++p) {
      gl_pts[r++] = vpts[p].x;
      gl_pts[r++] = vpts[p].y;
    }
  }
  glVertexPointer (2, GL_FLOAT, 0, gl_pts);
  glDrawArrays (GL_LINE_STRIP, 0, n);
}

void curve_display::add_gutter () {
  static const float GUTTER = 0.01f;
  bbox.left -= GUTTER;
  bbox.right += GUTTER;
  bbox.bottom -= GUTTER;
  bbox.top += GUTTER;
}

void curve_display::unit_bbox () {
  bbox.left = 0; bbox.bottom = -1;
  bbox.right = 1; bbox.top = 1;   
  add_gutter ();
  bbox.calc ();
}

curve_display::~curve_display () {
  if (gl_pts) delete [] gl_pts;
}

