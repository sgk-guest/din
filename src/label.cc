/*
* label.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "label.h"
#include "font.h"
#include "viewwin.h"

using namespace std;

extern int mousex, mouseyy;
extern viewport view;
extern int lmb;


label::label (const std::string& t) : text (t) {set_text (text);}

void label::set_text (const string& t) {
	text = t;
  set_name (text);
	int w = get_char_width (text), h = get_max_char_height (text);
	const box<int>& e = extents;
	set_extents (e.left, e.bottom, e.left + w, e.bottom + h);
}

void label::draw () {
	widget::draw ();
	draw_string (text, posx, posy);
}

void label::update () {
	set_text (text);
}
