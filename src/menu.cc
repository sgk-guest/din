/*
* menu.cc
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#include "main.h"
#include "menu.h"
#include "ui_list.h"
#include "viewwin.h"
#include "din.h"
#include "keyboard_keyboard.h"
#include "mondrian.h"
#include "binaural_drones.h"
#include "tcl_interp.h"
#include "console.h"
#include "recorder.h"
#include "oscilloscope.h"
#include "ball.h"
#include "mesh.h"

#include <string>
#include <fstream>

using namespace std;

extern ofstream dlog;

extern int mousex, mousey;

extern beat2value octave_shift;

extern std::string INSTRUMENT;
extern int CURRENT_INSTRUMENT, LAST_INSTRUMENT, NUM_INSTRUMENTS; 
extern void load_instrument ();

extern float VOICE_VOLUME;
extern int NUM_OCTAVES;
extern float DELTA_DRONE_MASTER_VOLUME;
extern const int MILLION;

extern void setup_plugin_labels ();

extern oscilloscope scope;

extern const float PI_BY_180;

extern char BUFFER [];

extern int wheel, wheely;

extern int line_height;

extern float ARROW_U, ARROW_V;


menu::menu () : gater_style_lis (ol_gater_style, "gr", " Style = "), am_style_lis (ol_am_style, "am", " Voice AM style = "), fm_style_lis (ol_fm_style, "fm", " Voice FM style = "), s_phrase_position (192, 16), td_tap_display (36), abm_left (12), abm_right(12), abm_up(12), abm_down(12), bm_zoom_in(16),  bm_zoom_out(16), abe_left (12), abe_right(12), abe_up(12), abe_down(12), pb_zoom_in (16), mb_zoom_out (16), abl_left (12, arrow_button::left), abl_right (12, arrow_button::right)/*, ab_up (18, arrow_button::up), ab_down (18, arrow_button::down)*/ {
  num_tabs = 0;
	//filtered = 0;
	am_depth = fm_depth = 0;
	dam_depth = dfm_depth = dam_bpm = dfm_bpm = 0;
	cnl.id = 0;
	cnl.name = "Left Note";
	cnl.orient = mouse_slider_listener::X;
	cnr.id = 1;
	cnr.name = "Right Note";
	cnr.orient = mouse_slider_listener::X;
	arl.name = "Range Left";
	arl.orient = mouse_slider_listener::X;
	arr.name = "Range Right";
	arr.orient = mouse_slider_listener::X;
	arb.name = "Range Left & Right";
	arb.orient = mouse_slider_listener::Y;
	rhl.name = "Range Height";
	rhl.orient = mouse_slider_listener::Y;
}

void menu::setup_items () {
  // all widgets
  widget* im [] = { 
    &b_keyboard_keyboard,
		&b_microtonal_keyboard, 
    &b_mondrian,
		&b_binaural_drones,
		&b_microtonal_keyboard_waveform,
		&b_drone_waveform,
		&b_drone_modulation,
		&b_voice_modulation,
		&b_gater,
		&b_keyboard_keyboard_waveform,
		&b_attack,
		&b_decay,
		&b_midi_velocity,
		&b_delays,
		&b_octave_shift,
		&b_compressor,
		&b_morse_code,
    &b_mondrian_waveform,
    &b_mondrian_attack,
    &b_mondrian_decay,
		&b_binaural_drones_waveform,
		&b_range_modulation,
		&b_range_width_height,
		&b_range_pitch_vol,
		&b_point_modulation,
    &b_exit_din,
		&l_octave_shift,
		&ab_octave_down,
		&ab_octave_up,
		&sp_octave_shift_bpm,
    &sp_gater_bpm,
    &sp_voice_volume,
    &cb_show_anchors,
    &sp_change_drone_handle_size,
    &sp_change_drone_trail_length,
    &sp_am_depth, 
    &sp_fm_depth,
    &sp_am_bpm, 
    &sp_fm_bpm,
    &tab_sep,
    &cb_instrument,
    &cb_editors,
    &cb_mkb_drone_parameters,
    &cb_file,
    &ol_drone_master_volume,
    &lf_delta_drone_master_volume,
    &b_mute_drones,
    &ol_gater_style,
    &l_gater,
    &ol_am_style,
    &ol_fm_style,
    &cb_mkb_drone_tools,
    &b_add_drones,
    &b_move_drones,
    &b_delete_drones,
    &b_select_all_drones,
    &b_invert_drone_selection,
    &b_record_phrase,
    &b_clear_phrases,
    &l_phrase_position,
    &s_phrase_position,
    &ol_set_range,
    &b_selected_to_all,
    &b_default_to_selected,
    &b_default_to_all,
    &b_key_to_pitch_at_cursor,
    &cb_scope,
    &sp_scope_height,
    &sp_scope_samples,
    &l_tap_bpm,
    &td_tap_display,
    &l_tap_bpm_value,
    &cb_am, 
    &cb_fm, 
    &cb_gater, 
    &cb_octave_shift, 
    &cb_auto_reset,
    &cb_ed_tools,
    &abe_left, 
    &abe_right,
    &abe_up,
    &abe_down,
    &b_close,
    &cb_snapx,
    &cb_snapy,
    &cb_snapboth,
    &cb_snapnone,
    &l_snap,
    &pb_zoom_in,
    &mb_zoom_out,
    &b_insert_vertex,
    &b_delete_vertex,
    &b_fold_tangents,
    &b_unfold_tangents,
		&ol_mirror,
    &ol_vertices_carry_tangents,
    &ol_mirror_tangents,
    &cb_selection_only,
    &b_undo,
    &b_redo,
    &b_copy,
    &b_paste,
    &b_draw_replacement_curve,
    &l_library,
    &abl_left, 
    &abl_right,
    &lf_curve_name,
    &b_add_curve,
    &b_replace_curve,
    &b_delete_curve,
    &l_capture,
    &b_start_capture,
    &b_assign_capture,
    &cb_label_vertices,
    &cb_show_waveform_samples,
    &sp_waveform_hz,
    &b_pick_curve,
    &sp_curve_limit,
    &sp_waveform_periods,
    &ol_curve_style,
    &sp_curve_rpm,
    &b_stop_rotating,
    &cb_draw_curve,
		&cb_record,
		&b_clear_record,
		&lf_file,
		&b_save,
		&b_select_attractees,
		&b_select_attractors,
		&b_orbit_selected_drones,
		&cb_show_vel,
		&cb_show_accel,
		&sp_change_drone_vel,
		&sp_change_drone_accel,
    &cb_show_gravity,
    &b_move_drones_under_gravity,
    &sp_rotate_drone_vel,
    &sp_drones_per_min,
    &b_launch_drones,
    &b_stop_launching_drones,
    &ol_create_this,
    &sp_mesh_rows,
    &sp_mesh_cols,
    &b_track_drones,
    &b_select_tracked_drones,
    &sp_bounces,
    &sp_rebound,
    &b_track_gravity,
    &b_add_balls, 
    &b_move_selected_balls,
    &b_delete_selected_targets, 
    &b_delete_all_targets,
    &b_select_all_targets, 
    &b_invert_selected_targets,
    &b_select_targets_in_box,
    &b_split_horizontal, 
    &b_split_vertical, 
    &b_delete_box,
    &sp_mondrian_min_voices,
    &sp_mondrian_change_attack_time,
    &sp_mondrian_change_decay_time,
    &sp_mondrian_change_speed,
    &b_freeze_balls,
    &b_thaw_balls,
		&abm_left,
		&abm_right,
		&abm_up,
		&abm_down,
		&bm_zoom_in,
		&bm_zoom_out,
		&b_turn_off_ui,
		&b_set_targets,
		&b_clear_targets,
		&sp_drone_lifetime,
		&sp_orbit_insertion_time,
		&b_clear_modulations,
		&b_modulate_balls_up,
		&b_modulate_balls_down,
		&cb_binaural_drones_tools,
		&lf_master_volume,
		&sp_bd_separation,
		&b_create_binaurals_on_notes,
		&b_create_binaurals_from_pitch,
	 	&lf_bd_start_pitch,
		&sp_bd_pairs,
		&lf_bd_spacing,
		&cb_close_octave,
		&lf_vol_fade_time,
		&sp_mondrian_change_dir,
		&sp_mondrian_change_trail_size,
		&sp_mondrian_change_note_poly_points,
		&sp_mondrian_change_note_poly_radius,
		&b_auto_change_direction_clockwise,
		&b_stop_auto_changing_direction,
		&b_auto_change_direction_anti_clockwise,
		&b_flip_direction,
		&b_make_random_color,
		&ol_justification,
		&cb_resize_separation,
		&ol_key_note,
		&b_add_remove_slits,
		&b_select_wreckers,
		&b_select_healers,
		&b_switch_ball_type,
		&b_toggle_wreckers,
		&b_toggle_healers,
		&b_toggle_bouncers,
		&sp_mondrian_change_slit_size,
		&b_remove_slits_on_edge,
		&b_toggle_slit_anim,
		&cb_mondrian_auto_adjust_voices,
		&sp_mondrian_change_vol,
		&cb_draw_boxes,
		&cb_fill_boxes,
		&cb_draw_notes,
		&cb_label_notes,
		&ol_ball_types,
		&ol_split_types_h,
		&ol_split_types_v,
		&sp_mondrian_num_boxes,
		&b_make_note_grid,
		&b_make_nxn_grid,
		&b_delete_all_boxes,
		&cb_mkb_voice,
		&cb_mkb_misc,
		&b_select_launchers,
		&cb_select_launched,
		&b_freeze_drones,
		&b_thaw_drones,
		&sp_dam_depth,
		&sp_dfm_depth,
		&sp_dam_bpm,
		&sp_dfm_bpm,
		&sp_rotate_drones,
		&sp_scale_drones,
		&ol_selection_targets,
		&sp_mondrian_change_slit_anim_time,
		&cb_mark_segments,
		&cb_auto_split_box,
		&cb_auto_delete_box,
		&sp_auto_split_time,
		&sp_auto_delete_time,
		&ol_auto_pick_box_split,
		&ol_auto_split_at,
		&ol_auto_split_orient,
		&ol_auto_pick_box_delete,
		&sp_min_split_size,
		&cb_speed,
		&cb_turn,
		&cb_teleport,
		&sp_turn_every,
		&sp_turn_min,
		&sp_turn_max,
		&sp_speed_every,
		&sp_speed_min,
		&sp_speed_max,
		&sp_max_speed,
		&sp_tel_every,
		&sp_tel_radius,
		&cb_draw_ball_position,
		&cb_draw_ball_heading,
		&cb_draw_ball_trails,
		&l_draw_ball,
		&cb_turn_sync,
		&cb_speed_sync,
		&sp_clone_every,
		&sp_max_clones,
		&sp_clone_offset,
		&sp_max_balls,
		&cb_clone,
		&cb_clone_can_clone,
		&ol_browse_balls,
		&cb_mon_tools, 
		&cb_mon_parameters, 
		&cb_mon_ballops, 
		&cb_mon_boxops, 
		&cb_mon_misc,
		&cb_transform,
		&sp_transform_every,
		&ol_bouncer,
		&ol_healer,
		&ol_wrecker,
		&cb_label_hz_vol,
		&il_binaural_drones,
		&cb_binaural_drones_edit,
		&bbd_select_all,
		&bbd_select_none,
		&bbd_invert_select,
		&bbd_delete,
		&bbd_sync,
		&lf_pitch_fade_time,
		&lf_modulation_amount,
		&bd_modulate_up,
		&bd_modulate_down,
		&bbd_modulate,
		&bbd_select2,
		&ol_select_what,
		&ol_select_rule,
		&bdf_value,
		&lf_l,
		&lf_r,
		&lf_sep,
		&lf_vol,
		&ol_just,
		&bbd_flip,
		&b_adjust_board_height,
		&b_adjust_range_left, 
		&b_adjust_range_right,
		&b_adjust_range_both,
		&sp_snap_left,
		&sp_snap_right,
		&ol_bounce_style,
		&b_reset_velocity,
		&b_reset_gravity,
		&cb_keep_size,
		&cb_mod_ran,
		&sp_range,
		&sp_ran_mod_width,
		&sp_ran_mod_width_bpm,
		&cb_mark_ran,
		&l_ran_mod,
		&b_rm_pause_resume,
		&b_rm_start_all,
		&b_rm_stop_all,
		&b_rm_toggle,
		&b_get_cur_ran,
		&ol_snap_style, 
		&sp_ran_mod_height,
		&sp_ran_mod_height_bpm,
		&cb_mkb_ranges,
		&l_adjust_range,
		&l_adjust_height,
		&b_adjust_range_height,
		&sp_arrow.depth,
		&sp_arrow.width,
		&l_drone_arrow,
		&sp_default_width,
		&sp_default_height,
		&b_change_note_left,
		&b_change_note_right,
		&ol_change_note_style,
		&ol_set_unset_toggle,
		&b_set,
		&b_unset,
		&b_toggle,
		&sp_browse_drone,
		&ol_drone_order,
		&l_drone_order,
		&ol_mesh_point,
		&f_mesh_xy,
		&cb_sync_rows_cols,
		&sp_mesh_dur,
		&b_flip_rows_cols,
		&cb_overlay_instrument,
		&dp_spacing,
		&dp_bpm1,
		&dp_bpm2,
		&dp_orient,
		&b_swap_curves,
		&cb_pitch_dis,
		&cb_vol_dis,
		&sp_lev_sz,
		// items list
	}; 

  // tabs
  num_tabs = 0;
  last_tab = cur_tab = 0;
  checkbutton* cbns [] = {
		&cb_file, &cb_instrument, &cb_editors, 
		&cb_mkb_voice, &cb_mkb_drone_tools, &cb_mkb_drone_parameters, &cb_mkb_ranges, &cb_mkb_misc,
		&cb_ed_tools, 
		&cb_mon_tools, &cb_mon_parameters, &cb_mon_ballops, &cb_mon_boxops, &cb_mon_misc,
		&cb_binaural_drones_tools,
		&cb_binaural_drones_edit,
	};
  static const char* const cbn_lbls [] = {
		"File", "Instrument", "Editors", 
		 "Voice", "Drone Tools", "Drone Params", "Ranges", "Misc",
		"Tools", 
		"Tools", "Params", "Ball Ops", "Box Ops", "Misc",
		"Create",
		"Edit"

	};
  for (int i = 0; i < 16; ++i) {
    checkbutton* cbni = cbns[i];
    cbni->set_text (cbn_lbls[i]);
    cbni->set_listener (this);
  }

  widget* wfile [] = {
		&cb_record,
    &b_exit_din,
		&lf_file,
		&b_clear_record,
		&b_save,
		&b_turn_off_ui,
    &cb_scope, 
    &sp_scope_height,
    &sp_scope_samples
  }; // file 
	
	cb_record.set_listener (&recl);
	b_clear_record.set_listener (&recl);
	b_save.set_listener (&recl);
	lf_file.fld.typing_lsnr = &recl;
	lf_file.fld.set_text ("din.wav");
	lf_file.lbl.set_text ("File on Desktop?");

  widget* winst [] = {
    &b_microtonal_keyboard, 
    &b_keyboard_keyboard,
    &b_mondrian,
		&b_binaural_drones
  };

  widget* weds [] = { 
    &b_microtonal_keyboard_waveform, 
    &b_drone_waveform, 
    &b_drone_modulation, 
    &b_voice_modulation, 
		&b_range_modulation,
		&b_range_width_height,
		&b_range_pitch_vol,
    &b_gater, 
    &b_keyboard_keyboard_waveform, 
    &b_attack, 
    &b_decay, 
    &b_midi_velocity, 
    &b_mondrian_waveform,
    &b_mondrian_attack,
    &b_mondrian_decay,
		&b_binaural_drones_waveform,
    &b_delays, 
    &b_octave_shift, 
    &b_compressor, 
    &b_morse_code,
		&b_point_modulation
  };

	widget* wvoice [] = {
		&sp_voice_volume,
		&b_record_phrase, 
		&b_clear_phrases,
		&s_phrase_position,
		&l_phrase_position, 
		&sp_am_depth, 
		&sp_fm_depth,
		&sp_am_bpm, 
		&sp_fm_bpm, 
		&ol_am_style,
		&ol_fm_style
	};

  for (int i = 1; i < 3; ++i) static_cast<button*>(wvoice[i])->set_listener (&pcl); // phrase commands listener

	widget* wdrone_tools [] = {
		&b_add_drones, 
		&b_move_drones, 
		&b_move_drones_under_gravity,
		&b_delete_drones,
		&b_select_all_drones, 
		&b_invert_drone_selection,
		&b_orbit_selected_drones,
		&b_select_attractees,
		&b_select_attractors,
		&b_launch_drones,
		&b_stop_launching_drones,
		&b_track_drones,
		&b_select_tracked_drones,
		&b_track_gravity,
		&b_set_targets,
		&b_clear_targets,
		&b_select_launchers,
		&b_freeze_drones,
		&b_thaw_drones,
		&b_reset_velocity,
		&b_reset_gravity,
		&b_flip_rows_cols,
		&ol_create_this,
		&sp_mesh_rows,
		&sp_mesh_cols,
		&cb_select_launched,
		&sp_rotate_drones,
		&sp_scale_drones,
		&cb_keep_size,
		&sp_browse_drone,
		&ol_drone_order,
		&l_drone_order,
		&ol_mesh_point,
		&f_mesh_xy,
		&cb_sync_rows_cols,
		&sp_mesh_dur,
		&dp_spacing,
		&dp_bpm1,
		&dp_bpm2,
		&dp_orient
	};
	for (int i = 0; i < 22; ++i) static_cast<button*>(wdrone_tools[i])->set_listener (&dcl);
	//for (int i = 0; i < 40; ++i) wdrone_tools[i]->set_moveable(1);

	widget* wdrone_params [] = { 
		&cb_show_anchors, 
		&cb_show_vel,
		&cb_show_accel,
		&cb_show_gravity,
		&sp_change_drone_vel,
		&sp_change_drone_accel,
		&sp_rotate_drone_vel,
		&sp_drones_per_min,
		&sp_drone_lifetime,
		&sp_orbit_insertion_time,
		&sp_change_drone_handle_size,
		&sp_change_drone_trail_length,
		&ol_drone_master_volume, 
		&lf_delta_drone_master_volume, 
		&b_mute_drones,
		&sp_dam_depth,
		&sp_dfm_depth,
		&sp_dam_bpm,
		&sp_dfm_bpm,
		&sp_bounces,
		&sp_rebound,
		&ol_bounce_style,
		&sp_snap_left,
		&sp_snap_right,
		&ol_snap_style,
		&sp_arrow.depth,
		&sp_arrow.width,
		&l_drone_arrow,
		&ol_set_unset_toggle,
		&b_set,
		&b_unset,
		&b_toggle
	};

	for (int i = 0; i < 4; ++i) static_cast<checkbutton*>(wdrone_params[i])->set_listener (&dcl);

	spinner<float>* spn [] = {&sp_am_depth, &sp_fm_depth, &sp_am_bpm, &sp_fm_bpm};
	spinner<float>* dspn [] = {&sp_dam_depth, &sp_dfm_depth, &sp_dam_bpm, &sp_dfm_bpm};
	static const char* txt [] = {"AM Depth", "FM Depth", "AM BPM", "FM BPM"};
	static const int ori [] = {mouse_slider_listener::Y, mouse_slider_listener::X, mouse_slider_listener::Y, mouse_slider_listener::X};

	for (int i = 0; i < 4; ++i) {
		spinner<float>* dspni = dspn[i];
		const char* txti = txt[i];
		dspni->set_text (txti);
		dspni->set_delta (1.0f);
		dspni->set_listener (this);
		dspni->orient = ori[i];
		spinner<float>* spni = spn[i];
		spni->set_text (txti);
		spni->set_listener (this);
		spni->orient = ori[i];
	}

	for (int i = 1; i < 4; ++i) spn[i]->set_delta (1.0f);
	sp_am_depth.set_delta (din0.am_delta.depth);
	sp_am_bpm.set_limits (0, MILLION);
	sp_fm_bpm.set_limits (0, MILLION);

	widget* wranges [] = {
		&b_selected_to_all,
		&b_default_to_selected,
		&b_default_to_all, 
		&b_adjust_range_left, 
		&b_adjust_range_right,
		&b_adjust_range_both,
		&b_adjust_range_height,
		&b_adjust_board_height,
		&b_rm_start_all,
		&b_rm_stop_all,
		&b_rm_toggle,
		&b_rm_pause_resume,
		&b_get_cur_ran,
		&cb_mod_ran,
		&cb_mark_ran,
		&sp_ran_mod_width,
		&sp_ran_mod_width_bpm,
		&sp_ran_mod_height,
		&sp_ran_mod_height_bpm,
		&sp_range,
		&l_ran_mod,
		&l_adjust_height,
		&l_adjust_range,
		&ol_set_range,
		&sp_default_width,
		&sp_default_height,
		&b_change_note_left,
		&b_change_note_right,
		&ol_change_note_style
	};

	for (int i = 0; i < 3; ++i) static_cast<button*>(wranges[i])->set_listener (&sral);
	for (int i = 3; i < 6; ++i) static_cast<button*>(wranges[i])->set_listener (&rwl);
	b_adjust_range_height.set_listener (&rhl);
	b_adjust_board_height.set_listener (&bhl);
	for (int i = 8; i < 13; ++i) static_cast<button*>(wranges[i])->set_listener (&rml);
	cb_mod_ran.set_listener (&rml);
	cb_mark_ran.set_listener (&rml);
	for (int i = 15; i < 19; ++i) static_cast<spinner<float>*>(wranges[i])->set_listener (&rml);
	sp_range.set_listener (&rml);
	b_change_note_left.set_listener (&cnl);
	b_change_note_right.set_listener (&cnr);
	ol_change_note_style.set_listener (&cnsl);

	//for (int i = 0; i < 29; ++i) wranges[i]->set_moveable(1);


	widget* wmisc [] = {
		&l_octave_shift, 
		&ab_octave_down, 
		&ab_octave_up, 
		&sp_octave_shift_bpm, 
		&l_gater, 
		&sp_gater_bpm, 
		&ol_gater_style, 
		&b_key_to_pitch_at_cursor,
		&l_tap_bpm,
		&td_tap_display,
		&l_tap_bpm_value,
		&cb_am, 
		&cb_fm, 
		&cb_gater, 
		&cb_octave_shift,
		&cb_auto_reset,
		&cb_pitch_dis,
		&cb_vol_dis,
		&sp_lev_sz
	};

	cb_pitch_dis.add_child (&cb_vol_dis);
	cb_pitch_dis.set_listener (&pvdl);
	cb_vol_dis.set_listener (&pvdl);

	sp_lev_sz.set_text ("Level size");
	sp_lev_sz.set_limits (1, MILLION);
	sp_lev_sz.set_delta (1);
	sp_lev_sz.set_listener (&pvdl);

	/*cb_vol_dis.set_moveable(1);
	cb_pitch_dis.set_moveable(1);*/
	
  widget* wetools [] = { // curve editors tools
    &abe_left,
    &abe_right,
    &abe_up,
    &abe_down,
    &pb_zoom_in,
    &mb_zoom_out,
		&l_snap,
    &cb_snapx,
    &cb_snapy,
    &cb_snapboth,
    &cb_snapnone,
    &b_pick_curve,
    &b_insert_vertex,
    &b_delete_vertex,
		&b_draw_replacement_curve,
		&b_undo,
		&b_redo,
		&b_copy,
		&b_paste,
		&b_swap_curves,
		&b_fold_tangents,
		&b_unfold_tangents,
		&ol_mirror,
		&cb_selection_only,
		&ol_vertices_carry_tangents,
		&ol_mirror_tangents,
		&lf_curve_name,
		&sp_curve_limit,
		&ol_curve_style,
    &l_library,
    &abl_left, 
    &abl_right,
		&b_add_curve,
		&b_replace_curve,
		&b_delete_curve,
    &l_capture,
    &b_start_capture,
    &b_assign_capture,
    &cb_draw_curve,
		&cb_label_vertices,
		&cb_mark_segments,
    &cb_show_waveform_samples,
		&sp_waveform_hz,
    &sp_waveform_periods,
    &sp_curve_rpm,
    &b_stop_rotating,
		&cb_overlay_instrument,
		&b_swap_curves
  };

	int wek [] = {7, 13, 2, 3, 6, 3, 3, 3, 2};
	for (int i = 0, j = 4, k = 0; i < 9; ++i) {
		k = wek[i];
		make_hierarchy (&wetools[j], k);
		j += k;
	}

  widget* wmon [] = { // mondrian tools
    &b_add_balls, 
    &b_move_selected_balls,
    &b_delete_selected_targets, 
    &b_delete_all_targets,
    &b_select_all_targets, 
    &b_invert_selected_targets,
    &b_split_horizontal, 
    &b_split_vertical, 
    &b_delete_box,
    &b_select_targets_in_box,
    &b_freeze_balls,
    &b_thaw_balls,
		&b_clear_modulations,
		&b_modulate_balls_up,
		&b_modulate_balls_down,
		&b_auto_change_direction_clockwise,
		&b_auto_change_direction_anti_clockwise,
		&b_stop_auto_changing_direction,
		&b_flip_direction,
		&b_make_random_color,
		&b_add_remove_slits,
		&b_select_wreckers,
		&b_select_healers,
		&b_switch_ball_type,
		&b_toggle_wreckers,
		&b_toggle_healers,
		&b_toggle_bouncers,
		&b_remove_slits_on_edge,
		&b_toggle_slit_anim,
		&b_make_note_grid,
		&b_make_nxn_grid,
		&b_delete_all_boxes,
		&ol_ball_types,
		&ol_split_types_h,
		&ol_split_types_v,
		&sp_mondrian_num_boxes,
		&ol_selection_targets
  };


  arrow_button* mnav[] = {&abm_left, &abm_down, &abm_right, &abm_up};
  int mdirs [] = {arrow_button::left, arrow_button::down, arrow_button::right, arrow_button::up};
  for (int i = 0; i < 4; ++i) {
    arrow_button* ab = mnav[i];
    ab->set_dir (mdirs[i]);
  }

  widget* wmon_pars [] = { // mondrian parameters
    &l_octave_shift,
    &ab_octave_down, 
    &ab_octave_up,
    &sp_octave_shift_bpm,
    &sp_mondrian_min_voices,
    &sp_mondrian_change_attack_time,
    &sp_mondrian_change_decay_time,
    &sp_mondrian_change_speed,
		&sp_mondrian_change_dir,
		&sp_mondrian_change_trail_size,
		&sp_mondrian_change_note_poly_points,
		&sp_mondrian_change_note_poly_radius,
		&sp_mondrian_change_slit_size,
		&sp_mondrian_change_slit_anim_time,
		&cb_mondrian_auto_adjust_voices,
		&sp_mondrian_change_vol
  };

	widget* wmon_misc [] = { // mondrian misc
		&l_draw_ball,
		&abm_left,
		&abm_right,
		&abm_up,
		&abm_down,
		&bm_zoom_in,
		&bm_zoom_out,
		&cb_draw_boxes,
		&cb_fill_boxes,
		&cb_draw_notes,
		&cb_label_notes,
		&cb_label_hz_vol,
		&cb_draw_ball_position,
		&cb_draw_ball_heading,
		&cb_draw_ball_trails
	};

	widget* wmon_boxops [] = {
		&cb_auto_split_box,
		&cb_auto_delete_box,
		&sp_auto_split_time,
		&sp_auto_delete_time,
		&ol_auto_pick_box_split,
		&ol_auto_split_at,
		&ol_auto_split_orient,
		&ol_auto_pick_box_delete,
		&sp_min_split_size
	};

	widget* wmon_ballops [] = {
		&cb_turn, &cb_speed, &cb_teleport, &cb_clone, &sp_turn_every, &sp_turn_min, &sp_turn_max, 
		&sp_speed_every, &sp_speed_min, &sp_speed_max, &sp_max_speed, &sp_tel_every, &sp_tel_radius,
		&cb_turn_sync, &cb_speed_sync, &sp_clone_every, &sp_max_clones, &sp_clone_offset, &sp_max_balls,
		&cb_clone_can_clone, &ol_browse_balls,
		&cb_transform, &sp_transform_every, &ol_bouncer, &ol_healer, &ol_wrecker,
	};
	ol_browse_balls.set_listener (&bolis);
	
	spinner<float>* bospn [] = {&sp_turn_every, &sp_speed_every, &sp_tel_every, &sp_clone_every, &sp_transform_every};
	float bodta [] = {0.01f, 0.01f, 1.0f, 1.0f, 1.0f};
	for (int i = 0; i < ball_op::NUM_OPS; ++i) {
		spinner<float>* spni = bospn[i];
		spni->set_text ("Every", " seconds");
		spni->set_delta (bodta[i]);
		spni->set_limits (0.0f, MILLION);
		spni->set_listener (&bolis);
	}

	sp_turn_min.set_text ("Clockwise Max", " degrees");
	sp_turn_min.set_limits (0, MILLION);
	sp_turn_min.set_delta (1);
	sp_turn_min.set_listener (&bolis);

	sp_turn_max.set_text ("Anti-clockwise Max", " degrees");
	sp_turn_max.set_limits (0, MILLION);
	sp_turn_max.set_delta (1);
	sp_turn_max.set_listener (&bolis);

	sp_speed_min.set_text ("Brake");
	sp_speed_min.set_limits (0, MILLION);
	sp_speed_min.set_delta (1);
	sp_speed_min.set_listener (&bolis);

	sp_speed_max.set_text ("Accelerate");
	sp_speed_max.set_limits (0, MILLION);
	sp_speed_max.set_delta (1);
	sp_speed_max.set_listener (&bolis);

	sp_max_speed.set_text ("Max speed");
	sp_max_speed.set_limits (0, MILLION);
	sp_max_speed.set_delta (1);
	sp_max_speed.set_listener (&bolis);

	sp_tel_radius.set_text ("Max distance");
	sp_tel_radius.set_limits (0, MILLION);
	sp_tel_radius.set_delta (1);
	sp_tel_radius.set_listener (&bolis);

	sp_clone_offset.set_text ("Offset");
	sp_clone_offset.set_limits (0, MILLION);
	sp_clone_offset.set_delta (1.0f);
	sp_clone_offset.set_listener (&bolis);

	sp_max_clones.set_text ("Max clones");
	sp_max_clones.set_limits (1, MILLION);
	sp_max_clones.set_delta (1);
	sp_max_clones.set_listener (&bolis);

	sp_max_balls.set_text ("Max balls");
	sp_max_balls.set_limits (0, MILLION);
	sp_max_balls.set_delta (1);
	sp_max_balls.set_listener (&bolis);

	checkbutton* bocbn [] = {&cb_turn, &cb_speed, &cb_teleport, &cb_clone, &cb_transform};
	for (int i = 0; i < ball_op::NUM_OPS; ++i) bocbn[i]->set_listener (&bolis);

	for (int i = 1; i < 7; ++i) static_cast<button*>(wmon_misc[i])->set_listener (&monl);
	for (int i = 7; i < 15; ++i) static_cast<checkbutton*>(wmon_misc[i])->set_listener (&monl);

	options_list* oll [] = {&ol_ball_types, &ol_split_types_h, &ol_split_types_v, &ol_selection_targets, 
													&ol_auto_pick_box_split, &ol_auto_split_at, &ol_auto_split_orient, &ol_auto_pick_box_delete};
	for (int i = 0; i < 8; ++i) {
		options_list* oli = oll[i];
		oli->set_listener (&monl);
		//oli->set_moveable (1);
	}

	ol_split_types_h.set_text (mondrian_listener::split_types[monl.hsplit]);
	ol_split_types_v.set_text (mondrian_listener::split_types[monl.vsplit]);
	ol_selection_targets.set_text (mondrian_listener::selection_targets[mondrian0.sel_tar]);

	/*ol_split_types_h.set_moveable(1);
	ol_split_types_v.set_moveable(1);*/

  for (int i = 0; i < 32; ++i) {
    widget* wm = wmon[i];
    static_cast<button*>(wm)->set_listener (&monl);
  }


  for (int i = 0; i < 6; ++i) static_cast<button*>(wetools[i])->set_listener (&pzl);
  for (int i = 6; i < 10; ++i) static_cast<checkbutton*>(wetools[i])->set_listener (&snl);

  b_undo.click_repeat = b_redo.click_repeat = 1;
  abl_left.click_repeat = abl_right.click_repeat = 1;

  button* crvops [] = {
		&b_pick_curve,
    &b_insert_vertex, 
    &b_delete_vertex, 
		&b_draw_replacement_curve,
    &b_fold_tangents, 
    &b_unfold_tangents, 
    &ol_mirror.option, 
    &b_copy, 
    &b_paste, 
		&b_swap_curves,
    &b_undo, 
    &b_redo,
    &abl_left,
    &abl_right,
    &b_add_curve,
    &b_replace_curve,
    &b_delete_curve,
    &b_start_capture,
    &b_assign_capture,
  };
  for (int i = 0; i < 19; ++i) crvops[i]->set_listener (&col);

	cb_label_vertices.set_listener (&col);
	cb_mark_segments.set_listener (&col);
	cb_overlay_instrument.set_listener (&col);
	cb_draw_curve.set_listener (&col);
	lf_curve_name.set_listener (&col);
  sp_curve_limit.set_listener (&col);
	ol_mirror.set_listener (this);

	widget* wbd [] = { // binaural drones
		&b_create_binaurals_on_notes,
		&b_create_binaurals_from_pitch,
		&sp_bd_separation,
		&sp_bd_pairs,
		&lf_bd_start_pitch,
		&lf_bd_spacing,
		&ol_justification,
		&ol_key_note,
		&cb_resize_separation,
		&cb_close_octave,
	};

	/*for (int i = 0; i < 10; ++i) wbd[i]->set_moveable(1);
	lf_master_volume.set_moveable(1);
	lf_vol_fade_time.set_moveable(1);*/

	widget* wbde [] = {
		&bbd_select_all,
		&bbd_select_none,
		&bbd_invert_select,
		&bbd_delete,
		&bbd_sync,
		&bbd_select2,
		&bd_modulate_up,
		&bd_modulate_down,
		&bbd_modulate,
		&bbd_flip,
		&il_binaural_drones,
		&lf_vol_fade_time,
		&lf_master_volume,
		&lf_pitch_fade_time,
		&lf_modulation_amount,
		&ol_select_what,
		&ol_select_rule,
		&bdf_value,
		&lf_l,
		&lf_r,
		&lf_sep,
		&lf_vol,
		&ol_just
	};

	il_binaural_drones.sel_lis = &bdl;
	lf_sep.fld.set_text (0.0f);
	ol_just.set_text (binaural_drones_listener::justs[binaural_drone::CENTER]);
	ol_just.set_listener (&bdl);

	bd_modulate_up.set_size (24);
	bd_modulate_down.set_size (24);
	bd_modulate_down.set_dir (arrow_button::left);

	for (int i = 0; i < 10; ++i) static_cast<button*>(wbde[i])->set_listener (&bdl);
	ol_select_what.set_listener (&bdl);
	ol_select_rule.set_listener (&bdl);
	for (int i = 0; i < 2; ++i) static_cast<button*>(wbd[i])->set_listener (&bdl); // binaural drones commands
	cb_close_octave.set_listener (&bdl);
	cb_resize_separation.set_listener (&bdl);
	ol_justification.set_listener (&bdl);
	ol_key_note.set_listener (&bdl);

	label_field* lfb [] = {&lf_bd_start_pitch, &lf_bd_spacing, &lf_master_volume, &lf_vol_fade_time, &lf_pitch_fade_time, &lf_modulation_amount, &lf_l, &lf_r, &lf_sep, &lf_vol};
	string lfbs [] = {"Start Pitch (Hz)", "Spacing (Hz)", "Master volume (%)", "Volume fade time (secs)", "Pitch fade time (secs)", "Amount", "L (Hz)", "R (Hz)", "Separation (Hz)", "Volume (%)"};
	for (int i = 0; i < 10; ++i) {
		label_field* li = lfb[i];
		li->set_label (lfbs[i]);
		li->set_listener (&bdl);
		//li->set_moveable(1);
	}
	sp_bd_separation.set_text ("Separation (Hz)");
	sp_bd_separation.set_limits (0, MILLION);
	sp_bd_separation.set_value (1.0f);
	sp_bd_separation.set_delta (1.0f);
	sp_bd_separation.orient = mouse_slider_listener::NONE;

	sp_bd_pairs.set_text ("Number of Pairs");
	sp_bd_pairs.set_value (1);
	sp_bd_pairs.set_delta (1);
	sp_bd_pairs.set_limits (1, MILLION);
	sp_bd_pairs.orient = mouse_slider_listener::NONE;

	cb_select_launched.set_listener (&dcl);
  b_mute_drones.set_listener (&dcl);

  for (int i = 0; i < 21; ++i) {
		editors.push_back (weds[i]);
		//weds[i]->set_moveable(1);
	}

  widget** wmap [] = {wfile, winst, weds, wvoice, wdrone_tools, wdrone_params, wranges, wmisc, wetools, wmon, wmon_pars, wmon_ballops, wmon_boxops, wmon_misc, wbd, wbde};
  int numw [] = {9, 4, 21, 11, 40, 32, 29, 19, 47, 37, 16, 26, 9, 15, 10, 23};


	ol_snap_style.set_listener (&sdl);

  for (int i = 0; i < 16; ++i) {
    int n = numw[i];
    widget** wmi = wmap[i];
    vector<widget*>& vw = tab_members[cbns[i]];
    for (int m = 0; m < n; ++m) {
			vw.push_back (wmi[m]);
			//wmi[m]->set_moveable(1); // mover
		}
		tab_sep.set_name ("separator");
    vw.push_back (&tab_sep);
  }

  tab_sep.set_extents (640);
  tab_sep.set_color (1.0f, 1.0f, 1.0f);

  int si [] = {-1, -1, -1, -1, 1, 6, 14, 2, 3, 8, 9, 10, 11, 4, 5, 12, 13, 20, 21, 22, 24, 34, 35, 36, 38}; // editor indices
  for (int i = 0; i < num_items; ++i) {
    items[i] = im[i];
    if (i < n_inst_ed) {
      if (i > LAST_INSTRUMENT) {
        is_inst[i] = 0; 
        is_ed[i] = 1;
      } else {
        is_ed[i] = 0;
        is_inst[i] = 1;
				//static_cast<button*>(items[i])->set_listener (this); 
      }
      scr_id [i] = si[i];
      static_cast<button*>(items[i])->set_listener (this); 
    } 
  }

  sp_voice_volume.set_text ("Volume");
  sp_voice_volume.set_delta (0.005f);
	sp_voice_volume.set_limits (0.0f, MILLION);
  sp_voice_volume.set_listener (&vvl);

  l_octave_shift.set_text ("Octave Shift");
  ab_octave_down.set_dir (arrow_button::left);
  ab_octave_down.set_listener (&osl);
  ab_octave_up.set_dir (arrow_button::right);
  ab_octave_up.set_listener (&osl);
  
	int arrow_size = 24;
  ab_octave_up.set_size (arrow_size);
  ab_octave_down.set_size (arrow_size);

  sp_octave_shift_bpm.set_text ("BPM");
  sp_octave_shift_bpm.set_delta (1);
  sp_octave_shift_bpm.set_listener (&osl);

  sp_octave_shift_bpm.set_limits (0.0f, MILLION); // max 1 MILLION beats/min

  sp_gater_bpm.set_text ("BPM");
  sp_gater_bpm.set_delta (1);
	sp_gater_bpm.set_limits (0, MILLION);
  sp_gater_bpm.set_listener (&gbl);
  ol_gater_style.set_text (" style = ");
  ol_gater_style.set_listener (&gater_style_lis);
	l_gater.set_text ("Gater");
	l_gater.add_child (&sp_gater_bpm);
	l_gater.add_child (&ol_gater_style);

	widget* osn [] = {&ab_octave_down, &ab_octave_up, &sp_octave_shift_bpm};
	for (int i = 0; i < 3; ++i) l_octave_shift.add_child (osn[i]);

	widget* brs [] = {&ol_set_range, &b_default_to_all, &b_default_to_selected, &b_selected_to_all};
	make_hierarchy (brs, 4);

	widget* aht [] = {&l_adjust_height, &b_adjust_range_height, &b_adjust_board_height};
	make_hierarchy (aht, 3);

	widget* arb [] = {&l_adjust_range, &b_adjust_range_left, &b_adjust_range_right, &b_adjust_range_both};
	make_hierarchy (arb, 4);

	widget* brm [] = {&l_ran_mod, &b_rm_pause_resume, &b_rm_start_all, &b_rm_stop_all, &b_rm_toggle};
	make_hierarchy (brm ,5);

	widget* brc [] = {&b_change_note_left, &b_change_note_right, &ol_change_note_style};
	make_hierarchy (brc, 3);

	widget* dpw0 [] = {&sp_change_drone_vel, &cb_show_vel, &cb_show_accel};
	make_hierarchy (dpw0, 3);

	widget* dpw1 [] = {&sp_change_drone_vel, &sp_rotate_drone_vel, &sp_change_drone_accel};
	make_hierarchy (dpw1, 3);

	widget* dpw2 [] = {&ol_drone_master_volume, &lf_delta_drone_master_volume, &b_mute_drones};
	make_hierarchy (dpw2, 3);

	widget* dpw3 [] = {&sp_dam_depth, &sp_dam_bpm, &sp_dfm_depth, &sp_dfm_bpm};
	make_hierarchy (dpw3, 4);

	widget* dpw4 [] = {&sp_drones_per_min, &sp_drone_lifetime, &sp_orbit_insertion_time};
	make_hierarchy (dpw4, 3);

	widget* dpw5 [] = {&sp_change_drone_trail_length, &sp_change_drone_handle_size};
	make_hierarchy (dpw5, 2);

	widget* dpw6 [] = {&ol_set_unset_toggle, &b_toggle, &b_set, &b_unset};
	make_hierarchy (dpw6, 4);

	sp_range.set_text ("Range");
	sp_range.set_delta (1);
	sp_range.draw_more = 0;

	const char* rms [] = {"Width", "BPM", "Height", "BPM"};
	spinner<float>* srm [] = {&sp_ran_mod_width, &sp_ran_mod_width_bpm, &sp_ran_mod_height, &sp_ran_mod_height_bpm};
	int srl [] = {-MILLION, 0, -MILLION, 0};
	int sro[] = {mouse_slider_listener::X, mouse_slider_listener::X, mouse_slider_listener::Y, mouse_slider_listener::Y};
	for (int i = 0; i < 4; ++i) {
		spinner<float>* sp = srm[i];
		sp->set_text (rms[i]);
		sp->set_delta (1.0f);
		sp->set_limits (srl[i], MILLION);
		//dlog << rms[i] << SPC << srl[i] << eol;
		sp->orient = sro[i];
	}

	const char* rde [] = {"Default width", "Default height"};
	spinner<int>* sde [] = {&sp_default_width, &sp_default_height};
	int msd [] = {mouse_slider_listener::X, mouse_slider_listener::Y};
	for (int i = 0; i < 2; ++i) {
		spinner<int>& si = *sde[i];
		si.set_text (rde[i]);
		si.set_delta (1);
		si.set_limits (0, MILLION);
		si.orient = msd[i];
		si.draw_more = 0;
		si.set_listener (&rdel);
	}

	widget* rmw [] = {&sp_range, &cb_mod_ran, &sp_ran_mod_width, &sp_ran_mod_width_bpm, &sp_ran_mod_height, &sp_ran_mod_height_bpm};
	make_hierarchy (rmw, 6);

	rmw[1]=&b_get_cur_ran; rmw[2]=&cb_mark_ran;
	make_hierarchy (rmw, 3);

  // drones
  //

  ol_drone_master_volume.set_text (" Drone Master Volume: "); 
  ol_drone_master_volume.set_click_repeat (1);
  ol_drone_master_volume.set_listener (this);
  lf_delta_drone_master_volume.set_label ("@");
  lf_delta_drone_master_volume.fld.set_text (DELTA_DRONE_MASTER_VOLUME);
  lf_delta_drone_master_volume.fld.change_lsnr = this;

  sp_change_drone_handle_size.set_text ("Handle size");
  sp_change_drone_handle_size.set_delta (1);
  sp_change_drone_handle_size.set_value (0);
	sp_change_drone_handle_size.set_listener (&dhsl);

	sp_change_drone_trail_length.set_text ("Trail length");
  sp_change_drone_trail_length.set_delta (1);
	sp_change_drone_trail_length.set_value (0);
  sp_change_drone_trail_length.set_listener (&dtl);

	sp_change_drone_vel.set_text ("Velocity");
	sp_change_drone_vel.set_listener (&dvl);
	sp_change_drone_vel.set_delta (0.1);
	sp_change_drone_vel.set_limits (-MILLION, +MILLION);


	sp_change_drone_accel.set_text ("Acceleration");
	sp_change_drone_accel.set_listener (&dal);
	sp_change_drone_accel.set_delta (0.05);
	sp_change_drone_accel.set_limits (-MILLION, +MILLION);

  sp_rotate_drone_vel.set_text ("Rotate velocity");
  sp_rotate_drone_vel.set_listener (&rdvl);
  sp_rotate_drone_vel.set_delta (2.0f);
  sp_rotate_drone_vel.orient = mouse_slider_listener::X;

  sp_drones_per_min.set_text ("Drones per minute");
  sp_drones_per_min.set_delta (1);
  sp_drones_per_min.set_listener (&dpml);

	sp_drone_lifetime.set_text ("Lifetime");
	sp_drone_lifetime.set_delta (0.05);
	sp_drone_lifetime.set_listener (&dlf);

	sp_orbit_insertion_time.set_text ("Orbit insertion time");
	sp_orbit_insertion_time.set_delta (0.05);
	sp_orbit_insertion_time.set_listener (&oil);

	sp_rotate_drones.set_text ("Rotate");
	sp_rotate_drones.set_delta (1.0f);
	sp_rotate_drones.set_value (0.0f);
	sp_rotate_drones.set_listener (this);

	sp_scale_drones.set_text ("Scale");
	sp_scale_drones.set_delta (0.01f);
	sp_scale_drones.set_value (0.0f);
	sp_scale_drones.set_listener (this);

	sp_browse_drone.set_text ("Browse drone");
	sp_browse_drone.set_delta (1);
	sp_browse_drone.set_listener (this);
	sp_browse_drone.draw_more = 0;

	ol_create_this.set_listener (this);
	ol_create_this.option.set_listener (&dcl);

	dp_orient.set_listener (this);

	dp_spacing.set_listener (this);
	dp_spacing.set_text ("Spacing");
	dp_spacing.set_limits (2, MILLION);
	dp_spacing.set_delta (1);

	dp_bpm1.set_listener (this);
	dp_bpm1.set_limits (0, MILLION);
	dp_bpm1.set_text ("First Drone BPM");
	dp_bpm1.set_delta (1.0f);

	dp_bpm2.set_listener (this);
	dp_bpm2.set_limits (0, MILLION);
	dp_bpm2.set_text ("Last Drone BPM");
	dp_bpm2.set_delta (1.0f);

	widget* dpw [] = {
		&ol_create_this,
		&dp_orient,
		&dp_spacing,
		&dp_bpm1,
		&dp_bpm2
	};
	make_hierarchy (dpw, 5);

  spinner<int>* msh [] = {&sp_mesh_rows, &sp_mesh_cols};
  static const char* const mlb [] = {"Rows", "Columns"};
  for (int i = 0; i < 2; ++i) {
    spinner<int>* sp = msh[i];
    sp->set_limits (2, MILLION);
		sp->set_value (2);
    sp->set_delta (1);
    sp->set_text (mlb[i]);
    sp->set_listener (this);
		sp->orient = mouse_slider_listener::NONE;
		sp->draw_more = 0;
  }
	cb_sync_rows_cols.set_text ("Sync");
	cb_sync_rows_cols.set_listener (&dcl);
	l_drone_order.set_text ("Create drones");
	ol_drone_order.set_listener (this);
	ol_mesh_point.set_listener (this);

	f_mesh_xy.set_text ("0 0");
	f_mesh_xy.change_lsnr = this;

	sp_mesh_dur.set_text ("In", " seconds");
	sp_mesh_dur.set_limits (0, MILLION);
	sp_mesh_dur.set_delta (0.25f);
	sp_mesh_dur.set_value (1.0f);
	sp_mesh_dur.set_listener (this);

	widget *rcw [] = {&ol_create_this, &sp_mesh_rows, &sp_mesh_cols, &cb_sync_rows_cols, &b_flip_rows_cols};
	make_hierarchy (rcw, 5);

	widget* dow [] = {&l_drone_order, &ol_drone_order, &ol_mesh_point, &f_mesh_xy};
	make_hierarchy (dow, 4);

	widget* mshw [] = {&sp_mesh_rows, &sp_mesh_dur, &l_drone_order};
	make_hierarchy (mshw, 3);

	widget* grw [] = {&b_move_drones_under_gravity, &b_reset_gravity, &cb_keep_size, &b_reset_velocity};
	make_hierarchy (grw, 4);

	widget* rotw [] = {&sp_rotate_drones, &sp_scale_drones, &b_move_drones_under_gravity};
	make_hierarchy (rotw, 3);

	ol_set_unset_toggle.set_text (" Modulation affects velocity");
	ol_set_unset_toggle.set_listener (this);
	button* bsut [] = {&b_set, &b_unset, &b_toggle};
	for (int i = 0; i < 3; ++i) bsut[i]->set_listener (&sutl);

  ol_am_style.set_listener (&am_style_lis); // voice am
  ol_fm_style.set_listener (&fm_style_lis); // voice fm
  
  l_phrase_position.set_text ("Phrase position ");
  s_phrase_position.set_limits (0.0f, 1.0f);
  s_phrase_position.set_listener (this);
  
  b_exit_din.set_listener (&miscl);
	b_turn_off_ui.set_listener (&miscl);

	const char* lrs [] = {"Adjust Height?", "Adjust?", "Modulation?"};
	label* lrm [] = {&l_adjust_height, &l_adjust_range, &l_ran_mod};
	for (int i = 0; i < 3; ++i) lrm[i]->set_text (lrs[i]);

	ol_set_range.set_listener (&sral);

	const char* snp [] = {"Snap left", "Snap right"};
	spinner<float>* snsp [] = {&sp_snap_left, &sp_snap_right};
	for (int i = 0; i < 2; ++i) {
		spinner<float>* si = snsp[i];
		si->set_text (snp[i]);
		//si->set_limits (0.0f, 1.0f);
		si->set_delta (0.01f);
		si->draw_more = 0;
		si->orient = mouse_slider_listener::X;
		si->set_listener (&sdl);
		//si->set_moveable(1);
	}
	widget* wsp [] = {&sp_snap_left, &sp_snap_right, &ol_snap_style};
	make_hierarchy (wsp, 3);
	//ol_snap_style.set_moveable(1);

  b_key_to_pitch_at_cursor.set_listener (&miscl);

	l_drone_arrow.set_text ("Drone arrow");
	spinner<float>* das [] = {&sp_arrow.depth, &sp_arrow.width};
	int dao [] = {mouse_slider_listener::Y, mouse_slider_listener::X};
	float vals [] = {ARROW_U, ARROW_V};
	const char* dasl [] = {"Depth", "Width"};
	for (int i = 0; i < 2; ++i) {
		spinner<float>& si = *das[i];
		si.set_text (dasl[i]);
		si.set_limits (-MILLION, MILLION);
		si.set_delta (0.1f);
		si.set_value (vals[i]);
		si.set_listener (&uvl);
		si.orient = dao[i];
		si.draw_more = 0;
	}

	widget* wda [] = {&l_drone_arrow, &sp_arrow.depth, &sp_arrow.width};
	make_hierarchy (wda, 3);

  cb_scope.set_text ("Show oscilloscope");
  cb_scope.set_listener (&scol);
  sp_scope_height.set_text ("Height");
  sp_scope_height.set_limits (0, MILLION);
  sp_scope_samples.set_text ("Samples");
	sp_scope_samples.orient = mouse_slider_listener::X;
  sp_scope_samples.set_limits (1, MAX_SAMPLES);

  l_tap_bpm.set_text ("Tap BPM");
  checkbutton* cb_tars [] = {&cb_am, &cb_fm, &cb_gater, &cb_octave_shift, &cb_auto_reset};
  static const char* const cb_text [] = {"AM", "FM", "Gater", "Octave Shift", "Auto reset"};

  cb_auto_reset.turn_on ();
  for (int i = 0; i < 5; ++i) {
    checkbutton* cbi = cb_tars[i];
    cbi->set_text (cb_text[i]);
    cbi->set_listener (&tbl);
		l_tap_bpm.add_child (cbi);
  }
  td_tap_display.set_listener (&tbl);
	l_tap_bpm.add_child (&td_tap_display);
	l_tap_bpm.add_child (&l_tap_bpm_value);

  spinner<int>* sco [] = {&sp_scope_height, &sp_scope_samples};
  for (int i = 0; i < 2; ++i) {
    spinner<int>* sp = sco[i];
    sp->set_delta (1);
    sp->set_listener (&scol);
  }

  const char* bpmstr [] = {"os", "fm", "am", "gr"};
  spinner<float>* bpmspn [] = {&sp_octave_shift_bpm, &sp_fm_bpm, &sp_am_bpm, &sp_gater_bpm};
  for (int i = 0; i < 4; ++i) bpm_map[bpmstr[i]] = bpmspn[i];

  // editor tools 
  arrow_button* enav[] = {&abe_left, &abe_down, &abe_right, &abe_up};
  int edirs [] = {arrow_button::left, arrow_button::down, arrow_button::right, arrow_button::up};
  for (int i = 0; i < 4; ++i) {
    arrow_button* ab = enav[i];
    ab->set_dir (edirs[i]);
  }

  l_snap.set_text ("Snap?");
  checkbutton* cb_snaps [] = {&cb_snapx, &cb_snapy, &cb_snapboth, &cb_snapnone};
  const char* cb_snap_str [] = {"X", "Y", "Both", "None"};
  for (int i = 0; i < 4; ++i) {
    checkbutton* cbi = cb_snaps[i];
    cbi->set_text (cb_snap_str[i]);
    //l_snap.add_child (cbi);
  }

  l_library.set_text ("Library");

  lf_curve_name.set_label ("Curve name");

  l_capture.set_text ("Mouse capture");

  ol_vertices_carry_tangents.set_listener (this);
	//ol_vertices_carry_tangents.set_moveable (1);
	
  ol_mirror_tangents.set_listener (this);
	//ol_mirror_tangents.set_moveable (1);

  ol_curve_style.set_listener (this);
	//ol_curve_style.set_moveable (1);


  sp_curve_rpm.set_text ("RPM");
  sp_curve_rpm.set_value (0.0f);
  sp_curve_rpm.set_delta (1.0f);
  sp_curve_rpm.set_limits (0.0f, MILLION);
  sp_curve_rpm.set_listener (&col);
  //sp_curve_rpm.lbl.add_child (&b_stop_rotating);

  b_stop_rotating.set_listener (&col);
  
  cb_show_waveform_samples.set_listener (&col);
  //cb_show_waveform_samples.set_moveable (1);
  sp_waveform_hz.set_text ("Hz");
  sp_waveform_hz.set_delta (1);
  sp_waveform_hz.set_limits (0.01, MILLION);
  sp_waveform_hz.set_listener (&col);
	sp_waveform_hz.orient = mouse_slider_listener::NONE;
  sp_waveform_periods.set_text ("Cycles");
  sp_waveform_periods.set_delta (1);
  sp_waveform_periods.set_limits (1, MILLION);
  sp_waveform_periods.set_listener (&col);
	sp_waveform_periods.orient = mouse_slider_listener::NONE;

  sp_curve_limit.set_text ("Curve roughness");
  sp_curve_limit.set_limits (0.001, MILLION);
  sp_curve_limit.set_delta (0.001);


	// button labels
	const char* labels [] = {
    "Menu",
    "Microtonal Keyboard",
    "Keyboard Keyboard",
    "Mondrian",
		"Binaural Drones",
    "Waveform",
    "Drone Waveform",
    "Drone Modulation",
    "Voice Modulation",
    "Gater",
    "Waveform",
    "Attack",
    "Decay",
    "MIDI Velocity",
    "Delays",
    "Octave Shift",
    "Compressor",
    "Morse Code",
    "Exit DIN Is Noise",
    "Show anchors",
    "Mute",
    "Add",
    "Move",
    "Delete",
    "Select all",
    "Invert selected",
    "Record a phrase",
    "Clear phrase",
    "Selected to all",
    "Default to Selected",
    "Default to all",
    "Set key to pitch at cursor",
    "Insert vertex",
    "Delete vertex",
    "Fold tangents",
    "Unfold tangents",
    "Undo", 
    "Redo",
    "Copy", 
    "Paste",
    "Draw & replace curve",
    "Add",
    "Replace",
    "Delete",
    "Start",
    "Assign",
    "Label vertices",
    "(Selection only)",
    "Show waveform",
    "Pick curve",
    "Stop",
    "Draw curve only",
		"Clear",
		"Record",
		"Select attractees",
		"Select attractors",
		"Orbit",
		"Show velocity",
		"Show acceleration",
    "Show gravity",
    "Move under gravity",
    "Launch",
    "Stop launch",
    "Track",
    "Select tracked",
    "Track gravity",
    "Waveform",
    "Attack",
    "Decay",
    "Add ", 
    "Move balls", 
    "Delete selected balls", 
    "Delete all balls", 
    "Select all balls", 
    "Invert ball selection",
    "Select balls in box",
    "Split box horizontally", 
    "Split box vertically", 
    "Delete box",
    "Freeze balls",
    "Thaw balls",
		"Turn Off UI",
		"Set targets",
		"Clear targets",
		"Clear modulations",
		"Modulate balls up",
		"Modulate balls down",
		"Create binaural drones on the notes of the scale",
		"Create binaural drones using parameters above",
		"Waveform",
		"Close octave",
		"Auto-change ball direction clockwise",
		"Auto-change ball direction anti-clockwise",
		"Stop auto-changing ball direction",
		"Flip ball direction",
		"Randomize box color",
		"Resize separation",
		"Add / Remove slits",
		"Toggle wreckers",
		"Toggle healers",
		"Toggle bouncers",
		"Healers <> Wreckers",
		"Select wreckers",
		"Select healers",
		"Remove slits on edge",
		"Toggle slit animation",
		"Auto adjust voices",
		"Draw boxes",
		"Fill boxes",
		"Draw notes",
		"Label notes",
		"Position",
		"Make note grid",
		"Make N x N grid",
		"Delete all boxes",
		"Select launchers",
		"Auto select launched drones",
		"Freeze", 
		"Thaw",
		"Mark segments",
		"Auto split box",
		"Auto delete box",
		"Speed",
		"Turn",
		"Teleport",
		"Heading",
		"Trails",
		"Draw ball:",
		"Sync",
		"Sync",
		"Clone",
		"Clone can clone too",
		"Transform",
		"Label pitch and volume",
		"All",
		"None",
		"Invert",
		"Delete", 
		"Sync",
		"Modulate",
		"Select",
		"Flip",
		"All",
		"Left",
		"Right",
		"Both",
		"Reset velocity",
		"Reset Gravity", 
		"Keep size",
		"Modulate",
		"Mark",
		"Pause/Resume",
		"Start",
		"Stop",
		"Toggle",
		"Get",
		"Range modulation",
		"Selected", 
		"Change Left note",
		"Change Right note",
		"Set",
		"Unset",
		"Toggle",
		"Range Width & Height",
		"Flip",
		"Overlay instrument",
		"Range Pitch & Volume",
		"Swap",
		"Overlay pitch distribution",
		"Overlay volume distributon",
		"Point Modulation",
  };

  button* buttons [] = {
    &b_menu,
    &b_microtonal_keyboard, 
    &b_keyboard_keyboard,
    &b_mondrian,
		&b_binaural_drones,
    &b_microtonal_keyboard_waveform,
    &b_drone_waveform,
    &b_drone_modulation,
    &b_voice_modulation,
    &b_gater,
    &b_keyboard_keyboard_waveform,
    &b_attack,
    &b_decay,
    &b_midi_velocity,
    &b_delays,
    &b_octave_shift,
    &b_compressor,
    &b_morse_code,
    &b_exit_din,
    &cb_show_anchors,
    &b_mute_drones,
    &b_add_drones,
    &b_move_drones,
    &b_delete_drones,
    &b_select_all_drones,
    &b_invert_drone_selection,
    &b_record_phrase,
    &b_clear_phrases,
    &b_selected_to_all,
    &b_default_to_selected,
    &b_default_to_all,
    &b_key_to_pitch_at_cursor,
    &b_insert_vertex,
    &b_delete_vertex,
    &b_fold_tangents,
    &b_unfold_tangents,
    &b_undo,
    &b_redo,
    &b_copy,
    &b_paste,
    &b_draw_replacement_curve,
    &b_add_curve,
    &b_replace_curve,
    &b_delete_curve,
    &b_start_capture,
    &b_assign_capture,
    &cb_label_vertices,
    &cb_selection_only,
    &cb_show_waveform_samples,
    &b_pick_curve,
    &b_stop_rotating,
    &cb_draw_curve,
		&b_clear_record,
		&cb_record,
		&b_select_attractees,
		&b_select_attractors,
		&b_orbit_selected_drones,
		&cb_show_vel,
		&cb_show_accel,
    &cb_show_gravity,
    &b_move_drones_under_gravity,
    &b_launch_drones,
    &b_stop_launching_drones,
    &b_track_drones,
    &b_select_tracked_drones,
    &b_track_gravity,
    &b_mondrian_waveform,
    &b_mondrian_attack,
    &b_mondrian_decay,
    &b_add_balls, 
    &b_move_selected_balls,
    &b_delete_selected_targets, 
    &b_delete_all_targets,
    &b_select_all_targets, 
    &b_invert_selected_targets,
    &b_select_targets_in_box,
    &b_split_horizontal, 
    &b_split_vertical, 
    &b_delete_box,
    &b_freeze_balls,
    &b_thaw_balls,
		&b_turn_off_ui,
		&b_set_targets,
		&b_clear_targets,
		&b_clear_modulations,
		&b_modulate_balls_up,
		&b_modulate_balls_down,
		&b_create_binaurals_on_notes,
		&b_create_binaurals_from_pitch,
		&b_binaural_drones_waveform,
		&cb_close_octave,
		&b_auto_change_direction_clockwise,
		&b_auto_change_direction_anti_clockwise,
		&b_stop_auto_changing_direction,
		&b_flip_direction,
		&b_make_random_color,
		&cb_resize_separation,
		&b_add_remove_slits,
		&b_toggle_wreckers,
		&b_toggle_healers,
		&b_toggle_bouncers,
		&b_switch_ball_type,
		&b_select_wreckers,
		&b_select_healers,
		&b_remove_slits_on_edge,
		&b_toggle_slit_anim,
		&cb_mondrian_auto_adjust_voices,
		&cb_draw_boxes,
		&cb_fill_boxes,
		&cb_draw_notes,
		&cb_label_notes,
		&cb_draw_ball_position,
		&b_make_note_grid,
		&b_make_nxn_grid,
		&b_delete_all_boxes,
		&b_select_launchers,
		&cb_select_launched,
		&b_freeze_drones,
		&b_thaw_drones,
		&cb_mark_segments,
		&cb_auto_split_box,
		&cb_auto_delete_box,
		&cb_speed,
		&cb_turn,
		&cb_teleport,
		&cb_draw_ball_heading,
		&cb_draw_ball_trails,
		&l_draw_ball,
		&cb_turn_sync,
		&cb_speed_sync,
		&cb_clone,
		&cb_clone_can_clone,
		&cb_transform,
		&cb_label_hz_vol,
		&bbd_select_all,
		&bbd_select_none,
		&bbd_invert_select,
		&bbd_delete,
		&bbd_sync,
		&bbd_modulate,
		&bbd_select2,
		&bbd_flip,
		&b_adjust_board_height,
		&b_adjust_range_left,
		&b_adjust_range_right,
		&b_adjust_range_both,
		&b_reset_velocity,
		&b_reset_gravity,
		&cb_keep_size,
		&cb_mod_ran,
		&cb_mark_ran,
		&b_rm_pause_resume,
		&b_rm_start_all,
		&b_rm_stop_all,
		&b_rm_toggle,
		&b_get_cur_ran,
		&b_range_modulation,
		&b_adjust_range_height,
		&b_change_note_left,
		&b_change_note_right,
		&b_set,
		&b_unset,
		&b_toggle,
		&b_range_width_height,
		&b_flip_rows_cols,
		&cb_overlay_instrument,
		&b_range_pitch_vol,
		&b_swap_curves,
		&cb_pitch_dis,
		&cb_vol_dis,
		&b_point_modulation
  };

  for (int i = 0; i < 171; ++i) {
    button* bi = buttons[i];
    bi->set_text (labels[i]);
  }

	ol_select_what.set_text ("L");
	ol_select_rule.set_text (" >= ");
	bdf_value.change_lsnr = &bdl;
	bdf_value.set_text (bdl.val[binaural_drones_listener::GREATER_THAN_EQUAL]);

  sp_bounces.set_text ("Bounces");
  sp_bounces.set_value (0);
  sp_bounces.set_limits (0, MILLION);
  sp_bounces.set_delta (1);
	sp_bounces.set_listener (this);

  sp_rebound.set_text ("Speed %");
  sp_rebound.set_value (99);
  sp_rebound.set_limits (0, MILLION);
  sp_rebound.set_delta (1);
	sp_rebound.set_listener (this);

	ol_bounce_style.set_listener (this);

	widget* wbo [] = {&sp_bounces, &sp_rebound, &ol_bounce_style, &cb_show_gravity};
	make_hierarchy (wbo, 4);

  sp_mondrian_min_voices.set_text ("Min Voices");
  sp_mondrian_min_voices.set_limits (1, MILLION);
  sp_mondrian_min_voices.set_delta (1);
	sp_mondrian_min_voices.set_listener (&monl);
	sp_mondrian_min_voices.draw_more = 0;
	cb_mondrian_auto_adjust_voices.set_listener (&monl);
  sp_mondrian_change_attack_time.set_text ("Ball attack time");
  sp_mondrian_change_attack_time.set_delta (0.01f);
	sp_mondrian_change_attack_time.set_listener (&batl);
	sp_mondrian_change_decay_time.set_text ("Ball decay time");
  sp_mondrian_change_decay_time.set_delta (0.01f);
	sp_mondrian_change_decay_time.set_listener (&bdtl);
  sp_mondrian_change_speed.set_text ("Ball speed");
  sp_mondrian_change_speed.set_listener (&bsl);
	sp_mondrian_change_speed.set_listener (&monl, 1);
	sp_mondrian_change_dir.set_text ("Ball direction");
	sp_mondrian_change_dir.set_delta (1);
  sp_mondrian_change_dir.orient = mouse_slider_listener::X;
	sp_mondrian_change_dir.draw_more = 0;
	sp_mondrian_change_dir.set_listener (&brl);
	//sp_mondrian_change_dir.set_listener (&monl, 1);
	sp_mondrian_change_trail_size.set_text ("Ball trail length");
	sp_mondrian_change_trail_size.set_delta (1);
	sp_mondrian_change_trail_size.set_listener (&tll);
	sp_mondrian_change_note_poly_points.set_text ("Note polygon points");
	sp_mondrian_change_note_poly_points.set_limits (2, MILLION);
	sp_mondrian_change_note_poly_points.set_delta (1);
	sp_mondrian_change_note_poly_points.set_listener (&nppl);
	sp_mondrian_change_note_poly_radius.set_text ("Note polygon radius");
	sp_mondrian_change_note_poly_radius.set_limits (0, MILLION);
	sp_mondrian_change_note_poly_radius.set_delta (1);
	sp_mondrian_change_note_poly_radius.set_listener (&nprl);
	sp_mondrian_change_slit_size.set_text ("Slit size");
	sp_mondrian_change_slit_size.set_delta (1.0f);
	sp_mondrian_change_slit_size.set_listener (&ssl);
	sp_mondrian_change_slit_anim_time.set_text ("Slit open/close time");
	sp_mondrian_change_slit_anim_time.set_value (0.0f);
	sp_mondrian_change_slit_anim_time.set_delta (0.05f);
	sp_mondrian_change_slit_anim_time.set_listener (&satl);
	sp_mondrian_change_vol.set_text ("Ball volume");
	sp_mondrian_change_vol.set_value (0);
	sp_mondrian_change_vol.set_delta (0.01);
	sp_mondrian_change_vol.set_limits (-MILLION, MILLION);
	sp_mondrian_change_vol.set_listener (&bvl);
	sp_mondrian_num_boxes.set_limits (0, MILLION);
	sp_mondrian_num_boxes.set_text ("N");
	sp_mondrian_num_boxes.set_delta (1);
	sp_mondrian_num_boxes.draw_more = 0;
	sp_mondrian_num_boxes.set_listener (&monl);

	sp_auto_split_time.set_text ("Every", " seconds");
	sp_auto_split_time.set_delta (0.1);
	sp_auto_split_time.set_limits (0.01, MILLION);
	sp_auto_split_time.set_listener (&monl);

	sp_auto_delete_time.set_text ("Every", " seconds");
	sp_auto_delete_time.set_delta (0.1);
	sp_auto_delete_time.set_limits (0.01, MILLION);
	sp_auto_delete_time.set_listener (&monl);

	sp_min_split_size.set_text ("Min split size");
	sp_min_split_size.set_delta (1);
	sp_min_split_size.set_limits (2, MILLION);
	sp_min_split_size.set_listener (&monl);
	
	cb_auto_split_box.set_listener (&monl);
	cb_auto_delete_box.set_listener (&monl);

	options_list* olt [] = {&ol_bouncer, &ol_wrecker, &ol_healer};
	for (int i = 0; i < 3; ++i) olt[i]->set_listener (&bolis);

	button* pb[] = {&sp_mondrian_change_dir.inc, &sp_mondrian_change_dir.dec};
	set_repeat (pb, 2, 0.005);

	recl.typing (lf_file.fld);
}

void menu::update () {
	position_menu_items ();
  position_tabs ();
}

void menu::position_menu_items () {
	static const int lines = 4;
	int targety = view.ymax - lines * line_height;
	int dy = targety - cb_file.extents.bottom;
  for (int p = 0; p < num_items; ++p) items[p]->move (0, dy, 0);
}

void menu::setup () {
  dlog << "*** setting up menu ***" << endl;
  show = 0;
  screen_mousex = screen_mousey = 0;
  setup_items ();
  widget_load ("d_menu", items, num_items);
	b_menu.set_listener (this);
  dlog << "+++ menu setup complete +++" << endl;
}

void menu::set_pos (int x, int y) {
  b_menu.set_pos (x, y);
}

void menu::draw () {
  b_menu.draw ();
  if (show) {
    // draw transparent bg
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glColor4f (0, 0, 0.0, opacity);
      glRecti (bg_extents.left, bg_extents.bottom, bg_extents.right, bg_extents.top);
    glDisable (GL_BLEND);

		glPolygonMode (GL_FRONT, GL_LINE);
		glColor3f (0.5, 0.5, 0.5);
		glRecti (bg_extents.left, bg_extents.bottom, bg_extents.right, bg_extents.top); 
		glPolygonMode (GL_FRONT, GL_FILL);

    // draw items
    for (int i = 0; i < num_tabs; ++i) tabs[i]->draw ();
    if (cur_tab) { 
      vector<widget*>& ti = tab_members [cur_tab];
      for (int i = 0, j = ti.size (); i < j; ++i) ti[i]->draw ();
      if (cur_tab == &cb_mkb_voice && din0.phrasor0.state == phrasor::playing) s_phrase_position.set_val (din0.phrasor0.amount);
    }
  }
}

void menu::setup_tabs (ui* scr) {
	checkbutton* com [] = {&cb_file, &cb_instrument, &cb_editors};
  checkbutton* mkb [] = {&cb_mkb_voice, &cb_mkb_drone_tools, &cb_mkb_drone_parameters, &cb_mkb_ranges, &cb_mkb_misc};
  checkbutton* eds [] = {&cb_ed_tools};
  checkbutton* mon [] = {&cb_mon_tools, &cb_mon_parameters, &cb_mon_ballops, &cb_mon_boxops, &cb_mon_misc};
	checkbutton* bd [] = {&cb_binaural_drones_tools, &cb_binaural_drones_edit};
  int clear_existing_tabs = 1;
	setup_tabs (com, 3, clear_existing_tabs);
  if (scr == &din0) { // microtonal keyboard
    setup_tabs (mkb, 5);
  } else if (scr == &keybd2) { // keyboard-keyboard
  } else if (scr == &mondrian0) { // mondrian
    setup_tabs (mon, 5);
  } else if (scr == &binaural_drones0) { // binaural drones
		setup_tabs (bd, 2);
	} else { // is an editor 
    setup_tabs (com, 3, clear_existing_tabs);
    setup_tabs (eds, 1);
    changed (cb_ed_tools); 
  }
  position_tabs ();
}

void menu::setup_tabs (checkbutton** tlist, int n, int clear) {
  if (clear) tabs.clear ();
  for (int i = 0; i < n; ++i) tabs.push_back (tlist[i]);
  num_tabs = tabs.size ();
}

void menu::position_tabs () {
  if (num_tabs) {
    int x = cb_file.posx, y = cb_file.posy, spacing = 20;
    int i = 1, j = 0;
    for (; i < num_tabs; ++i, j = i - 1) {
      x = x + get_char_width (tabs[j]->text) + spacing;
      tabs[i]->set_pos (x, y);
    }

    tab_sep.set_extents (x + get_char_width (tabs[j]->text) - cb_file.posx);
    calc_bg ();

    checkbutton* lt = tabs[num_tabs - 1];
    menu_mousex = lt->extents.right + 1;
    menu_mousey = view.ymax - lt->posy;
  }
}

void menu::remove_from_tab (checkbutton* cb, widget* w) {
  vector<widget*>& tw = tab_members [cb];
  vector<widget*>::iterator end = tw.end (), i = find (tw.begin(), end, w);
  if (i != end) tw.erase (i); 
}

void menu::add_to_tab (checkbutton* cb, widget* w) {
  vector<widget*>& tw = tab_members[cb];
	vector<widget*>::iterator end = tw.end (), i = find (tw.begin(), end, w);
  if (i == end) tw.push_back (w);
}

int menu::handle_input () {

  if (b_menu.handle_input ()) return 1; // menu button present on all screens

  if (show) {

    // find tab mouse is hovering on
    for (int i = 0; i < num_tabs; ++i) tabs[i]->handle_input ();

    if (cur_tab) { // handle tab's items
      vector<widget*>& tm = tab_members [cur_tab];
      for (int i = 0, j = tm.size (); i < j; ++i) if (tm[i]->handle_input ()) return 1;
    }

		if (wheel && !widget::HOVER) move_items (0, wheel * wheely);

  }
  return 0;
}

void menu::init_modulation () {
  spinner<float>* spn [] = {&sp_am_depth, &sp_fm_depth, &sp_am_bpm, &sp_fm_bpm}; // for voice
	float vals [] = {din0.am_depth, din0.fm_depth, din0.am.bpm, din0.fm.bpm};
	spinner<float>* dspn [] = {&sp_dam_depth, &sp_dfm_depth, &sp_dam_bpm, &sp_dfm_bpm}; // for drones
	float* d_prev_vals [] = {&dam_depth, &dfm_depth, &dam_bpm, &dfm_bpm};
	for (int i = 0; i < 4; ++i) {
		spinner<float>* dspni = dspn[i];
		dspni->limits = 0;
		dspni->set_value (0);
		*d_prev_vals[i] = 0;
		spinner<float>* spni = spn[i];
		spni->set_value (vals[i]);
	}
	am_depth = din0.am_depth;
	fm_depth = din0.fm_depth;
}

void menu::toggle () {

  show = !show;
  if (show) {

		//if (filtered) unfilter ();

    b_menu.set_text ("Close menu"); 
    style_listener* sl [] = {&gater_style_lis, &am_style_lis, &fm_style_lis};
    for (int i = 0; i < 3; ++i) sl[i]->get_style ();

    if (CRVED) {
			if (curve_picker.visible) curve_picker.hide();
			//if (curve_picker.visible) curve_picker.clicked (&curve_picker.b_cancel);
			curve_editor* ed = CRVED;
			//ed->stop_todo ();
			ed->setup_tools_menu ();
		}

    if (din0.adding_drones) din0.toggle_adding_drones (); 
		else if (din0.moving_drones) din0.toggle_moving_drones ();
    din0.finish_phrase_recording ();

    keybd2.turn_off_bend (); 

    screen_mousex = mousex;
    screen_mousey = mousey;
		::warp_mouse (menu_mousex, menu_mousey);

    //after_drone_selection ();
		//from_mondrian ();

		mondrian0.stop_doing_stuff ();

		scol.setup ();

		scope.enabled = 0;

		load_range (din0.dinfo.sel_range);
		

  } else {

    b_menu.set_text ("Menu");
    menu_mousex = mousex;
    menu_mousey = mousey;
		/*if (!filtered)*/ warp_mouse (screen_mousex, screen_mousey);
		scope.enabled = 1;

  }

	uis.update_bottom_line ();

}



void menu::set_ball_ops (ball* b) {

	ball_op* ops [ball_op::NUM_OPS] = {&b->op_turn, &b->op_speed, &b->op_teleport, &b->op_clone, &b->op_transform};
	checkbutton* cbn [ball_op::NUM_OPS] = {&cb_turn, &cb_speed, &cb_teleport, &cb_clone, &cb_transform};
	for (int i = 0; i < ball_op::NUM_OPS; ++i) cbn[i]->set_state (ops[i]->alarm.active, 0);

	turn& trn = b->op_turn;
	sp_turn_min.set_value (-trn.rd.min);
	sp_turn_max.set_value (trn.rd.max);

	speed& spd = b->op_speed;
	sp_speed_min.set_value (-spd.rd.min);
	sp_speed_max.set_value (spd.rd.max);
	sp_max_speed.set_value (spd.max);

	teleport& tel = b->op_teleport;
	sp_tel_radius.set_value (tel.radius);

	Clone& clo = b->op_clone;
	sp_clone_offset.set_value (clo.offset);
	sp_max_clones.set_value (clo.max);
	sp_max_balls.set_value (Clone::max_balls);
	cb_clone_can_clone.set_state (clo.clone_can_clone);

	Transform& tf = b->op_transform;

	ball_op* bpa [] = {&trn, &spd, &tel, &clo, &tf};
	spinner<float>* spa [] = {&sp_turn_every, &sp_speed_every, &sp_tel_every, &sp_clone_every, &sp_transform_every};
	for (int i = 0; i < ball_op::NUM_OPS; ++i) spa[i]->set_value (bpa[i]->alarm.triggert);
	
}

void menu::clear_ball_ops () {
	checkbutton* cbn [ball_op::NUM_OPS] = {&cb_turn, &cb_speed, &cb_teleport, &cb_clone, &cb_transform};
	for (int i = 0; i < ball_op::NUM_OPS; ++i) cbn[i]->set_state (0, 0);
}

void misc_listener::clicked (button& b) {
  if (&b == MENUP.b_exit_din) 
		try_quit ();
	else if (&b == MENUP.b_turn_off_ui) {
		turn_off_ui ();
		return;
	} /*else if (&b == &uis.b_close) {
		MENU.unfilter ();
	}*/ else din0.set_key_to_pitch_at_cursor ();
}

void menu::clicked (button& b) {
  if (&b == &b_menu) {
		cons << YELLOW << "You can use right click to open / close the menu and mouse wheel to scroll" << eol;
    toggle ();
  } 
  if (show) {
		extern const char* INSTRUMENTS[];
		for (int i = 0; i < n_inst_ed; ++i) { // only check for instrument or editors
			if (&b == items[i]) { // close menu when instrument or editor selected
				if (i < NUM_INSTRUMENTS) { // an instrument
					scope.save_current_instrument ();
					CURRENT_INSTRUMENT = i;
					INSTRUMENT = INSTRUMENTS[i];
					load_instrument ();
				} else { // an editor
					ui* ed = uis.uis [scr_id[i]];
					setup_tabs (ed);
					uis.set_current (ed);
					setup_plugin_labels ();
				}
				return;
			}
		}
  } 
}

void menu::move_items (int dx, int dy) {
	for (int i = 0; i < num_items; ++i) {
		widget* wi = items[i];
		wi->move (dx, dy, 0);
	}
	calc_bg ();
}

void menu::changed (checkbutton& tb) { // current tab has changed

  int dont_call_listener = 0;
  cur_tab = &tb;
  tb.turn_on (dont_call_listener);

  if (cur_tab == last_tab) return;

  if (last_tab) { // hide last tab memebers
    last_tab->turn_off (dont_call_listener);
    vector<widget*>& v = tab_members [last_tab];
    for (int i = 0, j = v.size (); i < j; ++i) v[i]->hide ();
  }

  vector<widget*>& v = tab_members [cur_tab];
  for (int i = 0, j = v.size (); i < j; ++i) v[i]->show ();
  last_tab = cur_tab;

	calc_bg ();

	// save last tab to reload when loading new instrument
	extern checkbutton* LAST_TABS [];
	checkbutton* com [] = {&cb_file, &cb_instrument, &cb_editors, &cb_ed_tools}; // ignore these tabs
	for (int i = 0; i < 4; ++i) if (com[i] == cur_tab) return;
	LAST_TABS [CURRENT_INSTRUMENT] = cur_tab;

	opacity = 0.7f;
	if (&tb == &cb_mon_ballops) {
		opacity = 0.5f;
	} else {
		if (&tb == &cb_mon_boxops || &tb == &cb_mon_misc) opacity = 0.5f;
	}

}

void menu::changed (field& f) {
  if (&f == &sp_dam_depth.f_value) {
		din0.change_drone_depth (modulator::AM, sp_dam_depth);
		//filter (MENUP.sp_dam_depth);
	} else if (&f == &sp_dfm_depth.f_value) {
		din0.change_drone_depth (modulator::FM, sp_dfm_depth);
		//filter (MENUP.sp_dfm_depth);
	} else if (&f == &sp_dam_bpm.f_value) {
		din0.change_drone_bpm (modulator::AM, sp_dam_bpm);
		//filter (MENUP.sp_dam_bpm);
	} else if (&f == &sp_dfm_bpm.f_value) {
		din0.change_drone_bpm (modulator::FM, sp_dfm_bpm);
		//filter (MENUP.sp_dfm_bpm);
	} else if (&f == &sp_browse_drone.f_value) {
		din0.browsed_drone = f;
		din0.browse_drone (0);
		//filter (MENUP.sp_browse_drone);
	} else if (&f == &sp_am_depth.f_value) {
		din0.change_depth (din::AM, sp_am_depth()); //float(f) - din0.am_depth);
  } else if (&f == &sp_fm_depth.f_value) {
		din0.change_depth (din::FM, sp_fm_depth()); //float(f) - din0.fm_depth);
  } else if (&f == &sp_am_bpm.f_value) {
		float v = f;
		v = din0.am.set_bpm (v);
		cons << YELLOW << "Voice AM bpm = " << v << eol;
  } else if (&f == &sp_fm_bpm.f_value) {
		float v = f;
		v = din0.fm.set_bpm (v);
		cons << YELLOW << "Voice FM bpm = " << v << eol;
  } else if (&f == &lf_delta_drone_master_volume.fld) {
    DELTA_DRONE_MASTER_VOLUME = f;
  } else if (&f == &sp_mesh_rows.f_value) {
    din0.dinfo.rows = f;
		if (cb_sync_rows_cols.state) {
			din0.dinfo.cols = din0.dinfo.rows; 
			sp_mesh_cols.set_value (din0.dinfo.cols);
		}
    mkb_selector.set_mesh (din0.create_mesh, din0.dinfo.rows, din0.dinfo.cols);
		picked (ol_mesh_point.option, 0);
  } else if (&f == &sp_mesh_cols.f_value) {
    din0.dinfo.cols = f;
		if (cb_sync_rows_cols.state) {
			din0.dinfo.rows = din0.dinfo.cols;
			sp_mesh_rows.set_value (din0.dinfo.rows);
		}
    mkb_selector.set_mesh (din0.create_mesh, din0.dinfo.rows, din0.dinfo.cols);
		picked (ol_mesh_point.option, 0);
  } else if (&f == &sp_mesh_dur.f_value) {
		float t = f;
		if (equals (t, 0.0f)) t = 0.0001;
		din0.dinfo.mesh_vars.duration = t;
		cons << "Make mesh in " << t << " seconds" << eol;
	} else if (&f == &f_mesh_xy) {
		int r, c;
		tokenizer z (f.text); 
		z >> r >> c;
		if (clamp (0, r, din0.dinfo.rows-1) || clamp (0, c, din0.dinfo.cols-1) ) {
			sprintf (BUFFER, "%d %d", r, c);
			f_mesh_xy.set_text(BUFFER);
		}
		proximity_orderer::ROW = r;
		proximity_orderer::COL = c;
	} else if (&f == &sp_bounces.f_value) {
		din0.dinfo.bounce.n += MENU.sp_bounces (); 
		clamp<int>(0, din0.dinfo.bounce.n, MILLION);
		cons << YELLOW << "Bounces = " << din0.dinfo.bounce.n << eol;
	} else if (&f == &sp_rebound.f_value) {
		din0.dinfo.bounce.speed += MENU.sp_rebound ();
		clamp<int>(0, din0.dinfo.bounce.speed, MILLION);
		cons << YELLOW << "Speed = " << din0.dinfo.bounce.speed << " %" << eol;
	} else if (&f == &sp_rotate_drones.f_value) {
		float r = -MENU.sp_rotate_drones () * PI_BY_180;
		din0.rotate_selected_drones (r);
	} else if (&f == &sp_scale_drones.f_value) {
		float s = MENU.sp_scale_drones (); 
		din0.scale_selected_drones (s);
	} else if (&f == &dp_spacing.f_value) {
		din0.dinfo.drone_pend.spacing = int(f);
		cons << YELLOW << "Spacing = " << din0.dinfo.drone_pend.spacing << eol;
	} else if (&f == &dp_bpm1.f_value) {
		din0.dinfo.drone_pend.start_bpm = float(f);
		cons << YELLOW << "First drone BPM = " << din0.dinfo.drone_pend.start_bpm << eol;
	} else if (&f == &dp_bpm2.f_value) {
		din0.dinfo.drone_pend.end_bpm = float(f);
		cons << YELLOW << "Last drone BPM = " << din0.dinfo.drone_pend.end_bpm << eol;
	}
}

void menu::changed (slider<float>& s) {
  din0.phrasor0.set_cur (s.get_val());
}

void menu::picked (label& lbl, int dir) {
  const static char* vct [] = {" Vertices desert tangents", " Vertices carry tangents"};
  const static char* mit [] = {" Tangents are not mirrored", " Tangents are mirrored"};
	const static char* bbs [] = {" Drones bounce forward", " Drones bounce back"};
	const static char *sut [] = {" Snap drones to notes", " Modulation affects velocity"};
	const static char *ofl [] = {" in ascending rows", " in descending rows"," in ascending columns", " in descending columns", " randomly", " nearest to", " farthest from" };
	const static char *wpt [] = {"bottom left", "bottom right", "top left", "top right", "center", "random point", "custom point"};
	const static char *cwt [] = {" Create Mesh", " Create Drone Pendulum"};
	const static char *ort [] = {" Orientation = Horizontal", " Orientation = Vertical"};
	const static char *mir [] = {" Flip about Horizontal", " Flip about Vertical" };

	static const int npt = 7, npt_1 = npt-1;
	static widget* mshw [] = {
		&sp_mesh_rows, 
		&sp_mesh_cols,
		&cb_sync_rows_cols,
		&l_drone_order,
		&sp_mesh_dur,
		&b_flip_rows_cols,
		&ol_drone_order,
		&ol_mesh_point,
		&f_mesh_xy,
	};
	static widget* dpw [] = {
		&dp_orient,
		&dp_spacing,
		&dp_bpm1,
		&dp_bpm2
	};

	if (&lbl == &ol_drone_master_volume.option) {
    din0.update_drone_master_volume (dir * DELTA_DRONE_MASTER_VOLUME);
    set_drone_master_volume ();
  } else if (&lbl == &ol_vertices_carry_tangents.option) {
    CRVED->carry_tangents = !CRVED->carry_tangents;
    lbl.set_text (vct[CRVED->carry_tangents]);
  } else if (&lbl == &ol_mirror_tangents.option) {
    CRVED->mirror_tangents = !CRVED->mirror_tangents;
    lbl.set_text (mit[CRVED->mirror_tangents]);
  } else if (&lbl == &ol_curve_style.option) {
    CRVED->toggle_curve_style ();
  } else if (&lbl == &ol_bounce_style.option) {
		din0.dinfo.bounce.style += dir;
		wrap<int> (din_info::bounce_t::FORWARD, din0.dinfo.bounce.style, din_info::bounce_t::BACK);
		lbl.set_text (bbs[din0.dinfo.bounce.style]);
	} else if (&lbl == &ol_set_unset_toggle.option) {
		din0.dinfo.set_unset_toggle = !din0.dinfo.set_unset_toggle;
		lbl.set_text (sut[din0.dinfo.set_unset_toggle]);
	} else if (&lbl == &ol_drone_order.option) {
		din0.dinfo.mesh_vars.order += dir;
		wrap (0, din0.dinfo.mesh_vars.order, LAST_ORDERER);
		lbl.set_text (ofl[din0.dinfo.mesh_vars.order]);
		ol_mesh_point.move (lbl.extents.right - ol_mesh_point.extents.left + 10, 0);
		if (din0.dinfo.mesh_vars.order > 4) {
			add_to_tab (&cb_mkb_drone_tools, &ol_mesh_point);
			add_to_tab (&cb_mkb_drone_tools, &f_mesh_xy);
		} else {
			remove_from_tab (&cb_mkb_drone_tools, &ol_mesh_point);
			remove_from_tab (&cb_mkb_drone_tools, &f_mesh_xy);
		}
	} else if (&lbl == &ol_mesh_point.option) {
		din0.dinfo.mesh_vars.point += dir;
		wrap (0, din0.dinfo.mesh_vars.point, npt_1);
		sprintf (BUFFER, " %s @ ", wpt[din0.dinfo.mesh_vars.point]);
		lbl.set_text (BUFFER);
		int cols_1 = din0.dinfo.cols - 1;
		int rows_1 = din0.dinfo.rows - 1;
		rnd<int> rdr (0, rows_1), rdc (0, cols_1);
		int ROW [] = {0, 0, rows_1, rows_1, din0.dinfo.rows / 2, rdr(), proximity_orderer::ROW};
		int COL [] = {0, cols_1, 0, cols_1, din0.dinfo.cols / 2, rdc(), proximity_orderer::COL};
		proximity_orderer::ROW = ROW [din0.dinfo.mesh_vars.point];
		proximity_orderer::COL = COL [din0.dinfo.mesh_vars.point];
		sprintf (BUFFER, "%d %d", proximity_orderer::ROW, proximity_orderer::COL);
		f_mesh_xy.set_text (BUFFER);
		f_mesh_xy.move (lbl.extents.right - f_mesh_xy.extents.left + 10, 0);
	} else if (&lbl == &ol_create_this.option) {
		din0.dinfo.create_this += dir;

		wrap (0, din0.dinfo.create_this, 1);
		int cw = din0.dinfo.create_this;
		lbl.set_text (cwt[cw]);
		if (cw) { // drone pendulum
			for (int i = 0; i < 9; ++i) remove_from_tab (&cb_mkb_drone_tools, mshw[i]);
			for (int i = 0; i < 4; ++i) add_to_tab (&cb_mkb_drone_tools, dpw[i]);
		} else { // mesh
			for (int i = 0; i < 4; ++i) remove_from_tab (&cb_mkb_drone_tools, dpw[i]);
			int j = 7; if (din0.dinfo.mesh_vars.order > 4) j = 9; 
			for (int i = 0; i < j; ++i) add_to_tab (&cb_mkb_drone_tools, mshw[i]);
		}
	} else if (&lbl == &dp_orient.option) {
		din0.dinfo.drone_pend.orient += dir;
		wrap (0, din0.dinfo.drone_pend.orient, 1);
		int o = din0.dinfo.drone_pend.orient;
		lbl.set_text (ort[o]);
	} else if (&lbl == &ol_mirror.option) {
		CRVED->axis += dir;
		wrap (0, CRVED->axis, 1);
		lbl.set_text (mir[CRVED->axis]);
		cb_selection_only.set_pos (lbl.extents.right + 10, lbl.extents.bottom);
	}
}

void menu::calc_bg () {
  if (cur_tab && num_tabs) {
    vector<widget*>& v = tab_members [cur_tab];
    if (v.size () == 0) return;
    widget* w0 = v[0]; 
		bg_extents.left = cb_file.extents.left;
		bg_extents.right = bg_extents.left;
    bg_extents.bottom = w0->extents.bottom; 
    bg_extents.top = tabs[0]->extents.top;
    for (int i = 0, j = v.size (); i < j; ++i) {
      widget* wi = v[i];
			if (wi->extents.left < bg_extents.left) bg_extents.left = wi->extents.left;
			if (wi->extents.right > bg_extents.right) bg_extents.right = wi->extents.right;
      if (wi->extents.bottom < bg_extents.bottom) bg_extents.bottom = wi->extents.bottom;
      if (wi->extents.top > bg_extents.top) bg_extents.top = wi->extents.top;
    }
		static const int GUTTER = 5;
		bg_extents.resize (GUTTER, GUTTER);
		/*ab_up.set_pos (bg_extents.right + GUTTER, bg_extents.midy);
		ab_down.set_pos (ab_up.extents.right, bg_extents.midy);*/
  }
}

void menu::set_drone_master_volume () {
  sprintf (BUFFER, " Drone master volume = %.3f", din0.drone_master_volume);
  ol_drone_master_volume.set_text (BUFFER);
}

void menu::show_editors (ui* inst) {
  int starts [] = {8, 0, 12, 15};
  int ends [] = {12, 8, 15, 16};
	int starti = starts[CURRENT_INSTRUMENT], endi = ends[CURRENT_INSTRUMENT];
  vector<widget*>& tw = tab_members [&cb_editors];
  for (int i = starti; i < endi; ++i) {
    widget* ei = editors[i];
    tw.push_back (ei);
  }
}

void menu::hide_editors () {
  vector<widget*>& tw = tab_members [&cb_editors];
  for (int i = 0; i < 16; ++i) {
    widget* ei = editors[i];
    vector<widget*>::iterator itr = find (tw.begin(), tw.end(), ei);
    if (itr != tw.end()) tw.erase (itr);
  }
}

void menu::update_bpm (const string& name, float value) {
  spinner<float>* psp = bpm_map [name];
  if (psp) psp->set_value (value);
}

void menu::mark_tap_target () {
  interpreter ("set taptarget");
  tokenizer tz (interpreter.result);
  int dont_call_listener = 0;
  while (1) {
    string target; tz >> target;
    if (target == "") break;
    if (target == "gr") cb_gater.turn_on (dont_call_listener);
    else if (target == "am") cb_am.turn_on (dont_call_listener);
    else if (target == "fm") cb_fm.turn_on (dont_call_listener);
    else if (target == "os") cb_octave_shift.turn_on (dont_call_listener);
  }
}

menu::~menu () {
	dlog << "--- destroying menu ---" << endl;
  widget_save ("d_menu", items, num_items);
	dlog << "--- destroyed menu ---" << endl;
}

void octave_shift_listener::clicked (button& b) {
  if (&b == MENUP.ab_octave_up || &b == &uis.ab_octave_up) modulate_up (); else modulate_down ();
}

void octave_shift_listener::changed (field& f) {
	float v = f;
  v = octave_shift.set_bpm (v);
	static const string los = "Octave shift BPM = ";
	cons << YELLOW << los << v << eol;
}

void voice_volume_listener::changed (field& f) {
	VOICE_VOLUME = f;
	static const string vv ("Voice volume = ");
	cons << YELLOW << vv << VOICE_VOLUME << eol;
}

void gater_bpm_listener::changed (field& f) {
	float v = f;
  v = din0.gatr.set_bpm (v);
	static const string gt = "Gater BPM = ";
	cons << YELLOW << gt << v << eol;
	//MENU.filter (MENUP.sp_gater_bpm);
}

void drone_handle_size_listener::changed (field& f) {
  din0.change_drone_handle_size (MENU.sp_change_drone_handle_size);
	//MENU.filter (MENUP.sp_change_drone_handle_size);
}

void drone_trail_length_listener::changed (field& f) {
  din0.change_drone_trail_points (MENU.sp_change_drone_trail_length);
	//MENU.filter (MENUP.sp_change_drone_trail_length);
}

void change_drone_vel_listener::changed (field& f) {
  din0.change_drone_vel (MENU.sp_change_drone_vel);
	//MENU.filter (MENUP.sp_change_drone_vel);
}

void change_drone_accel_listener::changed (field& f) {
	din0.change_drone_accel (MENU.sp_change_drone_accel);
	//MENU.filter (MENUP.sp_change_drone_accel);
}

void rotate_drone_vel_listener::changed (field& f) {
	din0.rotate_drone_vel (MENU.sp_rotate_drone_vel);
	//MENU.filter (MENUP.sp_rotate_drone_vel);
}

void drones_per_min_listener::changed (field& f) {
	din0.change_drones_per_min (MENU.sp_drones_per_min);
	//MENU.filter (MENUP.sp_drones_per_min);
}

void drone_lifetime_listener::changed (field& f) {
	din0.change_drone_lifetime (MENU.sp_drone_lifetime);
	//MENU.filter (MENUP.sp_drone_lifetime);
}

void orbit_insertion_time_listener::changed (field& f) {
	din0.change_orbit_insertion_time (MENU.sp_orbit_insertion_time);
	//MENU.filter (MENUP.sp_orbit_insertion_time);
}

void style_listener::set_style (const string& style) {
  for (int i = 0; i < num_styles; ++i) {
    if (styles[i] == style) {
      id = i;
      string command ("set-style " + what + " " + style);
      interpreter (command);
      oplist.set_text (prefix + style);
    }
  }
}

void style_listener::get_style () {
  string command ("get-style " + what);
  interpreter (command);
  oplist.set_text (prefix + interpreter.result);
}

void style_listener::next_style (int dir) {
  id += dir;
  if (id < 0) id = last_style; else if (id >= num_styles) id = 0;
  set_style (styles[id]);
}

void style_listener::picked (label& lbl, int dir) {
  next_style (dir);
}

void drone_commands_listener::clicked (button& b) {
	int toggle = 1;
	if (&b == MENUP.b_add_drones) din0.toggle_adding_drones (); else
	if (&b == MENUP.b_move_drones) din0.toggle_moving_drones (); else
	if (&b == MENUP.b_delete_drones) din0.delete_selected_drones (); else
	if (&b == MENUP.b_select_all_drones) {toggle = 0; din0.select_all_drones ();} else
  if (&b == MENUP.b_launch_drones) din0.make_launchers (); else
	if (&b == MENUP.b_orbit_selected_drones) din0.orbit_selected_drones (); else
	if (&b == MENUP.b_freeze_drones) din0.freeze_drones (); else
	if (&b == MENUP.b_thaw_drones) din0.thaw_drones (); else
	if (&b == MENUP.b_select_launchers) {toggle = 0; din0.select_launchers (); } else
	if (&b == MENUP.b_set_targets) din0.set_targets (); else
	if (&b == MENUP.b_select_attractors) {toggle = 0; din0.select_attractors (); } else
	if (&b == MENUP.b_select_attractees) {toggle = 0; din0.select_attractees (); } else
	if (&b == MENUP.b_stop_launching_drones) din0.destroy_launchers (); else
  if (&b == MENUP.b_move_drones_under_gravity) din0.set_drones_under_gravity (); else
	if (&b == MENUP.b_invert_drone_selection) {toggle = 0; din0.invert_selected_drones();} else
  if (&b == MENUP.b_track_drones) din0.make_trackers (); else
	if (&b == MENUP.b_select_tracked_drones) {toggle = 0; din0.select_tracked_drones (); } else
  if (&b == MENUP.b_track_gravity) din0.set_gravity_to_track_drone (); else
	if (&b == MENUP.ol_create_this.option) din0.toggle_create_this (); else
	if (&b == MENUP.b_clear_targets) din0.clear_targets(); else
	if (&b == MENUP.b_reset_velocity) din0.reset_drone_vel (); else
	if (&b == MENUP.b_reset_gravity) din0.dinfo.gravity.reset (MENU.cb_keep_size.state); else
  if (&b == MENUP.b_mute_drones) din0.mute_drones (); else
	if (&b == MENUP.b_flip_rows_cols) {
		int r = MENU.sp_mesh_rows.f_value, c = MENU.sp_mesh_cols.f_value;
		MENU.sp_mesh_rows.set_value (c);
		MENU.sp_mesh_cols.set_value (r);
		din0.dinfo.rows = c;
		din0.dinfo.cols = r;
		mkb_selector.set_mesh (din0.create_mesh, din0.dinfo.rows, din0.dinfo.cols);
		MENU.picked (MENU.ol_mesh_point.option, 0);
		toggle = 0;
	}
	if (toggle) MENU.toggle ();
}

void drone_commands_listener::changed (checkbutton& cb) {
  int state = cb.state;
	if (&cb == MENUP.cb_show_vel) din0.dinfo.vel = state; else
	if (&cb == MENUP.cb_show_accel) din0.dinfo.accel = state; else 
  if (&cb == MENUP.cb_show_gravity) {if (state) din0.dinfo.gravity.show (); else din0.dinfo.gravity.hide ();} else
	if (&cb == MENUP.cb_show_anchors) din0.dinfo.anchor = state; else
	if (&cb == MENUP.cb_select_launched) din0.auto_select_launched = state;
	else din0.dinfo.mesh_vars.sync = cb.state;
}

void phrase_commands_listener::clicked (button& b) {
	MENU.toggle ();
  if (&b == MENUP.b_record_phrase) din0.do_phrase_recording ();
  else if (&b == MENUP.b_clear_phrases) din0.clear_all_phrases ();
}

void range_data::read_mod () {
	int& a = din0.ranges[din0.dinfo.sel_range].mod.active;
	mod = a;
	a = 0;
}

void range_data::write_mod () {
	din0.ranges[din0.dinfo.sel_range].mod.active = mod;
}

void range_height_listener::clicked (button& b) {
	mouse_slider0.add (MENUP.rhl);
	MENU.rhl.read_mod ();
	activate_mouse_slider ();
}

void range_height_listener::moused (int dh) {
	din0.height_changed (din0.dinfo.sel_range, dh);
}

void range_height_listener::after_slide () {
	MENU.rhl.write_mod ();
}

void board_height_listener::read_mod () {
	int n = din0.num_ranges;
	moda.resize (n);
	for (int i = 0; i < n; ++i) {
		int& a = din0.ranges[i].mod.active;
		moda[i]=a;
		a = 0;
	}
}

void board_height_listener::write_mod () {
	int n = din0.num_ranges;
	for (int i = 0; i < n; ++i) din0.ranges[i].mod.active = moda[i];
}

void board_height_listener::moused (int dh) {
	din0.height_changed (-1, dh);
}

void board_height_listener::clicked (button& b) {
	MENU.bhl.name = "Board height";
	MENU.bhl.orient = mouse_slider_listener::Y;
	mouse_slider0.add (MENUP.bhl);
	MENU.bhl.read_mod ();
	activate_mouse_slider ();
}

void board_height_listener::after_slide () {
	MENU.bhl.write_mod ();
}

void set_range_listener::clicked (button& b) {
	if (&b == MENUP.b_selected_to_all) {
    din0.selected_range_to_all (i);
  } else if (&b == MENUP.b_default_to_selected) {
    din0.default_range_to_selected (i);
  } else if (&b == MENUP.b_default_to_all) {
    din0.default_range_to_all (i);
  } 
}

void set_range_listener::picked (label& lbl, int dir) {
	i = !i;
	static const char* opts [] = {" Set Width?", " Set Height?"};
	lbl.set_text (opts[i]);
}

void range_width_listener::clicked (button& b) {
	if (&b == MENUP.b_adjust_range_left) {
		MENU.arl.range = din0.dinfo.sel_range;
		MENU.arl.read_mod ();
		mouse_slider0.add (MENUP.arl);
		activate_mouse_slider ();
	} else if (&b == MENUP.b_adjust_range_right) {
		MENU.arr.range = din0.dinfo.sel_range;
		MENU.arr.read_mod ();
		mouse_slider0.add (MENUP.arr);
		activate_mouse_slider ();
	} else {
		MENU.arb.range = din0.dinfo.sel_range;
		MENU.arb.read_mod ();
		mouse_slider0.add (MENUP.arb);
		activate_mouse_slider ();
	}
}

void adjust_range_left_listener::moused (int dir) {
	if (din0.range_left_changed (range, dir, 1)) {
		din0.reposition_drones (0, range);
		din0.find_visible_ranges ();
	}
}

void adjust_range_left_listener::after_slide () {
	MENU.arl.write_mod ();
}

void adjust_range_right_listener::moused (int dir) {
	if (din0.range_right_changed (range, dir, 1)) {
		din0.reposition_drones (range, din0.last_range);
		din0.find_visible_ranges ();
	}
}

void adjust_range_right_listener::after_slide () {
	MENU.arr.write_mod ();
}

void adjust_range_both_listener::moused (int dir) {
	int rl = din0.range_left_changed (range, -dir, 1);
	int rr = din0.range_right_changed (range, dir, 1); 
	if (rl || rr) {
		din0.reposition_all_drones ();
		din0.find_visible_ranges ();
	}
}

void adjust_range_both_listener::after_slide () {
	MENU.arb.write_mod ();
}
void range_mod_lis::changed (field& f) {
	if (&f == MENUP.sp_range.f_value) {
		din0.dinfo.sel_range = f;
		MENU.load_range_mod ();
	} else if (&f == MENUP.sp_ran_mod_width.f_value) {
		float v = f;
		din0.ranges[din0.dinfo.sel_range].mod.fm.depth = v;
		sprintf (BUFFER, "Range %d, Modulation Width = %0.3f", din0.dinfo.sel_range, v);
		cons << BUFFER << eol;
	} else if (&f == MENUP.sp_ran_mod_width_bpm.f_value) {
		float v = f;
		din0.ranges[din0.dinfo.sel_range].mod.fm.bv.set_bpm (f, aout.samples_per_channel);
		sprintf (BUFFER, "Range %d, Modulation Width BPM = %0.3f", din0.dinfo.sel_range, v);
		cons << BUFFER << eol;
	} else if (&f == MENUP.sp_ran_mod_height.f_value) {
		float v = f;
		din0.ranges[din0.dinfo.sel_range].mod.am.depth = v;
		sprintf (BUFFER, "Range %d, Modulation Height = %0.3f", din0.dinfo.sel_range, v);
		cons << BUFFER << eol;
	} else if (&f == MENUP.sp_ran_mod_height_bpm.f_value) {
		float v = f;
		din0.ranges[din0.dinfo.sel_range].mod.am.bv.set_bpm (f, aout.samples_per_channel);
		sprintf (BUFFER, "Range %d, Modulation Height BPM = %0.3f", din0.dinfo.sel_range, v);
		cons << BUFFER << eol;
	} 
}

void range_mod_lis::clicked (button& b) {
	if (&b == MENUP.b_rm_start_all) {
#ifdef __EVALUATION__
		cons << RED << "Range modulation available only on the Licensed Version of DIN Is Noise" << eol;
		return;
#endif
		din0.set_ran_mod (1);
	} else if (&b == MENUP.b_rm_stop_all) {
		din0.set_ran_mod (0);
	} else if (&b == MENUP.b_rm_toggle) {
#ifdef __EVALUATION__
		cons << RED << "Range modulation available only on the Licensed Version of DIN Is Noise" << eol;
		return;
#endif
		din0.toggle_ran_mod ();
	} else if (&b == MENUP.b_rm_pause_resume) {
#ifdef __EVALUATION__
		cons << RED << "Range modulation available only on the Licensed Version of DIN Is Noise" << eol;
		return;
#endif
		din0.pause_resume_ran_mod ();
	} else {
		MENU.load_range (din0.current_range);
	}
}

void range_mod_lis::changed (checkbutton& cb) {

	if (&cb == MENUP.cb_mod_ran) {
#ifdef __EVALUATION__
		cons << RED << "Range modulation available only on the Licensed Version of DIN Is Noise" << eol;
		cb.set_state (!cb.state, 0);
		return;
#endif
		modulator& srm = din0.ranges[din0.dinfo.sel_range].mod;
		int& a = srm.active;
		if (a) a *= cb.state; else a = cb.state;
	} else 
		din0.dinfo.mark_sel_range = cb.state;
}

void range_mod_lis::edited (curve_editor* ed, int i) {
	din0.update_range_mod_solvers (i, ed->mix);
	curve_listener::edited (ed, i);
}

void menu::load_range (int r) {
	sp_range.set_value (r);
	rml.changed (sp_range.f_value);
}

void menu::load_range_mod () {
	range& ri = din0.ranges[din0.dinfo.sel_range];
	cb_mod_ran.set_state (ri.mod.active);
	sp_ran_mod_width.set_value (ri.mod.fm.depth);
	sp_ran_mod_width_bpm.set_value (ri.mod.fm.bv.bpm);
	sp_ran_mod_height.set_value (ri.mod.am.depth);
	sp_ran_mod_height_bpm.set_value (ri.mod.am.bv.bpm);
}

void snap_drones_listener::picked (label& l, int dir) {
	din0.dinfo.snap.style += dir;
	static const char* ss [] = {" Free", " Slide", " Lock", " Mirror"};
	wrap<int> (din_info::snap_t::FREE, din0.dinfo.snap.style, din_info::snap_t::MIRROR);
	l.set_text (ss[din0.dinfo.snap.style]);
}

void snap_drones_listener::changed (field& f) {
	if (&f == MENUP.sp_snap_left.f_value) {
		float dl = float(f) - din0.dinfo.snap.left;
		din0.dinfo.snap.left = f; 
		if (din0.dinfo.snap.style > din_info::snap_t::FREE) {
			if (din0.dinfo.snap.style == din_info::snap_t::SLIDE) {
				din0.dinfo.snap.right += dl;
				/*if (din0.dinfo.snap.right > 1.0f) {
					din0.dinfo.snap.right = 1.0f;
					din0.dinfo.snap.left -= dl;
				}*/
			} 
			else if (din0.dinfo.snap.style == din_info::snap_t::LOCK) {
				din0.dinfo.snap.right = din0.dinfo.snap.left;
			} 
			else {
				din0.dinfo.snap.right = 1.0f - din0.dinfo.snap.left;
			}
			MENU.sp_snap_right.set_value (din0.dinfo.snap.right);
		} else {
			/*if (clamp<float>(0.0f, din0.dinfo.snap.left, din0.dinfo.snap.right)) 
				MENU.sp_snap_left.set_value (din0.dinfo.snap.left);*/
		}
	} else if (&f == MENUP.sp_snap_right.f_value) {
		float dr = float(f) - din0.dinfo.snap.right;
		din0.dinfo.snap.right = f;
		if (din0.dinfo.snap.style > din_info::snap_t::FREE) {
			if (din0.dinfo.snap.style == din_info::snap_t::LOCK) {
				din0.dinfo.snap.left = din0.dinfo.snap.right;
			} else if (din0.dinfo.snap.style == din_info::snap_t::SLIDE) {
				din0.dinfo.snap.left += dr;
				/*if (din0.dinfo.snap.left < 0.0f) {
					din0.dinfo.snap.left = 0.0f;
					din0.dinfo.snap.right -= dr;
				}*/
			} else {
				din0.dinfo.snap.left = 1.0f - din0.dinfo.snap.right; // mirror
			}
			MENU.sp_snap_left.set_value (din0.dinfo.snap.left);
		} else {
			/*if (clamp<float>(din0.dinfo.snap.left, din0.dinfo.snap.right, 1.0f))
				MENU.sp_snap_right.set_value (din0.dinfo.snap.right);*/
		}
	}

	if (din0.dinfo.snap.left > din0.dinfo.snap.right || din0.dinfo.snap.left < 0.0f || din0.dinfo.snap.right > 1.0f) cons << RED; else cons << GREEN;
	sprintf (BUFFER, "Snap left = %0.3f, Snap right = %0.3f", din0.dinfo.snap.left, din0.dinfo.snap.right);
	cons << BUFFER << eol;
}

void scope_listener::changed (field& f) {
  int n = f;
  if (&f == MENUP.sp_scope_height.f_value) {
    scope.set_height (n);
		//MENU.filter (MENUP.sp_scope_height);
		static const string ht = "Height = ";
		cons << YELLOW << ht << n << eol;
  } else {
		scope.set_num_samples (n);
		static const string ns = "Samples = ";
		cons << YELLOW << ns << n << eol;
		//MENU.filter (MENUP.sp_scope_samples);
  }
}

void scope_listener::changed (checkbutton& cb) {
	scope.visible = cb.state;
}

void scope_listener::setup () {
	if (scope.visible) MENU.cb_scope.turn_on (0); else MENU.cb_scope.turn_off (0);
	MENU.sp_scope_height.set_value (scope.height);
	MENU.sp_scope_samples.set_value (scope.num_samples);
}

void tap_bpm_listener::changed (tap_display& td) {
  sprintf (BUFFER, "%.3f", td.bpm);
  MENU.l_tap_bpm_value.set_text (BUFFER);
  extern double TAP_BPM; TAP_BPM = td.bpm;
  Tcl_UpdateLinkedVar (interpreter.interp, "tapbpm");
}

void tap_bpm_listener::changed (checkbutton& cb) {
  checkbutton* cbs [] = {MENUP.cb_am, MENUP.cb_fm, MENUP.cb_gater, MENUP.cb_octave_shift};
  const char* targets [] = {"am", "fm", "gr", "os"};
  for (int i = 0; i < 4; ++i) {
    checkbutton* cbi = cbs[i];
    if (&cb == cbi) {
      if (cbi->state)
        sprintf (BUFFER, "add-tap-target %s", targets[i]); 
      else 
        sprintf (BUFFER, "remove-tap-target %s", targets[i]); 
      interpreter (BUFFER);
      return;
    }
  }
  if (&cb == MENUP.cb_auto_reset) {
    if (MENU.cb_auto_reset.state) interpreter ("set resetbeat 1"); else interpreter ("set resetbeat 0");
  }
}

void pan_zoom_listener::clicked (button& b) {
  if (&b == MENUP.abe_left) {CRVED->do_panx (1); cons << YELLOW << "You can press a to move curves left" << eol;}
  else if (&b == MENUP.abe_right) {CRVED->do_panx (-1); cons << YELLOW << "You can press d to move curves right"<< eol;}
  else if (&b == MENUP.abe_up) {CRVED->do_pany (-1); cons << YELLOW << "You can press w to move curves up" << eol;}
  else if (&b == MENUP.abe_down) {CRVED->do_pany (+1); cons << YELLOW << "You can press s to move curves down" << eol;}
  else if (&b == MENUP.pb_zoom_in) {CRVED->do_zoom (-1); cons << YELLOW << "You can press e to zoom in" << eol;}
  else {CRVED->do_zoom (+1);cons << YELLOW << "You can press q to zoom out" << eol;}
}

void snap_listener::changed (checkbutton& cb) {
  int dont_call_listener = 0;
  checkbutton* snaps [] = {MENUP.cb_snapnone, MENUP.cb_snapx, MENUP.cb_snapy, MENUP.cb_snapboth};
  int ids [] = {basic_editor::SNAP_NONE, basic_editor::SNAP_X, basic_editor::SNAP_Y, basic_editor::SNAP_BOTH};
  const char* mesgs [] = {"You can press n to turn off snapping", "You can press x to snap in X", 
                    "You can press y to snap in Y", "You can press b to snap in both X and Y"};
  for (int i = 0; i < 4; ++i) {
    checkbutton* si = snaps[i];
    si->turn_off (dont_call_listener);
    if (&cb == si) {
      CRVED->set_snap (ids[i]);
      cb.turn_on (dont_call_listener);
      cons << YELLOW << mesgs[i] << eol;
    }
  }
}

void menu::set_snap (int what) {
  int dont_call_listener = 0;
  checkbutton* snaps [] = {&cb_snapnone, &cb_snapx, &cb_snapy, &cb_snapboth};
  int ids [] = {basic_editor::SNAP_NONE, basic_editor::SNAP_X, basic_editor::SNAP_Y, basic_editor::SNAP_BOTH};
  for (int i = 0; i < 4; ++i) {
    checkbutton* si = snaps[i];
    si->turn_off (dont_call_listener);
    if (what == ids[i]) si->turn_on (dont_call_listener);
  }
}

void menu::set_vertices_carry_tangents (int i) {
  const static char* vct [] = {" Vertices desert tangents", " Vertices carry tangents"};
  ol_vertices_carry_tangents.option.set_text (vct[i]);
}

void menu::set_mirror_tangents (int i) {
  const static char* mit [] = {" Tangents are not mirrored", " Tangents are mirrored"};
  ol_mirror_tangents.option.set_text (mit[i]);
}

void menu::set_repeat (button** B, int n, double dt) {
  for (int i = 0; i < n; ++i) {
    button* bi = B[i];
    bi->click_repeat = 1;
    bi->first_repeat_time = bi->subsequent_repeat_time = dt;
  }
}

void menu::set_pan_repeat (double dt) {
  button* ab [] = {&abe_left, &abe_right, &abe_up, &abe_down, &abm_left, &abm_right, &abm_up, &abm_down};
  set_repeat (ab, 8, dt);
}

void menu::set_zoom_repeat (double dt) {
  button* zb [] = {&pb_zoom_in, &mb_zoom_out, &bm_zoom_in, &bm_zoom_out};
  set_repeat (zb, 4, dt);
}

/*void menu::filter (widget* w) {
	if (filtered || mouse_slider0.active) return;
	filtered = w;
	vector<widget*>& vw = uis.widgets_of [uis.current]; 
	vw.insert (vw.begin(), w);
	uis.b_close.set_pos (w->posx, w->posy - line_height);
	uis.b_close.show ();
	if (is_menu_visible()) toggle ();
}

void menu::unfilter () {
	if (filtered) {
		uis.remove (filtered);
		filtered = 0;
		uis.b_close.hide ();
		draw_slit_cutter (0);
	}
}*/

void curve_ops_listener::clicked (button& b) {
  int toggle = 1;
  if (&b == MENUP.b_undo) {
    cons << YELLOW << "You can press z to undo!" << eol;
    CRVED->do_undo ();
    toggle = 0;
  } else
  if (&b == MENUP.b_redo) {
    cons << YELLOW << "You can press LSHIFT + z to redo!" << eol;
    CRVED->do_redo ();
    toggle = 0;
  } else
  if (&b == MENUP.abl_left) {
		cons << YELLOW << "You can press 9 to load previous curve from library" << eol;
    CRVED->do_load_curve (-1);
    toggle = 0;
  } else
  if (&b == MENUP.abl_right) {
		cons << YELLOW << "You can press 0 to load next curve from library" << eol;
    CRVED->do_load_curve (+1);
    toggle = 0;
  }
  if (&b == MENUP.b_insert_vertex) {
    CRVED->insert_using_menu ();
  } else if (&b == MENUP.b_delete_vertex) {
    CRVED->remove_using_menu ();
  } else if (&b == MENUP.b_fold_tangents) {
    CRVED->fold_tangents_using_menu ();
  } else if (&b == MENUP.b_unfold_tangents) {
    CRVED->unfold_tangents_using_menu ();
	} else if (&b == MENUP.ol_mirror.option) {
    CRVED->mirror_using_menu ();
  } else if (&b == MENUP.b_copy) {
    CRVED->copy_using_menu ();
  } else if (&b == MENUP.b_paste) {
    CRVED->paste_using_menu ();
  } else if (&b == MENUP.b_swap_curves) {
		CRVED->swap ();
	} else if (&b == MENUP.b_pick_curve) {
    CRVED->do_pick_curve ();
  } 
  // to library
  else if (&b == MENUP.b_add_curve) {
    CRVED->add_curve ();
  } else if (&b == MENUP.b_replace_curve) {
    CRVED->replace_curve ();
  } else if (&b == MENUP.b_delete_curve) {
    CRVED->delete_curve ();
  } else if (&b == MENUP.b_draw_replacement_curve) {
    CRVED->draw_replacement_curve_using_menu ();
  } else if (&b == MENUP.b_start_capture) {
    CRVED->start_mouse_capture_from_menu ();
  } else if (&b == MENUP.b_assign_capture) {
    CRVED->assign_mouse_capture_from_menu ();
  } else if (&b == MENUP.b_stop_rotating) {
    MENU.sp_curve_rpm.set_value (0);
    CRVED->set_rpm (0);
  }
	if (toggle) MENU.toggle ();
}

void curve_ops_listener::changed (checkbutton& cb) {
	int tog = 0;
	if (&cb == MENUP.cb_mark_segments) {
		CRVED->mark_segments = cb.state;
	} else if (&cb == MENUP.cb_label_vertices) {
    CRVED->label_vertices = cb.state;
  } else if (&cb == MENUP.cb_show_waveform_samples) {
    CRVED->toggle_waveform_samples_display ();
  } else if (&cb == MENUP.cb_draw_curve) {
    CRVED->draw_curve_only = cb.state;
  } else {
		CRVED->overlay = cb.state;
		string n (get_current_instrument()->name);
		if (cb.state)
			cons << GREEN << "Overlaid the " << n << eol;  
		else 
			cons << RED << "Removed " << n << " from overlay." << eol;
		tog = 1;
	}
	if (tog) MENU.toggle ();
}

void curve_ops_listener::changed (field& f) {
  if (&f == MENUP.sp_waveform_hz.f_value) {
    CRVED->set_hz (f);
  } else if (&f == MENUP.sp_waveform_periods.f_value) {
    CRVED->set_periods (f);
  } else if (&f == MENUP.sp_curve_rpm.f_value) {
    CRVED->set_rpm (f);
		//MENU.filter (MENUP.sp_curve_rpm);
  } else if (&f == MENUP.sp_curve_limit.f_value) {
		//MENU.filter (MENUP.sp_curve_limit);
    CRVED->set_limit (f);
  } else {
		if (f.text == "") f.set_text ("nameless");
    CRVED->set_picked_curve_name (f.text);
  }
}

void recording_listener::clicked (button& b) {
	if (&b == MENUP.b_clear_record) {
		recorder0.clear ();
		uis.cb_record.set_text ("Record");
		MENU.cb_record.set_text ("Record");
		if (MENU.show) MENU.toggle ();
	} else {
#ifdef __EVALUATION__
		cons << RED << "Can save recordings only in the Licensed Version of DIN Is Noise" << eol;
		return;
#endif
		recorder0.start_saving ();
	}
}

void recording_listener::typing (field& f) {
	string fname (recorder0.folder + f.text);
	string cmd ("file exists " + fname); 
	interpreter (cmd); int result; stringstream ss; ss << interpreter.result; ss >> result;
	if (result) MENU.b_save.set_text ("Overwrite"); else MENU.b_save.set_text ("Save");
	recorder0.fname = f.text;
}

void recording_listener::changed (checkbutton& cb) {
	int state = cb.state;
	if (state == 0) { // show recording save section of file menu
		MENU.changed (MENU.cb_file);
		if (MENU.show == 0) MENU.toggle ();
	} else { // close file menu
		if (MENU.show == 1) MENU.toggle ();
	}
	dont_call_listener (uis.cb_record, state);
	dont_call_listener (MENU.cb_record, state);
}

void mondrian_listener::handle_split (int& var, int dir, float t) {
	switch (var) {
		case 0: // into 2 boxes
			mondrian0.split_rect (dir, t);
			break;
		case 1: // into notes
			mondrian0.multi_split_rect (dir);
			break;
		case 2: // into n x n grid
			mondrian0.multi_split_rect (mondrian0.num_boxes, dir);
	}
}

void mondrian_listener::clicked (button& b) {
	int toggle = 1;
  if (&b == MENUP.b_split_horizontal) handle_split (hsplit, split::HORIZONTAL, mondrian0.win.mousey);
  else if (&b == MENUP.b_split_vertical) handle_split (vsplit, split::VERTICAL, mondrian0.win.mousex); 
	else if (&b == MENUP.b_add_balls) mondrian0.do_add_balls (mondrian0.added_ball_type);
	else if (&b == MENUP.b_add_remove_slits) mondrian0.start_slitting ();
	else if (&b == MENUP.b_modulate_balls_up) {if (!mondrian0.modulate_balls (+1)) cons << RED << "Please select some balls!" << eol;}
	else if (&b == MENUP.b_modulate_balls_down) {if (!mondrian0.modulate_balls (-1)) cons << RED << "Please select some balls!" << eol;}
	else if (&b == MENUP.b_select_all_targets) {mondrian0.select_all_targets ();toggle=0;}
	else if (&b == MENUP.b_invert_selected_targets) {mondrian0.invert_selected_targets ();toggle=0;}
	else if (&b == MENUP.b_select_targets_in_box) {mondrian0.select_box_targets ();toggle=0;}
  else if (&b == MENUP.b_delete_box) mondrian0.delete_current_rect ();
	else if (&b == MENUP.b_delete_all_boxes) mondrian0.delete_all_rects = 1;
	else if (&b == MENUP.b_freeze_balls) mondrian0.freeze_balls (mondrian0.get_balls());
	else if (&b == MENUP.b_thaw_balls) mondrian0.thaw_balls (mondrian0.get_balls());
	else if (&b == MENUP.b_delete_all_targets) mondrian0.delete_all_targets ();
	else if (&b == MENUP.b_delete_selected_targets) mondrian0.delete_selected_targets ();
  else if (&b == MENUP.b_move_selected_balls) mondrian0.do_move_balls ();
	else if (&b == MENUP.b_toggle_wreckers) mondrian0.toggle_balls_type (ball::WRECKER);
	else if (&b == MENUP.b_toggle_healers) mondrian0.toggle_balls_type (ball::HEALER);
	else if (&b == MENUP.b_toggle_bouncers) mondrian0.toggle_balls_type (ball::BOUNCER);
	else if (&b == MENUP.b_switch_ball_type) mondrian0.switch_balls_type ();
	else if (&b == MENUP.b_select_wreckers) mondrian0.select_type (ball::WRECKER);
	else if (&b == MENUP.b_select_healers) mondrian0.select_type (ball::HEALER);
	else if (&b == MENUP.b_remove_slits_on_edge) mondrian0.remove_slits_on_current_edge ();
	else if (&b == MENUP.b_toggle_slit_anim) mondrian0.toggle_slit_anim ();
	else if (&b == MENUP.b_clear_modulations) mondrian0.clear_modulations (mondrian0.get_balls());
	else if (&b == MENUP.b_auto_change_direction_clockwise) {mondrian0.set_auto_rotate (-1);}
	else if (&b == MENUP.b_auto_change_direction_anti_clockwise) {mondrian0.set_auto_rotate (1);}
	else if (&b == MENUP.b_stop_auto_changing_direction) {mondrian0.set_auto_rotate (0);}
	else if (&b == MENUP.b_flip_direction) {mondrian0.flip_velocity();}
	else if (&b == MENUP.b_make_random_color) mondrian0.randomise_box_color();
	else if (&b == MENUP.b_make_note_grid) mondrian0.make_note_grid ();
	else if (&b == MENUP.b_make_nxn_grid) mondrian0.make_nxn_grid ();
	else if (&b == MENUP.abm_left) {mondrian0.do_panx (1); toggle=0;}
	else if (&b == MENUP.abm_right) {mondrian0.do_panx (-1); toggle=0;}
	else if (&b == MENUP.abm_up) {mondrian0.do_pany (+1);toggle=0;}
	else if (&b == MENUP.abm_down) {mondrian0.do_pany (-1); toggle=0;}
	else if (&b == MENUP.bm_zoom_in) {mondrian0.do_zoom(-1); toggle=0;}
	else if (&b == MENUP.bm_zoom_out) {mondrian0.do_zoom(+1); toggle=0;}
  if (toggle) MENU.toggle ();
}

void mondrian_listener::changed (checkbutton& cb) {
	if (&cb == MENUP.cb_auto_split_box) {
		mondrian0.auto_split_rect.active = cb.state;
	} else if (&cb == MENUP.cb_auto_delete_box) {
		mondrian0.auto_del_rect.active = cb.state;
	} else if (&cb == MENUP.cb_draw_boxes) {
		mondrian0.draw__boxes = cb.state;
	} else if (&cb == MENUP.cb_fill_boxes) {
		mondrian0.fill_boxes = cb.state;
	} else if (&cb == MENUP.cb_draw_notes) {
		mondrian0.draw__notes = cb.state;
	} else if (&cb == MENUP.cb_label_notes) {
		mondrian0.label_notes = cb.state;
	} else if (&cb == MENUP.cb_label_hz_vol) {
		mondrian0.label_hz_vol = cb.state;
	} else if (&cb == MENUP.cb_draw_ball_position) {
		mondrian0.draw_ball.position = cb.state;
	} else if (&cb == MENUP.cb_draw_ball_heading) {
		mondrian0.draw_ball.heading = cb.state;
	} else if (&cb == MENUP.cb_draw_ball_trails) {
		mondrian0.draw_ball.trails = cb.state;
	} else if (&cb == MENUP.cb_mondrian_auto_adjust_voices) {
		mondrian0.auto_adjust_voices = cb.state;
	}
}

void menu::from_mondrian () {

	/*dv_get<float>* lisf [] = {&nprl, &bsl, &batl, &bdtl, &bvl, &satl, &ssl};
	dv_get<int>* lisi [] = { &nppl, &tll};
	for (int i = 0; i < 7; ++i) lisf[i]->reset ();
	for (int i = 0; i < 2; ++i) lisi[i]->reset ();

	spinner<float>* spnf [] = {
		&sp_mondrian_change_attack_time, &sp_mondrian_change_decay_time, &sp_mondrian_change_speed, &sp_mondrian_change_vol, 
		&sp_mondrian_change_slit_anim_time, &sp_mondrian_change_note_poly_radius, &sp_mondrian_change_slit_size,
	};
	for (int i = 0; i < 7; ++i) spnf[i]->set_value (0.0f);
	spinner<int>* spni [] = {
		&sp_mondrian_change_note_poly_points, 
		&sp_mondrian_change_trail_size, 
		&sp_mondrian_change_dir,
	};
	for (int i = 0; i < 3; ++i) spni[i]->set_value (0);

	mondrian0.stop_doing_stuff ();*/

}

void ball_speed_listener::changed (field& f) {
	mondrian0.change_speed (MENU.sp_mondrian_change_speed, MENU.sp_mondrian_change_speed.dir_delta ());
}

void ball_direction_listener::changed (field& f) {
	mondrian0.rotate_velocity (MENU.sp_mondrian_change_dir.dir);
}

void ball_volume_listener::changed (field& f) {
	mondrian0.change_ball_vol_mult (MENU.sp_mondrian_change_vol);
}

void trail_length_listener:: changed (field& f) {
	mondrian0.change_trail_size (MENU.sp_mondrian_change_trail_size);
}

void ball_attack_time_listener:: changed (field& f) {
	mondrian0.change_attack_time_kb (MENU.sp_mondrian_change_attack_time);
}

void ball_decay_time_listener:: changed (field& f) {
	mondrian0.change_decay_time_kb (MENU.sp_mondrian_change_decay_time);
}

void slit_size_listener:: changed (field& f) {
	mondrian0.change_slit_size (MENU.sp_mondrian_change_slit_size);
}

void slit_anim_time_listener:: changed (field& f) {
	mondrian0.change_slit_anim_time (MENU.sp_mondrian_change_slit_anim_time);
}

void note_poly_radius_listener::changed (field& f) {
	mondrian0.set_note_poly_radius (float(f));
}

void note_poly_points_listener::changed (field& f) {
	mondrian0.set_note_poly_points (int(f));
}

void mondrian_listener::changed (field& f) {
	/*if (&f == MENUP.sp_mondrian_change_dir.f_delta) {
		mondrian0.delta_rotate_velocity = float (MENU.sp_mondrian_change_dir.f_delta);
		button* pb[] = {MENUP.sp_mondrian_change_dir.inc, MENUP.sp_mondrian_change_dir.dec};
		MENU.set_repeat (pb, 2, 0.005 * mondrian0.delta_rotate_velocity);
	}
	else */
	if (&f == MENUP.sp_mondrian_change_speed.f_delta) mondrian0.delta_speed = f;
	else if (&f == MENUP.sp_mondrian_min_voices.f_value) {
		mondrian0.min_voices = f;
		uis.update_bottom_line ();
		cons << YELLOW << "Min voices = " << mondrian0.min_voices << eol;
	} else if (&f == MENUP.sp_mondrian_num_boxes.f_value) {
		mondrian0.num_boxes = f;
		cons << YELLOW << "Number of boxes = " << mondrian0.num_boxes << eol;
	} else if (&f == MENUP.sp_auto_split_time.f_value) {
		mondrian0.auto_split_rect.triggert = f;
		cons << YELLOW << "Split box every = " << mondrian0.auto_split_rect.triggert << " secs" << eol;
	} else if (&f == MENUP.sp_auto_delete_time.f_value) {
		mondrian0.auto_del_rect.triggert = f;
		cons << YELLOW << "Delete box every = " << mondrian0.auto_del_rect.triggert << " secs" << eol;
	} else {
		mondrian::min_split_size = f;
		cons << YELLOW << "Min split size = " << mondrian::min_split_size << eol;
	}
}

void mondrian_listener::handle_auto_pick_box (options_list& ol, int dir, int& v) {
	v += dir;
	wrap<int> (rect::EARLIEST, v, rect::BALLED);
	ol.set_text (pick_box_types[v]);
}

void mondrian_listener::picked (label& lbl, int dir) {
	if (&lbl == MENUP.ol_auto_pick_box_split.option) {
		handle_auto_pick_box (MENU.ol_auto_pick_box_split, dir, mondrian0.split_leaf);
	} else if (&lbl == MENUP.ol_auto_pick_box_delete.option) {
		handle_auto_pick_box (MENU.ol_auto_pick_box_delete, dir, mondrian0.delete_leaf);
	} else if (&lbl == MENUP.ol_auto_split_at.option) {
		mondrian0.auto_split_at += dir;
		if (mondrian0.auto_split_at < split::NOTES) mondrian0.auto_split_at = split::ANYWHERE;
		else if (mondrian0.auto_split_at > split::ANYWHERE) mondrian0.auto_split_at = split::NOTES;
		MENU.ol_auto_split_at.set_text (auto_split_at_types [mondrian0.auto_split_at]);
	} else if (&lbl == MENUP.ol_auto_split_orient.option) {
		mondrian0.auto_split_orient += dir;
		if (mondrian0.auto_split_orient < split::HORIZONTAL) mondrian0.auto_split_orient = split::BOTH;
		else if (mondrian0.auto_split_orient > split::BOTH) mondrian0.auto_split_orient = split::HORIZONTAL;
		MENU.ol_auto_split_orient.set_text (auto_split_orient_types [mondrian0.auto_split_orient]);
	} else if (&lbl == MENUP.ol_ball_types.option) {
		mondrian0.added_ball_type += dir;
		if (mondrian0.added_ball_type < ball::BOUNCER) 
			mondrian0.added_ball_type = ball::HEALER;
		else if (mondrian0.added_ball_type > ball::HEALER) 
			mondrian0.added_ball_type = ball::BOUNCER;
		MENU.ol_ball_types.set_text (ball::types_str[mondrian0.added_ball_type]);
	} else if (&lbl == MENUP.ol_split_types_h.option) {
		hsplit += dir;
		check_split_type (MENU.ol_split_types_h, hsplit);
	} else if (&lbl == MENUP.ol_split_types_v.option) {
		vsplit += dir;
		check_split_type (MENU.ol_split_types_v, vsplit);
	} else if (&lbl == MENUP.ol_selection_targets.option) {
    mondrian0.clear_selected_targets ();
		mondrian0.sel_tar = !mondrian0.sel_tar;
		MENU.ol_selection_targets.set_text (selection_targets[mondrian0.sel_tar]);
		static const char* bb [] = {"Select all balls", "Select balls in box", "Invert selected balls", "Delete all balls", "Delete selected balls"};
		static const char* bs [] = {"Select all slits", "Select slits in box", "Invert selected slits", "Remove all slits", "Remove selected slits"};
		const char** pb [] = {bs, bb};
		button* bt [] = {
			MENUP.b_select_all_targets, MENUP.b_select_targets_in_box, 
			MENUP.b_invert_selected_targets, MENUP.b_delete_all_targets, 
			MENUP.b_delete_selected_targets
		};
		const char** cb = pb[mondrian0.sel_tar];
		for (int i = 0; i < 5; ++i) bt[i]->set_text (cb[i]);
	}
}

void mondrian_listener::check_split_type (options_list& ol, int& o) {
	if (o < 0) o = MAX_SPLIT_TYPES; else if (o > MAX_SPLIT_TYPES) o = 0;
	ol.set_text (split_types[o]);
}


binaural_drones_listener::binaural_drones_listener () : select_rule (GREATER_THAN_EQUAL), select_what (0) {
	val[EQUAL] = "0";
	val[GREATER_THAN_EQUAL] = val[LESSER_THAN_EQUAL]= "100";
	val[ID] = "1 2 1";
	val[RANGE] = "100 200";
	just = binaural_drone::CENTER;
}

void binaural_drones_listener::clicked (button& b) {

	if (&b == MENUP.bbd_select_all) {
		MENU.il_binaural_drones.select (1); // 1 == all
	} else if (&b == MENUP.bbd_select_none) {
		MENU.il_binaural_drones.select (0); // 0 == none
		MENU.il_binaural_drones.last = 0;
	} else if (&b == MENUP.bbd_invert_select) {
		MENU.il_binaural_drones.invert_select ();
		if (MENU.il_binaural_drones.num_selected() == 0) MENU.il_binaural_drones.last = 0;
	} else if (&b == MENUP.bbd_select2) {
		if (select_rule == ID) {
			tokenizer tv (MENU.bdf_value.text);
			int s, e, i; tv >> s >> e >> i;
			--s; --e;  // bcos starts at 1 on ui :(
			clamp (0, s, MENU.il_binaural_drones.n);
			clamp (0, e, MENU.il_binaural_drones.n);
			clamp (1, i, MENU.il_binaural_drones.n);
			sprintf (BUFFER, "wrap-get-nums %d %d %d", s, e, i);
		} else {
			const char* get [] = {"left", "right", "separation", "volume"};
			const char* op [] = {"==", ">=", "<=", "<>"};
			sprintf (BUFFER, "filter-binaurals %s %s %s", get[select_what], op[select_rule], MENU.bdf_value.text.c_str());
		}
		interpreter (BUFFER);
		tokenizer tz (interpreter.result);
		if (!MENU.il_binaural_drones.select_these (tz)) cons << RED << "No matching binaural drone paris found!" << eol;
	}

	if (binaural_drones0.busy ()) {
		cons << RED << "Still executing your command, please wait or ESC to abort" << YELLOW << eol;
		return;
	}

	binaural_drones0.separation = MENU.sp_bd_separation.f_value;
	binaural_drones0.master_volume = (float)MENU.lf_master_volume.fld / 100.0f; 
	binaural_drones0.pairs = MENU.sp_bd_pairs.f_value;
	binaural_drones0.close_octave = MENU.cb_close_octave.state;
	binaural_drones0.resize_separation = MENU.cb_resize_separation.state;

	const char* selerr = "Please select some binaural drone pairs";
	if (&b == MENUP.b_create_binaurals_on_notes) {

#ifdef __EVALUATION__
	cons << RED << "Available only in the Licensed Version of DIN Is Noise" << eol;
	return;
#endif

		cons << YELLOW << "Creating binaural drones [fading in], please wait" << eol;
		float tonic;
		if (binaural_drones0.keynote == i_binaural_drones::START_PITCH) 
			tonic = binaural_drones0.starting_pitch; 
		else 
			tonic = get_tonic (&binaural_drones0);
		string intervals; 
		vector<string>& notes = binaural_drones0.scaleinfo.notes;
		int j = notes.size (); if (binaural_drones0.close_octave == 0) --j;
		for (int i = 0; i < j; ++i) intervals = intervals + notes[i] + " ";
		stringstream cmd;
		cmd << "create-binaurals-on-notes " << tonic << ' ' << binaural_drones0.separation << " {" << intervals << "}" << ' ' << binaural_drones0.resize_separation;
		interpreter (cmd.str());
		MENU.changed (MENU.cb_binaural_drones_edit);
	} else if (&b == MENUP.b_create_binaurals_from_pitch) {

#ifdef __EVALUATION__
	if (binaural_drones0.num_binaural_drones || binaural_drones0.pairs > 1) {
		cons << RED << "Can create > 1 binaural drone pair in the Licensed Version of DIN Is Noise" << eol;
		return;
	}
#endif

		cons << YELLOW << "Creating binaural drones [fading in], please wait" << eol;
		string start_pitch = MENU.lf_bd_start_pitch.fld.text;
		string spacing = MENU.lf_bd_spacing.fld.text;
		sprintf (BUFFER, "create-binaurals-from-pitch %f %f %d %f", binaural_drones0.starting_pitch, binaural_drones0.separation, binaural_drones0.pairs, binaural_drones0.spacing); 
		interpreter (BUFFER);
		MENU.changed (MENU.cb_binaural_drones_edit);
	} else if (&b == MENUP.bbd_sync) {
		if (MENU.il_binaural_drones.num_selected ()) {
			cons << YELLOW << "Syncing (fade-out + sync + fade-in) : please wait or ESC to abort" << eol;
			interpreter ("sync-selected-binaurals");
		} else {
			cons << RED << selerr << eol;
		}
	} else if (&b == MENUP.bbd_delete) {
		if (MENU.il_binaural_drones.num_selected ()) {
			cons << YELLOW << "Deleting (after fade-out) : please wait or ESC to abort" << eol;
			interpreter ("delete-selected-binaurals");
		} else {
			cons << RED << selerr << eol;
		}
	} else if (&b == MENUP.bd_modulate_up) {
		if (MENU.il_binaural_drones.num_selected ()) {
			cons << YELLOW << "Modulating up: please wait or ESC to abort" << eol;
			sprintf (BUFFER, "modulate-selected-binaurals %f", binaural_drones0.modulation_amount);
			interpreter (BUFFER);
		} else {
			cons << RED << selerr << eol;
		}
	} else if (&b == MENUP.bd_modulate_down) {
		if (MENU.il_binaural_drones.num_selected ()) {
			cons << YELLOW << "Modulating down: please wait or ESC to abort" << eol;
			sprintf (BUFFER, "modulate-selected-binaurals %f", 1.0f / binaural_drones0.modulation_amount);
			interpreter (BUFFER);
		} else {
			cons << RED << selerr << eol;
		}
	} else if (&b == MENUP.bbd_flip) {
		if (MENU.il_binaural_drones.num_selected ()) {
			for (int i = 0; i < MENU.il_binaural_drones.n; ++i) {
				if (MENU.il_binaural_drones.sel[i]) {
					binaural_drone* bi = binaural_drones0.binaural_drones[i];
					float r_hz = bi->l_hz, l_hz = bi->r_hz;
					bi->set_hz (binaural_drone::LEFT, l_hz);
					bi->set_hz (binaural_drone::RIGHT, r_hz);
				}
			}
			binaural_drones0.pitch_fader.start ("Pitch flip");
			cons << YELLOW << "Flipping Hz: please wait or ESC to abort" << eol;
			interpreter ("flip-selected-binaurals");
		} else {
			cons << RED << selerr << eol;
		}
	}
}

void binaural_drones_listener::changed (field& f) {
	float v = float(f);
	if (&f == MENUP.lf_bd_start_pitch.fld) {
		binaural_drones0.starting_pitch = v;
	} else if (&f == MENUP.lf_master_volume.fld) {
		float ov = binaural_drones0.master_volume * 100.0f;
		float mv = v / 100.0f;
		binaural_drones0.master_volume = mv;
		stringstream cmd;
		cmd << "set-all-binaurals-volume " << mv;
		interpreter (cmd.str());
		sprintf (BUFFER, "Master Volume from %0.2f%% to %0.2f%% : please wait or ESC to abort", ov, v);
		cons << YELLOW << BUFFER << eol;
	} else if (&f == MENUP.lf_vol_fade_time.fld) {
		binaural_drones0.vol_fader.set_duration (v);
	} else if (&f == MENUP.lf_pitch_fade_time.fld) {
		binaural_drones0.pitch_fader.set_duration (v);
	} else if (&f == MENUP.lf_modulation_amount.fld) {
		if (v < 1.0f) {
			v = 1.0f;
			MENU.lf_modulation_amount.fld.set_text (v);
		}
		binaural_drones0.modulation_amount = v;
	} else if (&f == MENUP.lf_bd_spacing.fld) {
		binaural_drones0.spacing = v;
	} else if (&f == MENUP.bdf_value) {
		val[select_rule] = MENU.bdf_value.text;
		MENU.bdl.clicked (MENU.bbd_select2);
	} else if (&f == MENUP.lf_vol.fld) {
		float vp = v / 100.0f;
		sprintf (BUFFER, "set-selected-binaurals-volume %f", vp);
		interpreter (BUFFER);
	} else if (&f == MENUP.lf_l.fld) {
		set_hz (binaural_drone::LEFT, v);
	} else if (&f == MENUP.lf_r.fld) {
		set_hz (binaural_drone::RIGHT, v);
	} else if (&f == MENUP.lf_sep.fld) {
		int j = 0;
		for (int i = 0; i < MENU.il_binaural_drones.n; ++i) {
			if (MENU.il_binaural_drones.sel[i]) {
				binaural_drone* bi = binaural_drones0.binaural_drones[i];
				bi->set_sep (v);
				++j;
			}
		}
		if (j) 
			binaural_drones0.pitch_fader.start ("Separation Hz set");
		else 
			cons << RED << "Please select some binaural drone pairs" << eol;
	}
}

void binaural_drones_listener::set_hz (int w, float v) {

	int n = MENU.il_binaural_drones.num_selected ();
	if (n == 0) {
		cons << RED << "Please select some binaural drone pairs" << eol;
		return;
	}

	if (n == 1) {
		int i = MENU.il_binaural_drones.get_first (); 
		binaural_drone* bi = binaural_drones0.binaural_drones[i];
		bi->set_hz (w, v); // v is absolute
		binaural_drones0.pitch_fader.start ("Hz set");
	} else {
		for (int i = 0; i < MENU.il_binaural_drones.n; ++i) {
			if (MENU.il_binaural_drones.sel[i]) {
				binaural_drone* bi = binaural_drones0.binaural_drones[i];
				float ohz [] = {bi->l_hz, bi->r_hz};
				bi->set_hz (w, ohz[w] + v); // v is relative
			}
		}
		binaural_drones0.pitch_fader.start ("Hz change");
	}
}

void binaural_drones_listener::picked (label& lbl, int dir) {
	if (&lbl == MENUP.ol_justification.option) {
		int j = binaural_drones0.change_justification (dir);
		MENU.ol_justification.set_text (justs[j]);
	} else if (&lbl == MENUP.ol_key_note.option) {
		int k = binaural_drones0.change_key_note (dir);
		const string kn [] = {"start pitch", "from scale"};
		MENU.ol_key_note.set_text (" Key note is " + kn[k]);
	} else if (&lbl == MENUP.ol_select_what.option) {
		select_what += dir;
		wrap<int> (binaural_drone::LEFT, select_what, binaural_drone::VOLUME);
		const string sc [] = {"L ", "R ", "Separation ", "Volume "};
		MENU.ol_select_what.set_text (sc[select_what]);
		MENU.ol_select_rule.set_pos (MENU.ol_select_what.extents.right, MENU.ol_select_rule.posy);
		MENU.bdf_value.set_pos (MENU.ol_select_rule.extents.right, MENU.bdf_value.posy);
	} else if (&lbl == MENUP.ol_select_rule.option) {
		select_rule += dir;
		wrap<int> (EQUAL, select_rule, ID);
		const string sr [] = {" = ", " >= ", " <= ", " <> ", " id "};
		MENU.ol_select_rule.set_text (sr[select_rule]);
		MENU.bdf_value.set_text (val[select_rule]);
		MENU.bdf_value.set_pos (MENU.ol_select_rule.extents.right, MENU.bdf_value.posy);
	} else if (&lbl == MENUP.ol_just.option) {
		just += dir;
		wrap<int> (binaural_drone::LEFT, just, binaural_drone::CENTER);
		MENU.ol_just.set_text (justs[just]);
		for (int i = 0; i < MENU.il_binaural_drones.n; ++i) {
			if (MENU.il_binaural_drones.sel[i]) {
				binaural_drone* bi = binaural_drones0.binaural_drones[i];
				bi->set_just (just);
			}
		}
	}
}

void binaural_drones_listener::selected (item_list& il, int s) {
	int ns = il.num_selected ();
	if (ns == 1) {
		binaural_drone* bs = binaural_drones0.binaural_drones[s];
		bs->sel = 1;
		sprintf (BUFFER, "%0.3f", bs->vol*100.0);
		MENU.lf_vol.set_text (BUFFER);
		sprintf (BUFFER, "%0.3f", bs->l_hz);
		MENU.lf_l.set_text (BUFFER);
		sprintf (BUFFER, "%0.3f", bs->r_hz);
		MENU.lf_r.set_text (BUFFER);
		sprintf (BUFFER, "%0.3f", bs->sep);
		MENU.lf_sep.set_text (BUFFER);
		MENU.lf_l.set_label ("L (Hz) ");
		MENU.lf_r.set_label ("R (Hz) ");
		just = bs->just;
		MENU.ol_just.set_text (justs[just]);
	} else {
		if (ns) {
			MENU.lf_l.set_label ("dL (Hz) ");
			MENU.lf_r.set_label ("dR (Hz) ");
		}
		MENU.lf_vol.fld.set_text (0.0f);
		MENU.lf_l.fld.set_text (0.0f);
		MENU.lf_r.fld.set_text (0.0f);
		MENU.lf_sep.fld.set_text (0.0f);
		just = binaural_drone::CENTER;
		MENU.ol_just.set_text (justs[just]);
		for (int i = 0, n = il.n; i < n; ++i) {
			binaural_drone* bi = binaural_drones0.binaural_drones[i];
			bi->sel = il.sel[i];
		}
	}
	cons << GREEN << "Selected " << ns << " binaural drone pairs" << eol;
}

void binaural_drones_listener::changed (checkbutton& cb) {
	binaural_drones0.close_octave = MENU.cb_close_octave.state;
}

void menu::update_binaurals_list () {
	il_binaural_drones.repos (cb_file.extents.left, bbd_select_all.extents.bottom);
	calc_bg ();
}

ball_ops_listener::ball_ops_listener () {
	op_id = 0;
}

void ball_ops_listener::picked (label& lbl, int dir) {
	label* olt [] = {MENUP.ol_bouncer.option, MENUP.ol_wrecker.option, MENUP.ol_healer.option};
	for (int i = 0; i < 3; ++i) {
		if (&lbl == olt[i]) {
			int& t = Transform::rules [i];
			t += dir;
			if (t < ball::BOUNCER) t = ball::INVALID;
			else if (t > ball::INVALID) t = ball::BOUNCER;
			sprintf (BUFFER, "%s becomes %s", ball::types_str[i], ball::types_str[t]);
			olt[i]->set_text (BUFFER);
			return;
		}
	}
	if (mondrian0.browse.n) mondrian0.browse_ball (dir);
}

void ball_ops_listener::clicked (button& b) {
}

void ball_ops_listener::changed (checkbutton& cb) {
	ball* b = mondrian0.get_one_selected_ball ();
	if (b) {
		ball_op* ops [] = {&b->op_turn, &b->op_speed, &b->op_teleport, &b->op_clone, &b->op_transform};
		checkbutton* cbn [] = {MENUP.cb_turn, MENUP.cb_speed, MENUP.cb_teleport, MENUP.cb_clone, MENUP.cb_transform};
		for (int i = 0; i < ball_op::NUM_OPS; ++i) {
			if (&cb == cbn[i]) {
				ball_op* opi = ops[i];
				if (cb.state) opi->alarm.start (); else opi->alarm.stop ();
				break;
			}
		}

		if (cb.state && (&cb == MENUP.cb_speed)) {
			MENU.sp_max_speed.set_value (b->V);
			b->op_speed.max = b->V;
		}

		b->op_clone.clone_can_clone = MENU.cb_clone_can_clone.state;

	} else {
		cons << RED << "Please select a ball!" << eol;
	}
}

void ball_ops_listener::changed (field& f) {
	if (&f == MENUP.sp_max_balls.f_value) {
		int i = f;
		Clone::max_balls = i;
		cons << YELLOW << "Max balls = " << Clone::max_balls << eol;
	} else {
		ball* b = mondrian0.get_one_selected_ball ();
		if (b) {
			if (&f == MENUP.sp_turn_every.f_value) {
				float t = f;
				b->op_turn.alarm.triggert = t;
				sprintf (BUFFER, "Turn every %0.3f seconds", t);
				cons << YELLOW << BUFFER << eol;
			} else 
			if (&f == MENUP.sp_turn_min.f_value) {
				float minn = f, maxx;
				if (MENU.cb_turn_sync.state) {
					maxx = minn; 
					MENU.sp_turn_max.set_value (maxx);
				} else maxx = b->op_turn.rd.max;
				b->op_turn.rd.set (-minn, maxx);
				sprintf (BUFFER, "Turn Clockwise upto %0.3f degrees | Anti-clockwise upto %0.3f degrees", minn, maxx);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_turn_max.f_value) {
				float minn, maxx = f;
				if (MENU.cb_turn_sync.state) {
					MENU.sp_turn_min.set_value (maxx);
					minn = -maxx; 
				} else minn = b->op_turn.rd.min;
				b->op_turn.rd.set (minn, maxx);
				sprintf (BUFFER, "Turn Clockwise upto %0.3f degrees | Anti-clockwise upto %0.3f degrees", -minn, maxx);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_speed_min.f_value) {
				float minn = f, maxx;
				if (MENU.cb_speed_sync.state) {
					maxx = minn; 
					MENU.sp_speed_max.set_value (maxx);
				} else maxx = b->op_speed.rd.max;
				b->op_speed.rd.set (-minn, maxx);
				sprintf (BUFFER, "Brake = %0.3f | Accelerate = %0.3f", minn, maxx);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_speed_max.f_value) {
				float minn, maxx = f;
				if (MENU.cb_speed_sync.state) {
					MENU.sp_speed_min.set_value (maxx);
					minn = -maxx; 
				} else minn = b->op_speed.rd.min;
				b->op_speed.rd.set (minn, maxx);
				sprintf (BUFFER, "Accelerate = %0.3f | Brake = %0.3f", maxx, -minn);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_speed_every.f_value) {
				float m = f;
				b->op_speed.alarm.triggert = m;
				sprintf (BUFFER, "Speed every %0.3f seconds", m);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_max_speed.f_value) {
				float m = f;
				b->op_speed.max = m;
				sprintf (BUFFER, "Max speed = %0.3f", m);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_tel_every.f_value) {
				float s = f;
				b->op_teleport.alarm.triggert = s;
				sprintf (BUFFER, "Teleport every %0.3f seconds", s);
				cons << YELLOW << BUFFER << eol;
			} else 
			if (&f == MENUP.sp_tel_radius.f_value) {
				float r = f;
				b->op_teleport.radius = r;
				sprintf (BUFFER, "Max Teleport distance = %0.3f", r);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_clone_every.f_value) {
				float m = f;
				b->op_clone.alarm.triggert = m;
				sprintf (BUFFER, "Clone every %0.3f seconds", m);
				cons << YELLOW << BUFFER << eol;
			} else
			if (&f == MENUP.sp_max_clones.f_value) {
				int i = f;
				b->op_clone.n = b->op_clone.max = i;
				cons << "Max clones = " << i << eol;
			} else
			if (&f == MENUP.sp_clone_offset.f_value) {
				float m = f;
				b->op_clone.offset = m;
				cons << YELLOW << "Clone offset = " << m << eol;
			} else
			if (&f == MENUP.sp_transform_every.f_value) {
				float m = f;
				b->op_transform.alarm.triggert = m;
				sprintf (BUFFER, "Transform every %0.3f seconds", m);
				cons << YELLOW << BUFFER << eol;
			}
		} else {
			cons << RED << "Please select a ball! " << eol;
		}
	}
}

void arrow_uv_lis::changed (field& f) {
	extern float ARROW_U, ARROW_V;
	if (&f == MENUP.sp_arrow.depth.f_value) {
		ARROW_U = f;
		sprintf (BUFFER, "Drone Arrow Depth = %0.3f", ARROW_U);
		cons << BUFFER << eol;
	}
	else {
		ARROW_V = f;
		sprintf (BUFFER, "Drone Arrow Width = %0.3f", ARROW_V);
		cons << BUFFER << eol;
	}
}

void range_defaults_lis::changed (field& f) {
	if (&f == MENUP.sp_default_width.f_value) {
		extern int WIDTH;
		WIDTH = f;
		cons << "Default Range Width = " << WIDTH << eol;
	} else {
		extern int HEIGHT;
		HEIGHT = f;
		cons << "Default Range Height = " << HEIGHT << eol;
	}
}

void change_note_listener::moused (int dx) {
	int dir = dx / abs(dx);
	din0.change_range_note (id, dir);
}

void change_note_listener::clicked (button& b) {
	if (&b == MENUP.b_change_note_left) {
		mouse_slider0.add (MENUP.cnl);
	} else {
		mouse_slider0.add (MENUP.cnr);
	}
	activate_mouse_slider ();
}

void change_note_style_listener::picked (label& lbl, int dir) {
	din0.dinfo.change_note_style = !din0.dinfo.change_note_style;
	set (lbl, din0.dinfo.change_note_style);
}

void change_note_style_listener::set (label& lbl, int v) {
	lbl.set_text (din_info::cns_styles[v]);
	extern scale_info all_notes;
	scale_info* ptr_si [] = {&din0.scaleinfo, &all_notes};
	din0.ptr_scaleinfo = ptr_si[v];
}

void set_unset_toggle_listener::clicked (button& b) {
	if (&b == MENUP.b_toggle) {
		if (din0.dinfo.set_unset_toggle) din0.mod_afx_vel (-1); else din0.snap_drones (-1);
	} else if (&b == MENUP.b_set) {
		if (din0.dinfo.set_unset_toggle) din0.mod_afx_vel (1); else din0.snap_drones (1);
	} else {
		if (din0.dinfo.set_unset_toggle) din0.mod_afx_vel (0); else din0.snap_drones (0);
	}
}

void pitch_vol_dis_pix_lis::changed (checkbutton& cb) {
	if (&cb == MENUP.cb_pitch_dis) {
		din0.dinfo.dist.pitch = cb.state;
	} else {
		din0.dinfo.dist.vol = cb.state;
	}
}

void pitch_vol_dis_pix_lis::changed (field& f) {
	din0.dinfo.dist.pix = f;
	cons << "Pixels per level = " << din0.dinfo.dist.pix << eol;
}
