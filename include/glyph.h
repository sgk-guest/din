/*
* glyph.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __glyph
#define __glyph

#include <vector>
#include "line.h"

struct glyph {

  std::vector<line> lines;
  int width, height;

  glyph (const std::vector<line>& vl);
  glyph (int w = 3, int h = 8) {width = w; height = h;}
  void find_width_height ();
	int num_points ();

};

#endif



