/*
* bit_display.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __BIT_DISPLAY
#define __BIT_DISPLAY

#include "widget.h"
#include <vector>

struct ucolor_t;

struct bit_display : widget {

	int d_width; // display width of each bit
	int xstart, xend; // start, end of display

	std::vector<unsigned char> bits; // the bits
	int num_bits;
	int last_bit;
	int cur_id; // mouse @
	int prev_id;
	change_listener<bit_display>* lsnr;

	bit_display (int w = 256, int h = 64);
	void set_pos (int x, int y);
	void set_size (int w, int h);
	void set (void* bytes, int num_bytes);
	int handle_input ();
	void draw ();
	void calc_visual_params ();
	int find_bit_id ();
	void get_color (ucolor_t* uc);
	template <typename T> void get_data (T* ptr_) {
		unsigned char* ptr = (unsigned char*) ptr_;
		int num_bytes = sizeof (T);
		for (int i = 0, j = num_bits - 1; i < num_bytes; ++i) {
			unsigned char uci = 0;
			for (int k = 0; k < 8; ++k) {
				unsigned char bj = bits[j--];
				uci = uci | (bj << k);
			}
			ptr[i] = uci;
		}
	}
};

#endif



