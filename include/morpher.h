/*
* morpher.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __MORPHER
#define __MORPHER

#include "spinner.h"
#include "plugin.h"
#include "curve_display.h"
#include "solver.h"
#include "slider.h"

struct morpher : plugin, change_listener< slider<float> > {

	label l_source, l_target;
	curve_display cd_source, cd_target;
	button b_source, b_target;
	slider<float> sl_amount;
	spinner<int> sp_points;
	slider<float> sl_src_start, sl_tar_start;

	multi_curve source, target;
	solver source_sol, target_sol;
	std::vector < point<float> > source_pts, target_pts;

	int num_points;
	float amount;
	int src_start, tar_start;

  morpher ();
  ~morpher ();

	void assign_curve (multi_curve& crv, solver& sol, curve_display& crvdis);
	void make_points ();
	void make_classic_pts (std::vector < point<float> >& pts, solver& sol);
	void make_shapeform_pts (std::vector < point<float> >& pts, solver& sol, multi_curve& crv);
	void clicked (button& b);
	void changed (field& f);
	void changed (slider<float>& s);
  void load_params ();
  void save_params ();
  void setup ();
	void render ();
	int apply (multi_curve& crv);

};

#endif



