/*
* scale_info.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __SCALE_INFO
#define __SCALE_INFO

#include <string>
#include <vector>
#include <map>

struct checkbutton;
struct scale_info;

struct scale_listener {
	virtual void scale_loaded () = 0;
	virtual void scale_changed () = 0;
	virtual void tonic_changed () = 0;
};

struct scale_info {

  std::string name; // name of scale

  float lo_tonic, tonic, hi_tonic;

  std::map <std::string, float> intervals; // custom intervals for this scale

  std::vector<std::string> notes; // notes of the scale
  int num_notes; 
	int last_note;
	int second_last_note;
	void set_num_notes (int nn);

	struct nearest_note_t {
		std::string name;
		float freq;
		float distance;
		nearest_note_t () : freq (0), distance (0) {}
	} nearest_note;

	int western; // C = 0, C# = 1, D = 2, D# = 3, E = 4, F = 5, F# = 6, G = 7, G# = 8, A = 9, A# = 10, B = 11

	// range is microtonal space between two notes
  int num_ranges; 
  int last_range;

	scale_listener* scl;

  scale_info () {
		lo_tonic = tonic = hi_tonic = 0.0f;
    num_notes = last_note = second_last_note = 0;
    num_ranges = 0;
    last_range = -1;
		western = 0;
		scl = 0;
  }

  int set_tonic (float f);

  int load_scale (const std::string& n);
  int save_scale ();

  void update (checkbutton* cb_notes);
	void update_settings ();

};

#endif



