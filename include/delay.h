/*
* delay.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __delay
#define __delay

#include "multi_curve.h"
#include "solver.h"
#include "listeners.h"

struct delay { // 1 channel delay

  int nsamples;

  float* samples;
  float* fbk;
  float* vol;
  int id;

  float msecs; // delay time

  // feedback and volume are beziers solved into fbk and vol
  std::string fbk_fname, vol_fname;
  multi_curve fbk_crv, vol_crv;
  delay_listener fbk_lis, vol_lis;
  solver fbk_sol, vol_sol;

  delay (float t, const std::string& fn, const std::string& vn);
  ~delay ();
  void zero ();
  void prep_buffer ();
  void operator() (float* out, int nz, float fdr);
  void set (float t);
  void get (float& t);
	void setup ();

};

extern delay left_delay, right_delay;
#endif



