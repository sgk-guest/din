/*
* curve_mixer.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __CURVE_MIXER__
#define __CURVE_MIXER__

#include "solver.h"

struct multi_curve;

struct set_mixer {
	virtual void set_mix (multi_curve& crv) = 0;
};

struct curve_mixer {
	static float TIME;
	static int SAMPLES;
  static double ELAPSEDT;
  static curve_mixer* LAST_MIXER;
  static int NMIXES;
  static void force_finish ();

	multi_curve mix;

	int active;

	float a, da;
	void update_a ();

	solver sol;
	float x, dx;

	curve_mixer ();
  ~curve_mixer ();
	void setup (multi_curve& crv, float ix, float idx);
  void fill_alpha (float* alpha, int n);
  void gen_alpha (float* alpha, int n);
	void gen_mix (float* mixb, int n);
	void gen_mix (float* mixb, int n, xhandler& xmin, xhandler& xmax);
	void gen_fm (float* out, int n, float* fm);
	void do_mix (float* out, float* mixb, float* ab, int n);

};

#endif



