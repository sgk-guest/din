/*
* plugin_browser.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __PLUGIN_BROWSER
#define __PLUGIN_BROWSER

#include <vector>
#include "label.h"
#include "arrow_button.h"
#include "filled_button.h"
#include "item_list.h"

struct curve_editor;

struct plugin_browser : widget, click_listener {

	// see main.cc for inits
	static const int num_plugins;
	static plugin* plugins [];

  static const int num_ctrls = 4, num_ctrls_ = num_ctrls - 1;
  widget* ctrls[num_ctrls];

  arrow_button ab_fold;
  filled_button fb_list;
  label l_title;
  item_list il_plugins;

  ~plugin_browser ();

  void setup ();
  int handle_input ();
  void update ();
  void draw ();
  void draw (curve_editor* ed);
  void add_children_of (plugin* p);
  void clicked (button& b);

	int cur;
  void set_cur (int c);
  plugin* get_cur ();

  int folded ();
	void set_fold (int f);

	void set_ed (curve_editor* e);

};

#endif



