/*
* custom_periodic.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __CUSTOM_PERIODIC
#define __CUSTOM_PERIODIC

/*
* custom_periodic.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "multi_curve.h"
#include "solver.h"
#include "curve_listener.h"
#include "funktion.h"

#include <string>

// periodic function defined by a bezier curve
struct custom_periodic : funktion {
  std::string fname;
  multi_curve crv;
  solver sol;
  custom_periodic (const std::string& fname);
  ~custom_periodic ();
  float operator() (float a, float ea);
};

#endif



