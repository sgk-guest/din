/*
* log.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/
#ifndef __LOG
#define __LOG
#include <fstream>
using namespace std;
extern ofstream dlog;
extern const char SPC;
#endif
