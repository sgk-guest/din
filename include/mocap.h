/*
* mocap.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __mocap
#define __mocap

#include <vector>

struct basic_editor;

struct mocap { // mouse capture

  std::vector<float> x, y;
  unsigned int cur;

  int state;
  enum {empty, capturing, finished};

  mocap ();
  int get (float& x, float& y, int dir = 1);
  int add (float wx, float wy);
  void finish (basic_editor* b);
  void clear ();
  int operator() (); // mocap exists?

};

#endif



