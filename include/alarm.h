/*
* alarm.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __ALARM__
#define __ALARM__
struct alarm_t { 
  double startt;
  double triggert;
  int active;
  alarm_t (double tt = 1.0); 
  int operator() (double t);
  void start ();
  void stop ();
};
#endif
