/*
* fader.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __FADER__
#define __FADER__

#include <string>
#include <fstream>

struct fader;
struct fade_listener {
	virtual void after_fade (fader& f) = 0;
};

struct fader {

  static double TIME; // default

  int on;

  float start;
  float end;
  float delta;
  int reached;

  float alpha;

  int flip; // end > start? 1:0

  float amount; // output

  double start_time, delta_time;

	fade_listener* afl;

	fader (double dt);
  fader (float vs = 0, float ve = 1);
  void set (float vs, float ve, int _on = 1, double dt = TIME);
	void copy (fader* src);
  int eval ();
  void restart ();
	void load (std::ifstream& f);
	void save (std::ofstream& f);

};

#endif



