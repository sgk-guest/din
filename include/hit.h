/*
* hit.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __hit
#define __hit

#include "point.h"

struct multi_curve;
struct hit_t {
  multi_curve* crv; // curve hit
  int crv_id; // id among list of curves
  enum {NONE = 0, VERTEX, LEFT_TANGENT, RIGHT_TANGENT}; // things that can be hit
  int what; // what was hit
  int id; // id of hit thing

	static int name_only;

  // tangent vectors from corresponding vertex
  //
  point<float> left_tangent, right_tangent;
  float left_tangent_magnitude, right_tangent_magnitude;

  hit_t (multi_curve* c = 0, int cid = -1, int w = NONE, int i = -1, int no = 0);
  hit_t (const hit_t& h);
  void clear ();
  int operator()() const; // hit?
	int operator()(int) const; // hit curve item exists?
  int operator== (const hit_t& h) {return ((crv == h.crv) && (what == h.what) && (id == h.id));}
	int matched_id (const hit_t& h) {return ( (crv == h.crv) && (id == h.id) ); }
  hit_t& operator= (const hit_t& h);
  void copy (const hit_t& src);
  const point<float>& get (); 
};
#endif
