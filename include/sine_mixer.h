/*
* sine_mixer.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __sine_mixer
#define __sine_mixer

#include "box.h"
#include "multi_curve.h"
#include "spinner.h"
#include "checkbutton.h"
#include "levels.h"
#include "plugin.h"
#include "point.h"
#include "custom_periodic.h"
#include "sine.h"
#include "curve_editor.h"
#include "listeners.h"
#include "filled_button.h"
#include "mouse_slider.h"
#include <vector>

struct sine_mixer : plugin, change_listener<levels>, curve_listener, option_listener, move_listener, mouse_slider_listener {

  static int NUM_SINE_SAMPLES;
  static const int MIN_SINE_SAMPLES;

  funktion* pf_sin;
  custom_periodic cp_sin;
  sine st_sin;
  int type;
  curve_editor sin_ed;

  int nharmonics;
  std::vector< std::vector< point<float> > > harmonics;

  // output
  std::vector< point<float> > norm;
  void normalise ();

  // ui
	//
  spinner<int> sp_points;

  levels sine_levels;

	checkbutton cb_make_shapeform;
	int num_points, make_shapeform; // for load/save

	checkbutton cb_paint_harmonics;

  options_list ol_sin;
  button b_edit;

	filled_button fb_mover;

	button b_lshift, b_rshift;
	button b_slide;
	checkbutton cb_wrap;

  sine_mixer ();
  ~sine_mixer ();
	void load_params ();
	void save_params ();

  void setup ();

  void set_type ();
  void set_samples (int s);

  void prep_harmonics ();
  void num_harmonics (int n);
  void mix ();
  void render ();
	void apply_not_auto_apply ();
	void shift_apply ();

  void changed (levels& l);
  void changed (field& f);
	void changed (checkbutton& cb);

  void picked (label& lbl, int dir);
  void edited (curve_editor* e, int i);
  void clicked (button& b);
	void moused (int d);
	void moved ();


};

extern sine_mixer sinemixer;
#endif
