/*
* osc.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __osc
#define __osc

#include <lo/lo.h>
#include "command.h"
#include <string>

struct oserver : command {

  lo_server server;
  int lo_fd;
  fd_set rfds;
  struct timeval tv;
  bool enabled;
  bool msg;

  oserver (const std::string& ln, const std::string& sn) : command (ln, sn) {}
  bool operator() (tokenizer& tz);
  void handle_requests ();
  void toggle ();
  ~oserver ();

};

int command_handler (const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data);

struct oconnect : command {

  lo_address& client;

  oconnect (lo_address& c, const std::string& ln, const std::string& sn) : command (ln, sn), client(c) {}
  bool operator() (tokenizer& tz);
  ~oconnect () {}

};

struct osend : command {

  lo_address& client;

  osend (lo_address& c, const std::string& ln, const std::string& sn) : command (ln, sn), client(c) {}
  bool operator() (tokenizer& tz);
  ~osend () {}

};

void osc ();

#endif



