/*
* audio.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __AUDIO
#define __AUDIO

#include "RtAudio.h"

#include <map>
#include <vector>
#include <string>

typedef float sample_t;

struct audio_out {

	RtAudio dac;
  int num_devices;

	enum {INVALID=-1};
  int current_device;
  int default_device;

	std::map<int, RtAudio::DeviceInfo> infos; // id -> info
	std::vector<std::string> names; // use @ settings screen
	std::string get_name (int i) {
		int n = names.size ();
		if (i > -1 && i < n) {
			return names[i];
		} else return "?";
	}

	void probe ();
	void goto_next_device (int i);

	int i_sample_rate;
	void find_sample_rate_id (unsigned int sr);
	int goto_next_sample_rate_id (int i);

	int num_channels;
	int samples_per_channel;
	int last_sample;

  int sample_rate;
  int samples_per_buffer; // buffer has all channels
  int samples_channel_size, samples_buffer_size; // in bytes

  // written by the main thread
  //
  // a multi buffer scheme
  //
  // main thread writes a buffer, while audio thread streams another to audio card
  //

  sample_t * samples_buffers; // a bunch of sample buffers
  int num_samples_buffers;
  int* available; // buffer available for streaming?
  sample_t *readp, *writep;
  int readi, writei;
  inline int can_write () {return !available [writei];}

  // reuseable buffers
  sample_t *result, *ams, *fms, *gatr, *vol, *mix, *mixa, *bufL, *bufR, *fdr1, *fdr2; // result, AM, FM, gater, volume, mix, mix alpha, buffer L and R, fader buffers

  // audio preferences
  std::string prefs_name;
  void load_prefs ();
  void save_prefs ();
  void defaults ();

  audio_out ();
  ~audio_out ();

  void alloc ();
	void open ();
  int open (int id, unsigned int sr, unsigned int spc);
	int start ();
	int close ();

  void set_sample_rate (int s);
	void set_samples_per_channel (int spc);
  static int audio_wanted (void *ob, void *ib, unsigned int spc, double t, RtAudioStreamStatus status, void *data);

};

extern audio_out aout;
extern int SAMPLE_RATE;
extern float SAMPLE_DURATION;

#endif
