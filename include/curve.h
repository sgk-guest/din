/*
* curve.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __CURVE__
#define __CURVE__

#include <list>
#include <vector>

#include "crvpt.h"
#include "point.h"

struct curve {

  float x[2], y[2]; // start/end points
  float tx[2], ty[2]; // start/end tangents

  std::vector<crvpt> vpts; // bezier curve points for draw & solve
  std::vector<crvpt> dpts; // shapeform points (see multi_curve.h/cc)

  curve ();
  curve (float x0, float y0, float x1, float y1, float tx0, float ty0, float tx1, float ty1, float d = 0);
		
	/*
	 * din generates points on the bezier curve using recursive subdivision:
	 * 
	 * find midpoint of line joining end points
	 * if distance between midpoint on this line and point on the bezier curve > limit 
	 * insert point and repeat else stop
	*/ 
	
  float limit2; // square of limit distance
  void set_limit (float l);

	// evaluation generates points of the curve
	//
  enum {EVAL_COMPLETE, EVAL_REQUIRED};
  int eval_state;
	inline int eval_required () {return eval_state == EVAL_REQUIRED;}
	void eval (int find_length = 0);

	inline void vertex (int i, float xv, float yv) {
		x[i] = xv;
		y[i] = yv;
		eval_state = EVAL_REQUIRED;
	}

	inline void get_vertex (int i, float& xv, float& yv) {
		xv = x[i];
		yv = y[i];
	}

  inline void tangent (int i, float xt, float yt) {
    tx[i] = xt;
    ty[i] = yt;
    eval_state = EVAL_REQUIRED;
  }

  inline void get_tangent (int i, float& xt, float& yt) {
    xt = tx[i];
    yt = ty[i];
  }

	float length;
  float calc_length ();
  void normalise_length (float& now_length, float total_length);
  void calc_slope (crvpt& p, crvpt& q);

};

#endif
