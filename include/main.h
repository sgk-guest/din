/*
* main.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __main
#define __main

#include <string>
#include <fstream>

enum {DONT=0, TRY, IMMEDIATE, SOON}; 
extern int quit;
void try_quit ();

void accept_keypress (int accept);
int keydown_modifiers ();

void setup_video_mode (int w, int h, int vw, int vh, int fs = 0);
void setup_window ();

struct instrument;
void modulate_up ();
void modulate_down ();
void start_octave_shift (instrument* instr, int idir, const std::string& sdir);
void do_octave_shift ();
void set_tonic (instrument* instr, float f);
float get_tonic (instrument* instr);
void find_instrument ();
instrument* find_instrument (const std::string& inst);
instrument* get_current_instrument ();
void load_instrument ();
void goto_next_instrument ();
struct ui; int is_instrument (ui* u);
void set_num_octaves (int);

enum {WESTERN=1, NUMERIC=0};
void set_notation (int what);

int find_nearest_note (std::string& nn, float& frequency, float& dist);

char get_typed_char ();

int hide_menu ();
void show_menu ();

void draw_microtonal_keyboard ();

void hz2step (float& hz, float& step);

void applyfx (float* out0, float* out1, int delay = 1, int compress = 1);
void update_audio ();

struct console;
struct audio_out;

console& operator<< (console& c, const audio_out& ao);

void set_snap_drones (int what);
void set_snap (int what); // mark snap on tools tab on editor menu

struct checkbutton;
void dont_call_listener (checkbutton& cb, int state);

void overlay_instrument ();

extern int TURN_OFF_UI; 
void turn_on_ui ();
void turn_off_ui ();

void sync_sounding_board ();

void make_app_name ();
void set_window_caption ();

void warp_mouse (int x, int y);
void make_good_name (std::string& name);

#endif
