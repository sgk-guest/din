/*
* font_editor.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __font_editor
#define __font_editor

#include "viewwin.h"
#include "ui.h"
#include "point.h"
#include "line.h"
#include "help.h"
#include "curve_editor.h"
#include "glyph.h"
#include "font.h"
#include <map>

struct line_hit {
  int l; // line?
  int v; // vertex?
  line_hit () {
    l = v = -1; // invalid
  }
  line_hit (int ll, int vv) : l(ll), v(vv) {}
  bool operator() () {
    return ( (l > -1) && (v > -1));
  }
};

struct font_editor : curve_editor {

  font& fn;

  int nchars;
  std::string name;

  std::map<char, glyph> glyphs;
  std::vector<char> chars;
  glyph gl;
  std::vector<line> lines;
  int ichar;

  std::vector< point<float> > scratch_points;

  line_hit cur, ins, del;
  bool moving;

  void prep_edit ();
  void prep_save ();
  void edit_char (int i);
  void save_char (int i);

  font_editor (font& f, const std::string& settingsf, const std::string& helpf = "font.hlp");
  virtual ~font_editor ();

  void save_font ();


  int handle_input ();
  void draw ();
  void draw_lines ();
  void draw_scratch_line ();
	void mark_area ();

  line_hit hittest ();

  void move ();

  void leave ();

};
#endif



