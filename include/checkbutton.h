/*
* checkbutton.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __checkbutton
#define __checkbutton

#include "button.h"

struct checkbutton;

struct state_listener {
	virtual void changed (checkbutton& cb) = 0;
};

struct checkbutton : button, click_listener {

	int state; // 1 - on, 0 - off
	state_listener* lsnr;

	int colorize_;
  static color on_color, off_color;

	checkbutton ();
		
	void set_state (int s, int call_listener = 1);
	int is_on () const {return state;}
	void turn_on (int call_listener = 1);
	void turn_off (int call_listener = 1);
	void toggle ();

  void set_listener (state_listener* sl) {lsnr = sl;}
		
	void blend_on_off_color (float blend);
	int colorize () const {return colorize_;}
	void colorize (int clr) {colorize_ = clr;}
  void clicked (button& b);
};

struct state_button : checkbutton {
	static const int SIZE = 8;
	static const int SIZE2 = 2 * SIZE;
	state_button (int sz = SIZE) {set_size (sz);}
	void draw ();
};

#endif



