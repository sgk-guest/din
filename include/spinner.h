/*
* spinner.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __spinner
#define __spinner

#include "input.h"
#include "font.h"
#include "widget.h"
#include "label.h"
#include "arrow_button.h"
#include "checkbutton.h"
#include "field.h"
#include "utils.h"
#include "mouse_slider.h"
#include "tokenizer.h"
#include <string>
#include <typeinfo>

extern int is_mouse_slider_active ();
extern void abort_selectors ();
extern void cant_mouse_slide ();
extern int wheel;
extern char BUFFER [];
extern int VAR_MIN, VAR_MAX;
extern std::string STR_VAR_MIN_MAX;
extern int SPACING;

template <typename T> struct spinner : widget, click_listener, state_listener, change_listener<field>, typing_listener, mouse_slider_listener {

  checkbutton lbl;

	label backlbl;
	int draw_backlbl;

  arrow_button dec, inc;
	int dir;

  field f_value;
  T value;
	T lastv;

	int draw_more;
  arrow_button more;

  label l_delta;
  field f_delta;
  T delta0, delta;

	checkbutton cb_variance;
	field f_variance;
	rnd<float> variance;

  int limits;
  T lo, hi;

  void set_limits (T _lo, T _hi) {
		limits = 1;
    lo = _lo;
    hi = _hi;
  }

  change_listener<field> *lis[3]; // 0 - f_value, 1 - f_delta, 2 - f_variance = null

  spinner (const std::string& _name = "unknown") : l_delta ("+-") {

		cb_variance.set_text (" ~ ");
		f_variance.set_text (STR_VAR_MIN_MAX);
		set_var (VAR_MIN, VAR_MAX);

		lbl.set_listener (this);

    widget* chld [] = {this, &dec, &inc, &f_value, &more, &l_delta, &f_delta, &cb_variance, &f_variance};
    for (int i = 0, j = 8; i < j; ++i) lbl.add_child (chld[i]);

    dec.set_dir (arrow_button::left);
    inc.set_dir (arrow_button::right);
    dec.set_listener (this);
    inc.set_listener (this);
    inc.click_repeat = 1;
    dec.click_repeat = 1;
		dir = 0;

		draw_more = 1;
    more.set_dir (arrow_button::right);
    more.set_listener (this);

    l_delta.hide ();
    f_delta.hide ();
		cb_variance.hide ();
		f_variance.hide ();

		lastv = value = 0;
    f_value.change_lsnr = f_delta.change_lsnr = f_variance.change_lsnr = this;
		f_value.typing_lsnr = f_delta.typing_lsnr = this;

		lis [0]=lis [1]=lis[2]=0;

    limits = 0;
    lo = hi = 0;

		// see field::call_listener ()
		const std::type_info& ti = typeid (T);
		std::string tn (ti.name());
		if (tn == "i") {
			f_value.return_type_open = f_delta.return_type_open = "{int("; 
			f_value.return_type_close = f_delta.return_type_close = ")}";
		}

		else if (tn == "f") {
			f_value.return_type_open = f_delta.return_type_open = "{double(";
			f_value.return_type_close = f_delta.return_type_close = ")}";
		}

		draw_backlbl = 0;

  }

	~spinner () {
		dlog << "~spinner: " << lbl.text << SPC << f_value.text << SPC << endl;
	}

  void set_pos (int x, int y) {
    widget::set_pos (x, y);
    widget* w [] = {&lbl, &dec, &inc, &f_value, &more, &l_delta, &f_delta, &cb_variance, &f_variance};
    int xshift [] = {0, 0, -1, 0, -1, 5, 1, 0, 0};
    int lft [] = {0, fnt.lift, fnt.lift, 0, fnt.lift, 0, 0, 0, 0};
    for (int i = 0, j = 9; i < j; ++i) {
      x += xshift [i];
      w[i]->set_pos (x, y + lft[i]);
      advance_right (x, *w[i], SPACING);
    }
		set_pos_backlbl ();
  }

	void set_pos_backlbl () {
		if (draw_backlbl) {
			widget* w = 0;
			if (f_delta.visible) w = &f_variance; 
			else if (draw_more) w = &more;
			else w = &f_value;
			backlbl.set_pos (w->extents.right + SPACING, w->extents.bottom - fnt.lift);
		}
	}

  void update () {
    lbl.update ();
		backlbl.update ();
    l_delta.update ();
    f_value.update ();
    f_delta.update ();
		cb_variance.update ();
		f_variance.update ();
    set_pos (posx, posy);
		inc.update ();
		dec.update ();
		more.update ();
  }

  void toggle_delta () {
    if (more.dir == arrow_button::right) {
      l_delta.show ();
      f_delta.show ();
			cb_variance.show ();
			f_variance.show ();
      more.set_dir (arrow_button::left);
    } else {
      l_delta.hide ();
      f_delta.hide ();
			cb_variance.hide ();
			f_variance.hide ();
      more.set_dir (arrow_button::right);
    }
		set_pos_backlbl ();
		abort_selectors ();
  }
  
  void change_value (int _dir) {
		dir = _dir;
		delta = delta0;
		value += operator()();
		f_value = value;
		changed (f_value);
	}

	void decrease () {
		change_value (-1);
	}

	void increase () {
		change_value (+1);
	}

	spinner<T>& operator++ () {
		change_value (+1);
		return *this;
	}

	spinner<T>& operator-- () {
		change_value (-1);
		return *this;
	}

  void clicked (button& b) {
    if (&b == &dec) {
			decrease ();
    } else if (&b == &inc) {
			increase ();
    } else if (&b == &more) {
      toggle_delta ();
    }
	}

	void changed (checkbutton& cb) {
		if (orient == NONE) {
			cb.turn_off (0);
			if (is_mouse_slider_active ()) 
				cant_mouse_slide ();
		}
		else {
			if (cb.state) mouse_slider0.add (this); else mouse_slider0.remove (this);
			if (shift_down() == 0) activate_mouse_slider ();
		}
	}

	void set_var (float s, float t) {
		s /= 100.0f;
		t /= 100.0f;
		variance.set (s, t);
	}

  void changed (field& f) {
		int i = 0;
    if (&f == &f_value) {
			if (limits) {
				T v = f;
				if (clamp<T>(lo, v, hi)) f = v;
			} 
			value = f;
			if (f.edited) {
				delta = value - lastv;
				if (delta > 0) dir = 1; else {dir = -1; delta = -delta;}
			}
			lastv = value;
    } else if (&f == &f_delta) {
			delta0 = delta = f;
			i = 1;
		} else if (&f == &f_variance) {
			tokenizer tz (f.text);
			float mn, mx; tz >> mn >> mx;
			set_var (mn, mx);
			i = 2;
		}
    set_pos (posx, posy);

		change_listener<field>* lisi = lis[i];
		if (lisi) lisi->changed (f);
		abort_selectors ();

  }

  void typing (field& f) {
    f.update ();
    set_pos (posx, posy);
  }

	void moused (int _dir) {
		change_value (_dir);
	}

	void after_slide () {
		lbl.turn_off (0);
	}

  void draw () {
    glColor3f (clr.r, clr.g, clr.b);
		widget* w [] = {&lbl, &dec, &inc, &f_value};
		for (int i = 0, j = 4; i < j; ++i) w[i]->draw ();
		if (draw_more) {
			more.draw ();
			if (more.dir == arrow_button::left) {
				l_delta.draw ();
				f_delta.draw ();
				cb_variance.draw ();
				f_variance.draw ();
			}
		}
		if (draw_backlbl) backlbl.draw ();
  }

  void set_value (T t) {
		value = t;
		lastv = value;
    f_value = value;
		set_pos (posx, posy);
  }

  void set_delta (T t) {
		delta0 = delta = t;
    f_delta = delta0;
		set_pos (posx, posy);
  }

  void set_listener (change_listener<field>* _lis, int id = 0) {
    lis [id] = _lis;
  }

  void set_text (const std::string& l, const std::string& bl = "") {
    lbl.set_text (l);
    set_name (l);
		mouse_slider_listener::name = l;
		if (bl != "") {
			backlbl.set_text (bl);
			draw_backlbl = 1;
		}
  }

	void set_moveable (int m, int mc = 0, int* pmb = &lmb) {lbl.set_moveable (m, mc, pmb);}

	int handle_input () {

		int r = lbl.handle_input (); 
		if (r) return r;

		int d1 = 0, i1 = 0, m1 = 0;
		d1 = dec.handle_input ();
		if (d1 == 0) {
			i1 = inc.handle_input ();
			if (i1 == 0) 
				m1 = more.handle_input ();
		}

		int c = d1 | i1 | m1;
		if (c) return c;

		if (wheel && f_value.hover) {
			change_value (wheel);
			HOVER = 1;
			return HOVER;
		}

		int s = f_value.handle_input ();
		if (s) return s;

  	if (more.dir == arrow_button::left) {
			widget* mow [] = {&f_delta, &cb_variance, &f_variance};
			for (int i = 0; i < 3; ++i) {
				widget* wi = mow[i];
				s = wi->handle_input ();
				if (s) return 1;
			}
		}

		return 0;

	}

	inline T dir_delta () {
		return (dir * delta);
	}

	T operator() () {
		return operator() (dir_delta());
	}
	
	T operator() (float dirdelta) {
		float var = 1;
		if (cb_variance.state) var += variance ();
		return (dirdelta * var); 
	}

};

#endif
