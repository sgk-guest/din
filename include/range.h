/*
* range.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __range__
#define __range__

#include "box.h"
#include "note.h"
#include "color.h"
#include "modulator.h"
#include <string>
#include <map>

struct scale_info;

struct range {

	//
	// microtonal range
	// from one note to another (both included).

  box<int> extents;

	int octave;

  note notes[2]; // end notes
	void change_note (int i, int j, scale_info& si);

  std::string intervals[2]; // interval names of end notes (see *.tuning files)

  float delta_octave_position; // difference of octave position of the end notes
  float delta_step; // difference of step values ie pitch/frequency of the end notes (see note.h)

  void calc (scale_info& si);
	void calc_note_freq (int i, scale_info& si);

  void draw_labels (int label_what = LEFT, int show_frequency = 0);
	enum {NONE = -1, LEFT, RIGHT, BOTH};

	int key;

  void sample_rate_changed ();

  // for opengl
  int pts [8];

	// modulation
	modulator mod; // fm => width, am => height
	void init_mod ();

	range ();
	void change_height (int dh);

};
#endif
