/*
* filled_button.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __filled_button
#define __filled_button

#include "button.h"

struct filled_button : button {
  filled_button (int sz = 8) {set_size (sz);}
  void update () {set_size (size);}
  void draw ();
};

#endif
