/*
* binaural_drone.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __BINAURAL_DRONE__
#define __BINAURAL_DRONE__

#include "multi_curve.h"
#include "play.h"
#include <string>

struct binaural_drone {

  std::string name;
	void make_name ();
	void make_name (float stepl, float stepr); // for fading in/out step > hz > name
	void make_name (float v); // for fading in/out vol > name

	// L and R hz
	float l_hz, r_hz;
	float l_step, r_step;
	float l_step_prev, r_step_prev;

	enum {SEPARATION = 2};
	float sep;
	void set_sep (float s);

	// hz justification
	enum {LEFT=0, RIGHT, CENTER};
	int just;
	void set_just (int j);

	// volume
	enum {VOLUME = 3};
	float vol;
	float vol_prev;

	solver soll, solr;
	play playl, playr;

	enum {INVALID = -1};
	binaural_drone (multi_curve* wav, float lhz, float rhz, float vol, int just, float sep, float lx = INVALID, float rx = INVALID);

	void set_hz (int i, float _hz);
	void set_hz (float _lhz, float _rhz);
	void fill_hz ();

	void render (float* L, float* R, float* vfdr, float* pfdr, int uv, int uh, int abort);

	void set_vol (float vol);

	struct fade_flags {

		int vol;
		float vol_cur;

		enum {NONE = 0, L, R, BOTH};
		int hz;
		float l_step_cur, r_step_cur;

		fade_flags () {vol = hz = NONE;}

	} fading;

	void calc_fader (float* out, float* alpha, int n, float v1, float v2, float* cur);
	void sync ();

	int sel;

};

#endif
