/*
* ui_list.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __ui_list
#define __ui_list

#define MENU uis.main_menu
#define MENUP &MENU

#include "fader.h"
#include "filled_button.h"
#include "checkbutton.h"
#include "font_editor.h"
#include "field.h"
#include "label.h"
#include "settings.h"
#include "spinner.h"
#include "curve_display.h"
#include "plugin_browser.h"
#include "menu.h"

#include <SDL/SDL.h>
#include <map>

struct curve_editor;
struct din;

struct attack_val : change_listener<field> {
	void changed (field& f);
};

struct decay_val : change_listener<field> {
	void changed (field& f);
};

struct voices_val : change_listener<field> {
	void changed (field& f);
};


struct fade_button_listener : state_listener, fade_listener {
	checkbutton* cb;
	fader* f;
	int* target;
	state_listener* lsnr;
	fade_button_listener ();
	fade_button_listener (checkbutton* cb, fader* f, int* target);
  void changed (checkbutton& c);
	void after_fade (fader& f);
};

struct compress__listener : state_listener {
  void changed (checkbutton& cb);
};

struct settings__listener : click_listener {
  void clicked (button& b);
};

struct pitch_bend_listener : state_listener, change_listener<field> {
  void changed (checkbutton& b);
  void changed (field& f);
};

struct waveform_display_listener : click_listener {
  void clicked (button& b);
};

struct parameters_listener : click_listener {
  int d_min_max_visible;
  void clicked (button& b);
};

struct scroll_arrow_listener : click_listener {
  void clicked (button& b);
};

struct show_pitch_volume_listener : state_listener {
  void changed (checkbutton& cb);
};

struct ui_list : ui {

  std::vector<ui*> uis;
  ui *current, *prev;

  curve_editor* crved;

  font_editor fed;

  static const int MAX_EDITORS = 7;
  static const Uint8 key [MAX_EDITORS];
  static ui* ed [MAX_EDITORS];

  double esct;

  ui_list ();

	void set_current (ui* u);
  int set_editor (const std::string& name, int screen);

	int rmb_clicked;
  int handle_input ();

  void bg ();
  void draw ();
  int escape_from_things ();

  menu main_menu;

  std::map<ui*, std::vector<widget*> > widgets_of; // every ui
  plugin_browser plugin__browser;

  // bottom line
  //
  checkbutton cb_voice;
  fader fdr_voice;

  checkbutton cb_gater;
  fader fdr_gater;
  void flash_gater ();

  checkbutton cb_delay;
  fader fdr_delay;

  checkbutton cb_compress;
  compress__listener clis;

	checkbutton cb_record;

	button l_mondrian_voices;

  // keyboard-keyboard parameters
  //

  label d_parameters;
  arrow_button ab_parameters;
  parameters_listener pal;

	spinner<float> sp_attack_time, sp_decay_time;
	spinner<int> sp_voices;
	attack_val atv;
	decay_val dkv;
	voices_val vov;

  spinner<float> sp_pitch_bend;
  checkbutton cb_show_nearby_notes;
  pitch_bend_listener pbl;

  label l_waveform_display;
  curve_display cd_waveform_display;
  arrow_button ab_prev_wav, ab_next_wav;
  waveform_display_listener wdl;

  label l_octave_shift;
  arrow_button ab_octave_down, ab_octave_up;
  spinner<float> sp_octave_shift_bpm;
	octave_shift_listener osl;

  button b_settings;
  settings__listener slis;
  settings settings_scr;

  arrow_button ab_scroll_left, ab_scroll_right, ab_scroll_up, ab_scroll_down;
  scroll_arrow_listener sal;
  checkbutton cb_show_pitch_volume_board, cb_show_pitch_volume_drones;
  show_pitch_volume_listener spvl;

	void add_widgets ();
	void update_widgets (int wnow = -1, int hnow = -1, int wprev = -1, int hprev = -1);
	int update_bottom_line ();

  void setup ();

  float eval_fade (fader& fdr, checkbutton& cb);
  int is_widget_on_screen (widget* w, ui* scr);
  void dofft ();
  void handle_plugin (widget* which, int what);
	void set_edit_labels (); // see fractaliser, warper
	void remove (widget* w);

	fade_button_listener vlis, glis, dlis;

  ~ui_list ();

};

extern ui_list uis;

#endif
