/*
* container.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __container
#define __container

#include <algorithm>

template<class S, class T> inline void clear (S& container) {
  // clears container of pointers. deletes the pointers.
  typedef typename S::iterator Si;
  for (Si i = container.begin(), j = container.end(); i != j; ++i) {
    T* t = *i;
    delete t;
  }
  container.clear ();
}

template<class T> inline void erase_id (T& container, unsigned int q) {
  // erases qth item in container
  typedef typename T::iterator Ti;
  Ti iter = container.begin();
  for (unsigned int p = 0; p < q; ++p, ++iter);
  if (iter != container.end()) container.erase (iter);
}

template <class C, class V> inline int erase (C& c, const V& v) {
	// erase item v from container c
	typedef typename C::iterator Ci;
	Ci ce = c.end (), ci = std::find (c.begin (), ce, v);
	if (ci != ce) {
		c.erase (ci);
		return 1;
	}
	return 0;
}

template <class C, class V> inline int push_back (C& c, V& v) {
	// push back v if unique
	typedef typename C::iterator Ci;
	Ci ce = c.end (), ci = std::find (c.begin (), ce, v);
	if (ci == ce) {
		c.push_back (v);
		return 1;
	}
	return 0;
}

template<class S, class T> inline T& get (S& container, int q) {
  // gets qth item in container
  typedef typename S::iterator Si;
  Si iter = container.begin ();
  for (int p = 0; p < q; ++p, ++iter);
  return *iter;
}

/*template<class S, class T, class I> inline T& get (S& container, I& iter, int q) {
  // gets qth item of container and its iterator
  iter = container.begin ();
  for (int p = 0; p < q; p++, ++iter);
  return *iter;
}*/
#endif
