/*
* item_list.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __item_list
#define __item_list

#include "button.h"
#include <vector>
#include <string>

struct tokenizer;

struct item_list;
struct selection_listener {
	virtual void selected (item_list& il, int l) = 0;
};

struct item_list : button, click_listener {

  int size;

	int last, hov, cur;

	int lh, yt;

	int n, n_1, nsel;
	void calc ();

  std::vector<std::string> items;
	std::vector <std::string> nums;

	std::vector<int> sel;
	selection_listener *sel_lis;
	void select (int w);
	void select (int i, int j);
	int select_these (tokenizer& tz);
	void invert_select ();
	std::string get_selected ();
	inline int num_selected () {return nsel;}
	int get_first ();

	item_list ();
  void add (const std::string& i);
	void insert (int i, const std::string& s);
	void remove (int i);

  void draw ();
  void set_pos (int x, int y);
	void repos (int tx, int ty);
	void clicked (button& b);

	int handle_input ();

};

#endif
