/*
* oscilloscope.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __oscilloscope
#define __oscilloscope

#define MAX_SAMPLES 1024

#include "box.h"
#include "ui.h"
#include "arrow_button.h"
#include "widget.h"
#include <string>
#include <vector>

struct oscilloscope_params_t {
	int left, base, height;
	int num_samples;
	int visible;
	int folded;
	oscilloscope_params_t (int l, int b, int h, int ns, int v, int f);
};

struct oscilloscope : ui, click_listener, widget {

	struct sample_t {
		static float lmin, lmax; // current min/max of left channel
		static float rmin, rmax; // current min/max of right channel
		float left, right; // stereo
		sample_t () {left = right = 0;}
	} samples [MAX_SAMPLES];

	std::vector<oscilloscope_params_t> params; // for each instrument

	int left, base, height;
  int num_samples;
	int folded;

	void set_folded (int f);

  int viewr;
  int addr;
	void add_samples (float* outl, float* outr, int n);

	std::string settingsf;

	int limit;

  oscilloscope (const std::string& _settingsf);
  ~oscilloscope ();

  int load ();
  int save ();
	void load_current_instrument ();
	void save_current_instrument ();

	// gl
  int* vertex_array;
  float* color_array;

	// ui
	//
	arrow_button fld;
	float lr, lg, lb; // left channel color
	float rr, rg, rb; // right channel color

	box<int> win, pick_win;
	int leftx, rightx, endx;

	int ndraw;
	int label_counter;

	int lly, lry, lh;
	static const int buf_size = 256;
	char lbuf1 [buf_size], rbuf1 [buf_size];

  void set_height (int h);
  void set_num_samples (int n);

	void calc_draw_params ();
  void draw ();

	int lmb_clicked;
	int px, py;
	int move, stop_move;
  int handle_input ();
	void clicked (button& b);

};
#endif



