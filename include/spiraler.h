/*
* spiraler.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __SPIRALER
#define __SPIRALER

#include "spinner.h"
#include "plugin.h"
#include "point.h"
#include "ui_sin_cos_radius.h"

struct spiraler : plugin, ui_sin_cos_radius_listener {

  // make a spiral
	//
	
  ui_sin_cos_radius scr;

  point<float> center;

  float radius;
  float turns;
  int num_points;

  spinner<float> sp_radius;
  spinner<float> sp_turns;
  spinner<int> sp_num_points;

  spiraler ();
  ~spiraler ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();

  void sin_cos_radius_optioned ();
  void sin_cos_radius_edited ();

};

extern spiraler spiraler_;

#endif



