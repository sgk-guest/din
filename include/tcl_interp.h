/*
* tcl_interp.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __tcl
#define __tcl

#include <tcl.h>
#include <string>
#include <sstream>

struct tcl_interp { // tcl interpreter

  Tcl_Interp *interp;

  tcl_interp ();
  ~tcl_interp ();
  void setup (Tcl_Interp* pi);

  std::string result;
	int result_status; 

  tcl_interp& operator() (const std::string& command); // execute command
  
  void add_din_specific ();


};

typedef int (*tclcmd)(ClientData cd, Tcl_Interp* ti, int objc, Tcl_Obj* CONST objv[]);
int tcl_run (ClientData cd, Tcl_Interp* ti, int objc, Tcl_Obj* CONST objv[]);

extern tcl_interp interpreter;

#endif



