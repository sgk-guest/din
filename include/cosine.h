/*
* cosine.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __COSINE
#define __COSINE

#include <math.h>
#include "funktion.h"

struct cosine : funktion {
  float operator() (float a, float ea) {
    return cos (a);
  }
};

#endif



