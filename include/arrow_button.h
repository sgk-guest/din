/*
* arrow_button.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __arrow_button
#define __arrow_button

#include "button.h"

struct arrow_button : button {

	enum {left = 1, right, up, down};
  int dir;

	int v[6];

  arrow_button (int sz = 10, int dir = right) {
    set_size (sz);
    set_dir (dir);
  }

  void draw ();

  void set_dir (int dir);

	void set_pos (int x, int y) {
		widget::set_pos (x, y);
		set_size (size);
		set_dir (dir);
	}

	void toggle ();

};
#endif
