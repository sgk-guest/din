/*
* ui_sin_cos_radius.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __UI_SIN_COS_RADIUS
#define __UI_SIN_COS_RADIUS
#include "options_list.h"
#include "button.h"
#include "sine.h"
#include "cosine.h"
#include "constant_radius.h"
#include "custom_periodic.h"
#include "curve_editor.h"

struct ui_sin_cos_radius_listener {
  virtual void sin_cos_radius_optioned () {}
  virtual void sin_cos_radius_edited () {}
};

struct ui_sin_cos_radius : widget, option_listener, click_listener, curve_listener {

  // functions sin, cos and radius
  funktion *pf_sin, *pf_cos, *pf_radius;

  // standard functions for sin, cos and radius
  sine st_sin;
  cosine st_cos;
  constant_radius st_radius;

  // custom functions for sin cos and radius
  custom_periodic cp_sin, cp_cos, cp_rad; 

  curve_editor sin_ed, cos_ed, rad_ed; // sin cos and radius editor

  options_list ol_sin, ol_cos, ol_radius;
  button b_sin, b_cos, b_radius;
  std::string fname;

  ui_sin_cos_radius (
      ui_sin_cos_radius_listener* _lis, 
      const std::string& scr_fname, 
      const std::string& cp_sin_fname,
      const std::string& cp_cos_fname, 
      const std::string& cp_rad_fname,
      const std::string& sin_ed_fname,
      const std::string& cos_ed_fname,
      const std::string& rad_ed_fname,
      int _inc_radius = 1);
  ~ui_sin_cos_radius ();

  void load ();
  void save ();
  void setup ();
  int handle_input ();
  void draw ();
  void set_pos (int x, int y);

  ui_sin_cos_radius_listener* lis;
  void picked (label& l, int dir);
  void clicked (button& b);

  int sin, cos, radius;
  int inc_radius;

  void edit_sin ();
  void edit_cos ();
  void edit_radius ();
  void edited (curve_editor* e, int i);

  void set_option (label& lbl, const std::string& text, funktion** pf, funktion* pfv, button& bt, int vis);

};

#endif



