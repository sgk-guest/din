/*
* color.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __color__
#define __color__

#include "dingl.h"
#include "random.h"

inline void hex2rgb (unsigned char hr, unsigned char hg, unsigned char hb, float& r, float& g, float& b) {
  const unsigned char ff = 0xff;
  const float ff1 = 1.0f / ff;
  r = hr * ff1;
  g = hg * ff1;
  b = hb * ff1;
}

struct color {

  float r, g, b;

  color& operator*= (float v) {
    r *= v;
    g *= v;
    b *= v;
    return *this;
  }

  color (float rr = 0, float gg = 0, float bb = 0) {
    r = rr;
    g = gg;
    b = bb;
  }

  color (const color& t, const color& s) {
		r = t.r - s.r;
		g = t.g - s.g;
		b = t.b - s.b;
	}
  
};


inline void set_rnd_color (float& r, float& g, float& b, float m0 = 0, float m1 = 1) {
	rnd<float> rd (m0, m1);
	r = rd(); g = rd (); b = rd ();
}

inline void set_color (unsigned char hr, unsigned char hg, unsigned char hb, float a = 1) {
  float r, g, b;
  hex2rgb (hr, hg, hb, r, g, b);
  glColor4f (r, g, b, a);
}


inline void blend_color (const color& source, const color& target, color& result, float amount) {
	color delta (target, source);
	delta *= amount; 
	result.r = source.r + delta.r; 
	result.g = source.g + delta.g;
	result.b = source.b + delta.b;
}

#endif
