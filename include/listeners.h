/*
* listeners.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __LISTENERS
#define __LISTENERS

#include "curve_listener.h"

struct din;
struct ui;
struct instrument;

struct wave_listener : curve_listener {
  enum {MICROTONAL_KEYBOARD, KEYBOARD_KEYBOARD, DRONE, MONDRIAN};
  int which;
  wave_listener (int w) : which (w) {}
  void edited (curve_editor* ed, int i);
};

struct beat2value;
struct beat2value_listener : curve_listener {
  beat2value* bv;
  beat2value_listener ();
	void edited (curve_editor* ed, int i);
	void set (beat2value* _bv) {bv = _bv;}
};

CURVE_LISTENER (drone_mod_lis);
CURVE_LISTENER (morse_code_listener);

struct solver;
struct delay;
struct delay_listener : curve_listener {
  delay* dly;
  solver* sol;
  float* result;
  delay_listener () : dly(0), sol (0), result (0) {}
	delay_listener& operator() (delay* d, solver* s, float* r) { dly = d; sol = s; result = r; return *this;}
  void edited (curve_editor* ed, int i);
};

struct attack_listener : curve_listener {
  instrument* inst;
  void edited (curve_editor* ed, int i);
};

struct decay_listener : curve_listener {
  instrument* inst;
  void edited (curve_editor* ed, int i);
};

CURVE_LISTENER (velocity_listener);
CURVE_LISTENER (range_wh_lis);
CURVE_LISTENER (pitch_vol_lis);
CURVE_LISTENER (pomo_lis);

#endif
