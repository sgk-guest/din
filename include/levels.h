/*
* levels.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __levels__
#define __levels__

#include "widget.h"
#include "box.h"

#include <string>
#include <vector>

struct levels : widget {

  void load (); // load settings
  void save (); // save settings

  // gui vars
  int elem_width;
  int height;

  int nlev; 
  int lastlev;

  int lev; // edited level
  float val; // edited val
  int hgt; // edited height

	int edited; // values edited?

  int editable; // is control editable?

	enum {STARTED=1, FINISH};
  int editing; // editing element?
	int stop_editing ();

	int paint;
	int lmb_clicked;

	int binary; // val = 0 or 1, height = min or max

  int saveable; // levels saved on quit?

  std::vector<float> values; // 0 to 1 for each level
  std::vector<int> heights; // 0 to height

  levels (const std::string& s);
  ~levels ();
  int handle_input ();

  float r, g, b;
  float a, a0;
  void draw ();

  void clear_hgt_val ();
  void calc_lev ();
	void calc_hgt_val ();
  void set (int i, float v, int h = -1);
	void set_only (int i, float v);
	int change (int i, float d);

	void update ();

  change_listener<levels>* lis;
  void set_listener (change_listener<levels>* l) {
    lis = l;
  }

	int wrap;
	int rshift ();
	int lshift ();

  void chkpos ();
	void reheight ();

};

#endif
