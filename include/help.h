/*
* help.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __help
#define __help
#include <string>
#include <vector>
struct help {
  std::vector<std::string> text;
  help (const std::string& fname);
  void operator() ();
};
#endif



