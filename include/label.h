/*
* label.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __label
#define __label

#include <string>
#include "widget.h"

struct label : widget {

  std::string text;
  void set_text (const std::string& txt);

	label (const std::string& t = "");

  void update ();
  void draw ();

};

#endif
