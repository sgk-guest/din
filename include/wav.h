/*
* wav.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __WAV
#define __WAV

template <typename T> void write(std::ofstream& stream, const T& t) {
  stream.write((const char*)&t, sizeof(T));
}

template <typename T> void writeFormat(std::ofstream& stream) {
  write<short>(stream, 1);
}

template <> void writeFormat<float>(std::ofstream& stream) {
  write<short>(stream, 3);
}

template <typename SampleType> void write_wav_header (std::ofstream& stream, int sampleRate, short channels, size_t bufSize) {
  stream.write("RIFF", 4);
  write<int>(stream, 36 + bufSize);
  stream.write("WAVE", 4);
  stream.write("fmt ", 4);
  write<int>(stream, 16);
  writeFormat<SampleType>(stream);                                // Format
  write<short>(stream, channels);                                 // Channels
  write<int>(stream, sampleRate);                                 // Sample Rate
	int sizeofSampleType = sizeof (SampleType);
  write<int>(stream, sampleRate * channels * sizeofSampleType); // Byterate
  write<short>(stream, channels * sizeofSampleType);            // Frame size
  write<short>(stream, 8 * sizeofSampleType);                   // Bits per sample
  stream.write("data", 4);
  stream.write((const char*)&bufSize, 4);
}
#endif



