/*
* slider.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __slider
#define __slider

#include "widget.h"
#include "dingl.h"
#include "viewwin.h"
#include "utils.h"

#include <algorithm>
#include <vector>

extern int mousex;
extern viewport view;
extern int lmb;

template <typename T> struct values {
	
	T low, high, delta, val;
	float amount;
	
		values (T l = 0, T h = 1, T v = 0) {
			set_limits (l, h);
			set_val (v);
		}
		
		operator T() {
			return ((T) val);
		}

		void set_limits (T l, T h) {
			low = l;
			high = h;
			delta = high - low;
		}
		
		void get_limits (T& l, T& h) {
			l = low;
			h = high;
		}
				
		T get_val () const { return val; }
		
		void set_val (const T& v) {
			val = v;
			clamp<T> (low, val, high);
			if (delta) amount = (val - low) * 1.0f / delta; else amount = 0;
		}
		
		void set_amount (float a) {
			amount = a;
			clamp<float> (0.0, amount, 1.0);
			val = (T) (low + delta * amount);
		}
		
		float get_amount () const { return amount; }
	
};

template <typename T> struct slider : widget {
	
	values<T> vx;
	int dx;
	
	int slide;
	int lmb_clicked;

	change_listener<slider> * lsnr;
			
		slider (int w = 64, int h = 16) : widget (0, 0, w, h), vx (0, 0, 0), dx (0), slide(0), lmb_clicked(0) {}

		int handle_input () {
			
			widget::handle_input ();
			
			int ret = 0;

			if (lmb) {
				if (lmb_clicked == 0) {
					if (slide) {
						slide = 0;
						ret = 1;
					} else {
						if (hover) {
							slide = 1;
							ret = 1;
						}
					}
					lmb_clicked = 1;
				}
			} else {
				if (slide) {
					const box<int>& e = extents;
					dx = mousex - e.left;
					clamp (0, dx, e.width);
					float new_amount = dx * e.width_1;
					float amount = vx.get_amount ();
					if (new_amount != amount) {
						vx.set_amount (new_amount);
						if (lsnr) lsnr->changed (*this);
					}
				} else ret = 0;
				lmb_clicked = 0;
			}
			
			return ret;
			
		}

		void draw () {
			
			widget::draw ();

			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			
			const color& c = clr;
			
			glColor4f (c.r, c.g, c.b, (GLfloat) 0.15f);
			
			const box<int>& e = extents;
				glRecti (e.left, e.bottom, e.right, e.top);
			
			glColor4f (c.r, c.g, c.b, 1);
				glRecti (e.left, e.bottom, e.left + dx, e.top);

			glDisable (GL_BLEND);

		}

		void update () {
			float amount = vx.get_amount ();
			const box<int>& e = extents;
			dx = (int) (amount * e.width);  
			//if (lsnr) lsnr->changed (*this);
		}
		
		void set_listener (change_listener<slider>* sl) {
			lsnr = sl;
		}
		
		T get_val () const { return vx.get_val (); }
		
		void set_val (const T& t) { 
			vx.set_val (t);
			update ();
		}
		
		void set_limits (T l, T h) {
			vx = values<T> (l, h, vx.get_val ());
			update ();
		}
		
		void get_limits (T& l, T& h) {
			vx.get_limits (l, h);
		}
};

#endif



