/*
* options_list.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __options_list
#define __options_list

#include "arrow_button.h"
#include "font.h"
#include <string>

extern int lmb;
extern int wheel;

struct option_listener {
  virtual void picked (label& lbl, int dir)=0;
};

struct options_list : widget, click_listener {

	button option;
  arrow_button left;
  arrow_button right;
	button apply;

  option_listener* olis;

  options_list (int show_apply = 0) {
    option.add_child (&left);
    option.add_child (&right);
		option.add_child (&apply);
		option.add_child (this);
    left.set_dir (arrow_button::left);
    right.set_dir (arrow_button::right);
    left.set_listener (this);
    right.set_listener (this);
		apply.set_text ("Apply");
		if (show_apply) apply.show (); else apply.hide ();
    olis = 0;
  }

  void set_text (const std::string& t) {
    option.set_text (t);
    set_name (t);
		set_pos (posx, posy);
  }

  void set_listener (option_listener* _olis) {
    olis = _olis;
  }

  void set_pos (int x, int y) {

    widget::set_pos (x, y);

    static const int spacing = 4;
    left.set_pos (x, y + fnt.lift);
    advance_right (x, left, spacing);

    right.set_pos (x, y + fnt.lift);
    advance_right (x, right, spacing + 1);

    option.set_pos (x, y);
		advance_right (x, option, 4 * spacing);

		apply.set_pos (x, y);

		widget* rw = 0;
		if (apply.visible) rw = &apply; else rw = &option;
		set_extents (left.extents.left, option.extents.bottom, rw->extents.right, rw->extents.top);

  }

  void save (std::ofstream& file) {
    file << name << ' ' << posx << ' ' << posy << std::endl;
  }

  int handle_input () {

		if (option.handle_input());
		else if (left.handle_input()) ;
		else if (right.handle_input()) ;
		else if (apply.visible && apply.handle_input()) ;
		else return 0;
		return 1;

    /*int r = option.handle_input ();
		if (r) return r;

		r = left.handle_input ();
		if (r) return r;

		r |= left.handle_input ();
		r |= right.handle_input ();
		if (apply.visible) r |= apply.handle_input ();
    return r;*/

  }

  void draw () {
    option.draw ();
    left.draw ();
    right.draw ();
		if (apply.visible) apply.draw ();
  }

  void set_color (unsigned char r, unsigned char g, unsigned char b) {
    option.set_color (r, g, b);
    left.set_color (r, g, b);
    right.set_color (r, g, b);
		apply.set_color (r, g, b);
  }

  void set_color (float r, float g, float b) {
    option.set_color (r, g, b);
    left.set_color (r, g, b);
    right.set_color (r, g, b);
		apply.set_color (r, g, b);
  }

  void clicked (button& b) {
    if (olis) {
      if (&b == &left) {
        olis->picked (option, -1);
      } else {
        olis->picked (option, 1);
      }
    }
  }

  void set_click_repeat (int click_repeat) {
    left.click_repeat = click_repeat;
    right.click_repeat = click_repeat;
  }

  void update () {
    option.update ();
		apply.update ();
  }

  void set_moveable (int i, int* pmb = &lmb) {
    option.set_moveable (i, 1, pmb);
  }

};

#endif
