/*
* recorder.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __RECORDER
#define __RECORDER

#include <string>
#include <list>
#include <fstream>

struct checkbutton;

struct rectime {

	double dsecs;
	int mins;
	int secs;

	rectime () {
		dsecs = 0;
		mins = 0;
		secs = 0;
	}

	rectime& operator+= (double dt) {
		dsecs += dt;
		if (dsecs >= 1.) {
			dsecs -= 1.;
			++secs;
			if (secs >= 60) {
				secs = 0;
				++mins;
			}
		}
		return *this;
	}

	void reset () {
		dsecs = 0;
		mins = secs = 0;
	}

};

struct recorder {

	std::string folder;
  std::string fname;

  std::ofstream file; 

  std::list <float*> record_buffers;
	std::list<float*>::iterator iter, jter;
  int buffer_size;
	float* samples_buffer;

  int saved;
	int saving_started;

  float nmin, nmax;
  void normalise ();

  void start_saving ();
  int save_some (checkbutton& cb);

	rectime rec_time_add, rec_time_save;
	recorder ();

  void add (float* sample_buffer, int sample_buffer_size, int num_samples, checkbutton& cb_record1, checkbutton& cb_record2);
	int exists () {return buffer_size;}

  void set_fname (const std::string& _fname);

  int stop_rec, stop_mins, stop_secs;
  void set_stop_at (int mins, int secs);

	void clear ();
  void start ();
  void stop ();

  ~recorder ();

};

extern recorder recorder0;

#endif



