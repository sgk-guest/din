#ifndef __fileutils
#define __fileutils

#include <fstream>
#include <string>

extern std::string user_data_dir;

struct file_in {
	std::ifstream f;
	file_in (const std::string& fn) {
		f.open ((user_data_dir + fn).c_str(), std::ios::in);
	}
	ifstream& operator() () {return f;}
};

struct file_out {
	std::ofstream f;
	file_out (const std::string& fn) {
		f.open ((user_data_dir + fn).c_str(), std::ios::out);
	}
	ofstream& operator() () {return f;}
};
#endif
