/*
* line.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __line
#define __line

#include <vector>
#include "point.h"

struct line {
  std::vector< point<int> > points;
  line () {}
  line (const std::vector< point<int> >& pts) : points(pts) {}
  line (const std::vector< point<float> >& pts) {
    points.clear ();
    for (unsigned int i = 0, j = pts.size(); i < j; ++i) {
      points.push_back (point<int>((int)pts[i].x, (int)pts[i].y));
    }
  }
  void set (int i, int x, int y);
  void insert (int i, int x, int y);
  int remove (int i);
  void draw ();
};

#endif



