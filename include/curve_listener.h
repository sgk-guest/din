/*
* curve_listener.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __CURVE_LISTENER
#define __CURVE_LISTENER
struct curve_editor;
struct curve_listener {
  virtual void selected (int i) {}
  virtual void edited (curve_editor* ed, int i);
  virtual ~curve_listener () {}
};
#define CURVE_LISTENER(name) struct name : curve_listener { void edited (curve_editor* ed, int i); };
#endif
