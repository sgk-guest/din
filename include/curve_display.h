/*
* curve_display.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __curve_display
#define __curve_display

#include "box.h"
#include "widget.h"

struct multi_curve;
struct curve_editor;

struct curve_display : widget {

  multi_curve* crv;

  box<float> bbox;
  void calc_bbox ();
  void add_gutter ();

  void draw ();
  void draw_curve ();

  float* gl_pts;
  int n_glpts;

  curve_display (int w = 96, int h = 96) {

    gl_pts = 0;
    n_glpts = 0;

    crv = 0;
    set_extents (0, 0, w, h);

  }

  void set_size (int w, int h) {
    set_extents (extents.left, extents.bottom, extents.left + w, extents.bottom + h);
  }

  void unit_bbox ();

  ~curve_display ();

};

#endif



