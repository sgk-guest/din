/*
* countries.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __COUNTRIES
#define __COUNTRIES

#include "plugin.h"
#include "box.h"
#include "options_list.h"
#include "spinner.h"
#include "label_field.h"
#include <vector>
#include <string>

struct area { // polygon of an area of a country
	int num_vertices;
	std::vector<float> x, y;
	box<float> bbox; // bounding box
	area () : num_vertices(0) {}
	bool operator< (const area& a) const;
	void calc_bbox ();
	void normalise ();
};

struct country {
	std::string name;
	std::vector<area> areas;
	int num_areas;
	country () : num_areas (0) {}
};

struct countries : plugin, option_listener {

	int num_countries;
	std::vector<std::string> index; // names of countries
	std::map<std::string, country> the_countries; // name > data
	int id; // current country
	int area_id; // current area
	country* p_cur_country; // ptr to current country

	label_field lf_search; // to choose country
	options_list ol_country; 
	options_list ol_area;
	spinner<int> sp_step; // increase step to have low res map, vice-versa

	std::vector<country> countrys;
  countries ();
  ~countries ();
	void load_params ();
	void save_params ();
	//void load_data ();
	void load_index ();
	country* load_country (const std::string& s);
	void set_country (const std::string& name);
	int find_id (const std::string& name);
  void setup ();
  void render ();

	void picked (label& lbl, int dir);
	void render_area ();

	void changed (field& f);

};

#endif



