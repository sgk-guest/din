#ifndef __modulator__
#define __modulator__

#include "beat2value.h"
#include <string>

struct multi_curve;

struct mod_params { // modulation params

  float depth;
	beat2value bv; // for bpm
	float result;

	float initial; // for range modulation

	mod_params (multi_curve* c, const std::string& n = "noname");
	void set_bpm (float value, int reset = 0);
	void calc ();
	void clear (int reset = 0);

};

struct modulator {
	
  enum {FM, AM};
  mod_params fm, am;
  
  int active;

	int afx_vel; // affects velocity vector of drones?

  modulator (multi_curve* fc, multi_curve* ac) : fm (fc), am (ac), active(0), afx_vel(0) {}

  void clear () {
    fm.clear ();
		am.clear ();
    active = 0;
  }

  void calc () {
    fm.calc ();
		am.calc ();
  }

  int calc_active () {
    active = ((am.depth == 0 || am.bv.bpm == 0) && (fm.depth == 0 || fm.bv.bpm == 0 )) ? 0:1;
		return active;
  }

};

#endif
