/*
* authors_note.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __authors_note
#define __authors_note

#include "ui.h"
#include "button.h"
#include <vector>

struct widget;

struct authors_note : ui, click_listener {

  button buy_now;
  button buy_later;

  int rollup; // console rolled up?

	authors_note ();
  void enter ();
  void leave ();
	int handle_input ();
	void draw ();
  void setup ();
	void clicked (button& b);

};

extern authors_note anote;
#endif



