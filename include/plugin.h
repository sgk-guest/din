/*
* plugin.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __PLUGIN
#define __PLUGIN

#include "arrow_button.h"
#include "checkbutton.h"
#include "label.h"
#include "options_list.h"
#include <vector>
#include <string>
#include <sstream>

struct multi_curve;
struct curve_editor;
struct field;

typedef std::vector< point<float> >::iterator points_iterator;

// converts geometry to bezier curve
//

struct plugin : widget, click_listener, change_listener<field>, state_listener {


  std::vector <widget*> ctrls;
  int num_ctrls;

  button b_apply;

	int mix;
	int undo;
  checkbutton cb_auto_apply;

  plugin ();
  virtual ~plugin ();

  int _folded;
  void fold ();
  void unfold ();
  int folded () {return _folded;}

  void clicked (button& b);
  void changed (field& f);
  void changed (checkbutton& cb);

  virtual void setup ();
  int handle_input ();
  void update ();

  virtual void render () = 0; // convert geometry > bezier curve

  void draw (); // draw ui
  virtual void draw (curve_editor* ed); // draw rendered curve

  static int change_curve_name; // 1 - yes, 0 - no
  virtual int apply (multi_curve& crv); // apply bezier curve to curve in editor

	curve_editor* ed;
	void set_ed (curve_editor* e);

	std::vector < point<float> > points; // vertices
	int npts;

  int shapeform; // render as shapeform?

  std::stringstream ss;
  std::string crv_name; // name of rendered curve

  std::string name; // name of the plugin
  std::string make_fname ();

  virtual void load_params () {}
  virtual void save_params () {}
	void try_auto_apply ();
	void do_render () {
		render ();
		try_auto_apply ();
	}

  // for opengl
  float* pts;
  int pts_n;
	void gen_pts ();

};

#endif
