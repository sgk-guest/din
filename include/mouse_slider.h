/*
* mouse_slider.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __mouse_slider
#define __mouse_slider

#include "widget.h"
#include "arrow_button.h"

#include <list>

struct mouse_slider_listener {

	enum {X, Y, NONE};
	int orient;

	std::string name;

	mouse_slider_listener ();
	virtual void moused (int dir) = 0;
	virtual void after_slide () {} 

};

struct mouse_slider : widget {

	arrow_button ab [4];
	int sz, sz_2, szn, szn_1;
	void update_x_arrows ();
	void update_y_arrows ();

	int lowx, highx, midx;
	int lowy, highy, midy;
	int prevx, prevy;
	int initx, inity, inityy;

	std::list<mouse_slider_listener*> mslx, msly;
	int nmslx, nmsly;

	int active;

	int lmb_clicked;

	mouse_slider ();
	~mouse_slider ();
	int handle_input ();

	int pts [4];
	void draw ();

	void add (mouse_slider_listener* sl);
	void remove (mouse_slider_listener* sl);
	int activate ();
	void deactivate ();

};

void activate_mouse_slider ();
extern int is_mouse_slider_active ();
extern mouse_slider mouse_slider0;

#endif
