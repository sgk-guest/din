/*
* tokenizer.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


// string tokenizer
// based on public domain code by Song Ho Ahn (song.ahn@gmail.com)

#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <string>
#include <vector>

struct tokenizer {

	static const std::string DEFAULT_DELIMITER;

  tokenizer();
  tokenizer(const std::string& s, const std::string& d=DEFAULT_DELIMITER);
  tokenizer (const std::vector<std::string>& tokens);

  tokenizer& operator>> (std::string& s);
  tokenizer& operator>> (float& f);
  tokenizer& operator>> (int& i);
  tokenizer& operator>> (char& c);
  tokenizer& operator>> (unsigned char& c);

  std::vector<std::string> tokens;
  int tokens_available;
  unsigned int tokid;

  std::string delimiter;
  std::string buffer;
  int blen, dlen;
  void str (const std::string& s);
  void del (const std::string& d);
  void vec (const std::vector<std::string>& v);
  void set (const std::string& s, const std::string& d=DEFAULT_DELIMITER);
  bool isdelim (char c);

  void skip_ws ();

  int cur;
  void init_cur ();

  std::string cur2end ();

  std::string token;

};
#endif
