/*
* menu.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __menu
#define __menu

#include "ui.h"
#include "button.h"
#include "label.h"
#include "separator.h"
#include "arrow_button.h"
#include "spinner.h"
#include "checkbutton.h"
#include "options_list.h"
#include "label_field.h"
#include "slider.h"
#include "tap_bpm.h"
#include "plus_button.h"
#include "minus_button.h"
#include "curve_listener.h"
#include "item_list.h"
#include <map>
#include <vector>
#include <string>

struct ball;

#define MAKE_CLICK_LISTENER(name) struct name : click_listener { void clicked (button& b); };
#define MAKE_FIELD_LISTENER(name) struct name : change_listener<field> { void changed (field& f); };
#define MAKE_STATE_LISTENER(name) struct name : state_listener { void changed (checkbutton& cb); };

MAKE_CLICK_LISTENER (set_unset_toggle_listener)
MAKE_CLICK_LISTENER (pan_zoom_listener)
MAKE_STATE_LISTENER (snap_listener)

struct pitch_vol_dis_pix_lis : state_listener, change_listener<field> {
	void changed (checkbutton& cb);
	void changed (field& f);
};

struct curve_ops_listener : click_listener, state_listener, change_listener<field> {
  void clicked (button& b);
  void changed (checkbutton& cb);
  void changed (field& f);
};

struct octave_shift_listener : click_listener, change_listener<field> {
  void clicked (button& b);
  void changed (field& f);
};

MAKE_FIELD_LISTENER (gater_bpm_listener)
MAKE_FIELD_LISTENER (voice_volume_listener)

struct drone_handle_size_listener : change_listener<field> {
  int last;
  drone_handle_size_listener () : last (0) {}
  void changed (field& f);
};

struct binaural_drones_listener : click_listener, change_listener<field>, state_listener, option_listener, selection_listener {

	std::string val[5];

	static const char* justs[];
	int just;

	enum {EQUAL = 0, GREATER_THAN_EQUAL, LESSER_THAN_EQUAL, RANGE, ID};
	int select_rule, select_what; 

	void clicked (button& b);
	void changed (field& f);
	void changed (checkbutton& cb);
  void picked (label& lbl, int dir);
	void selected (item_list& il, int i);

	void set_hz (int w, float v);
	binaural_drones_listener ();

};

struct ball_ops_listener : click_listener, state_listener, change_listener<field>, option_listener {
	int op_id;
	ball_ops_listener ();
	void changed (checkbutton& cb);
	void changed (field& f);
	void picked (label& lbl, int dir);
	void clicked (button& b);
};

MAKE_FIELD_LISTENER(drone_trail_length_listener)
MAKE_FIELD_LISTENER(change_drone_vel_listener)
MAKE_FIELD_LISTENER(change_drone_accel_listener)
MAKE_FIELD_LISTENER(rotate_drone_vel_listener)
MAKE_FIELD_LISTENER(drones_per_min_listener)
MAKE_FIELD_LISTENER(orbit_insertion_time_listener)
MAKE_FIELD_LISTENER(drone_lifetime_listener)

struct drone_commands_listener : click_listener, state_listener {
  void clicked (button& b);
	void changed (checkbutton& cb);
};

MAKE_CLICK_LISTENER(phrase_commands_listener)

struct snap_drones_listener : change_listener<field>, option_listener {
	void changed (field& f);
	void picked (label& l, int dir);
};

MAKE_STATE_LISTENER(plugin_listener)

MAKE_FIELD_LISTENER (ball_direction_listener)
MAKE_FIELD_LISTENER (note_poly_radius_listener)
MAKE_FIELD_LISTENER (note_poly_points_listener)
MAKE_FIELD_LISTENER (trail_length_listener)
MAKE_FIELD_LISTENER (ball_volume_listener)
MAKE_FIELD_LISTENER (ball_speed_listener)
MAKE_FIELD_LISTENER (slit_size_listener)
MAKE_FIELD_LISTENER (slit_anim_time_listener)
MAKE_FIELD_LISTENER (ball_attack_time_listener)
MAKE_FIELD_LISTENER (ball_decay_time_listener)

struct mondrian_listener : click_listener, change_listener<field>, state_listener, option_listener   {

  void clicked (button& b);
  void changed (field& f);
	void changed (checkbutton& cb);
	void picked (label& l, int dir);

	static const char* split_types [3];
	static const int MAX_SPLIT_TYPES = 2;
	int hsplit, vsplit;
	void check_split_type (options_list& ol, int& o);
	void handle_split (int& var, int dir, float t);
	void handle_auto_pick_box (options_list& ol, int dir, int& v);

	static const char* selection_targets [2];
	static const char* pick_box_types [];
	static const char* auto_split_at_types [];
	static const char* auto_split_orient_types [];

};

struct style_listener :  option_listener {
  static const int num_styles = 2;
  static const int last_style = num_styles - 1;
  static std::string styles [num_styles]; 
  int id;
  options_list& oplist;
  std::string what;
  std::string prefix;

  style_listener (options_list& ol, const std::string& wh, const std::string& pfx) : oplist(ol), what(wh), prefix(pfx) {id = 0;}
  void set_style (const std::string& style);
  void get_style ();
  void next_style (int dir);
  void picked (label& lbl, int dir);

};


struct read_write_mod {
	virtual void read_mod () = 0;
	virtual void write_mod () = 0;
};

struct range_data : read_write_mod {
	int range;
	int mod;
	range_data () : range(0), mod(0) {}
	void read_mod ();
	void write_mod ();
};

struct adjust_range_left_listener : range_data, mouse_slider_listener {
	void after_slide ();
	void moused (int dir);
};

struct adjust_range_right_listener : range_data, mouse_slider_listener {
	void after_slide ();
	void moused (int dir);
};

struct adjust_range_both_listener : range_data, mouse_slider_listener {
	void after_slide ();
	void moused (int dir);
};

struct adjust_range_height_listener : range_data, mouse_slider_listener {
	void after_slide ();
	void moused (int dh);
};

struct range_height_listener : range_data, click_listener, mouse_slider_listener {
	void clicked (button& b);
	void moused (int dir);
	void after_slide ();
};

struct change_note_listener : click_listener, mouse_slider_listener {
	int id;
	void moused (int dir);
	void clicked (button& b);
};

struct change_note_style_listener : option_listener {
	void picked (label& lbl, int dir);
	void set (label& lbl, int v);
};

struct board_height_listener : read_write_mod, click_listener, mouse_slider_listener {
	vector<int> moda;
	void read_mod ();
	void write_mod ();
	void clicked (button& b);
	void moused (int dir);
	void after_slide ();
};

MAKE_CLICK_LISTENER (range_width_listener)

struct set_range_listener : click_listener, option_listener {
	int i;
	set_range_listener () : i(1) {}
	void clicked (button& b);
	void picked (label& lbl, int dir);

};

struct range_mod_lis : curve_listener, click_listener, state_listener, change_listener<field> {
	void changed (field& f);
	void changed (checkbutton& cb);
	void clicked (button& b);
	void edited (curve_editor* ed, int i);
};

MAKE_FIELD_LISTENER (arrow_uv_lis)
MAKE_FIELD_LISTENER (range_defaults_lis)

struct oscilloscope;
struct scope_listener : change_listener<field>, state_listener {
	oscilloscope* oscp;
  void changed (field& f);
  void changed (checkbutton& cb);
	void setup ();
};

struct tap_bpm_listener : change_listener<tap_display>, state_listener {
  void changed (tap_display& td);
  void changed (checkbutton& cb);
};

struct recording_listener : click_listener, typing_listener, state_listener {
	void clicked (button& b);
	void changed (checkbutton& cb);
	void typing (field& f);
};

MAKE_CLICK_LISTENER (misc_listener)

struct menu : widget, click_listener, option_listener, change_listener<field>, change_listener< slider<float> >, state_listener {

  button b_menu; // click to display this menu; appears on all screens

  checkbutton b_microtonal_keyboard, b_keyboard_keyboard, b_mondrian, b_binaural_drones; // instruments

  // editors
  button b_microtonal_keyboard_waveform, b_drone_waveform, b_drone_modulation, b_voice_modulation, b_range_modulation, b_range_width_height, b_range_pitch_vol, b_gater;
  button b_keyboard_keyboard_waveform, b_attack, b_decay, b_midi_velocity;
  button b_mondrian_waveform, b_mondrian_attack, b_mondrian_decay;
  button b_delays, b_octave_shift, b_compressor, b_morse_code;
	button b_binaural_drones_waveform;
	button b_point_modulation;

  // octave shift 
  label l_octave_shift;
  arrow_button ab_octave_down, ab_octave_up;
  spinner<float> sp_octave_shift_bpm;
	octave_shift_listener osl;

  // gater 
  label l_gater;
  spinner<float> sp_gater_bpm;
  options_list ol_gater_style;
  style_listener gater_style_lis;
  gater_bpm_listener gbl;

  // lead voice volume
  spinner<float> sp_voice_volume;
  voice_volume_listener vvl;

  // drone master volume
  options_list ol_drone_master_volume;
  label_field lf_delta_drone_master_volume;
  button b_mute_drones;
  void set_drone_master_volume ();

  // drones visual 
  checkbutton cb_show_anchors;
  spinner<int> sp_change_drone_handle_size;
  drone_handle_size_listener dhsl;
  spinner<int> sp_change_drone_trail_length;
  drone_trail_length_listener dtl;
	checkbutton cb_show_vel, cb_show_accel, cb_show_gravity;
	spinner<float> sp_change_drone_vel;
	change_drone_vel_listener dvl;
	spinner<float> sp_change_drone_accel;
  change_drone_accel_listener dal;
  spinner<int> sp_rotate_drone_vel;
  rotate_drone_vel_listener rdvl;
  spinner<int> sp_drones_per_min;
  drones_per_min_listener dpml;
	spinner<float> sp_drone_lifetime;
	drone_lifetime_listener dlf;
	spinner<float> sp_orbit_insertion_time;
	orbit_insertion_time_listener oil;

  // drone/voice modulation
	//
  static const char* modulation_targets [2];
  options_list ol_modulation;

	// for voice modulation
  spinner<float> sp_am_depth, sp_fm_depth;
  spinner<float> sp_am_bpm, sp_fm_bpm;
	float am_depth, fm_depth;
	options_list ol_am_style, ol_fm_style;
	style_listener am_style_lis, fm_style_lis;

	// for drone modulation
	spinner<float> sp_dam_depth, sp_dfm_depth;
	spinner<float> sp_dam_bpm, sp_dfm_bpm;
	float dam_depth, dfm_depth, dam_bpm, dfm_bpm;

	options_list ol_set_unset_toggle;
	button b_set, b_unset, b_toggle;
	set_unset_toggle_listener sutl;

	button b_reset_velocity;
	button b_reset_gravity;
	checkbutton cb_keep_size;

  void init_modulation ();

  //void after_drone_selection ();
	void from_mondrian ();

	std::map<std::string, spinner<float>*> bpm_map;
	void update_bpm (const std::string& name, float value);

	// drone tools
  button b_add_drones, b_move_drones, b_delete_drones;
  button b_select_all_drones, b_invert_drone_selection;
  button b_move_drones_under_gravity;
	button b_select_launchers;
	checkbutton cb_select_launched;
	button b_freeze_drones, b_thaw_drones;
  spinner<int> sp_bounces;
  spinner<int> sp_rebound;
	options_list ol_bounce_style;
  button b_launch_drones;
  button b_stop_launching_drones;
	button b_set_targets;
	button b_clear_targets;
  button b_track_drones;
  button b_select_tracked_drones;

	options_list ol_create_this;

  spinner<int> sp_mesh_rows, sp_mesh_cols;
	checkbutton cb_sync_rows_cols;
	label l_drone_order;
	options_list ol_drone_order;
	options_list ol_mesh_point;
	field f_mesh_xy;
	spinner<float> sp_mesh_dur;
	button b_flip_rows_cols;

	spinner<int> dp_spacing;
	spinner<float> dp_bpm1, dp_bpm2;
	options_list dp_orient;

	button b_orbit_selected_drones, b_select_attractees, b_select_attractors; 
  button b_track_gravity;
	spinner<int> sp_browse_drone;

  drone_commands_listener dcl;

	spinner<float> sp_snap_left, sp_snap_right;
	options_list ol_snap_style;
  snap_drones_listener sdl;

	spinner<float> sp_rotate_drones, sp_scale_drones;

	// phrasing
  button b_record_phrase, b_clear_phrases;
  label l_phrase_position;
  slider<float> s_phrase_position;
  phrase_commands_listener pcl;

  // ranges
  label l_adjust_height, l_adjust_range;
	options_list ol_set_range;
	set_range_listener sral;
  button b_selected_to_all, b_default_to_selected, b_default_to_all;
  range_width_listener rwl;
	range_height_listener rhl;
	board_height_listener bhl;
	button b_adjust_range_height, b_adjust_board_height, b_adjust_range_left, b_adjust_range_right, b_adjust_range_both;
	button b_change_note_left, b_change_note_right;
	options_list ol_change_note_style;
	change_note_listener cnl, cnr;
	change_note_style_listener cnsl;
	adjust_range_left_listener arl;
	adjust_range_right_listener arr;
	adjust_range_both_listener arb;

	checkbutton cb_mark_ran;
	spinner<int> sp_range;
	void load_range (int r);
	spinner<float> sp_ran_mod_width, sp_ran_mod_height;
	spinner<float> sp_ran_mod_width_bpm, sp_ran_mod_height_bpm;
	button b_get_cur_ran;
	range_mod_lis rml;

	label l_ran_mod;
	checkbutton cb_mod_ran;
	button b_rm_start_all, b_rm_stop_all, b_rm_toggle, b_rm_pause_resume;
	void load_range_mod ();

	spinner<int> sp_default_width, sp_default_height;
	range_defaults_lis rdel;

	checkbutton cb_pitch_dis, cb_vol_dis;
	spinner<int> sp_lev_sz;
	pitch_vol_dis_pix_lis pvdl;

  // misc
  button b_key_to_pitch_at_cursor;

	label l_drone_arrow;
	struct {
		spinner<float> width, depth;
	} sp_arrow;
	arrow_uv_lis uvl;

  // oscilloscope
  checkbutton cb_scope; 
  spinner<int> sp_scope_height;
  spinner<int> sp_scope_samples;
  scope_listener scol;

  // tap BPM
  label l_tap_bpm, l_tap_bpm_value;
  tap_display td_tap_display;
  checkbutton cb_am, cb_fm, cb_gater, cb_octave_shift;
  checkbutton cb_auto_reset;
  tap_bpm_listener tbl;
  void mark_tap_target ();

  // mondrian tools
  button b_add_balls, b_move_selected_balls;
  button b_select_all_targets, b_invert_selected_targets, b_select_targets_in_box, b_delete_selected_targets, b_delete_all_targets;
  button b_split_horizontal, b_split_vertical, b_delete_box;
  button b_freeze_balls, b_thaw_balls;
	button b_modulate_balls_up, b_modulate_balls_down, b_clear_modulations;
	button b_auto_change_direction_clockwise;
	button b_auto_change_direction_anti_clockwise;
	button b_stop_auto_changing_direction;
	button b_flip_direction;
	button b_actual_voices;
	button b_make_random_color;
	button b_add_remove_slits;
	button b_remove_slits_on_edge;
	button b_toggle_slit_anim;
	button b_toggle_wreckers;
	button b_toggle_healers;
	button b_toggle_bouncers;
	button b_switch_ball_type;
	button b_select_wreckers;
	button b_select_healers;
	button b_make_note_grid;
	button b_make_nxn_grid;
	button b_delete_all_boxes;

	options_list ol_ball_types;
	options_list ol_split_types_h;
	options_list ol_split_types_v;
	spinner<int> sp_mondrian_num_boxes;

	options_list ol_selection_targets;

	// mondrian visuals
	arrow_button abm_left, abm_right, abm_up, abm_down; 
	plus_button bm_zoom_in;
	minus_button bm_zoom_out;
	checkbutton cb_draw_boxes;
	checkbutton cb_fill_boxes;
	checkbutton cb_label_notes;
	checkbutton cb_label_hz_vol;
	checkbutton cb_draw_notes;

	// for ball
	button l_draw_ball;
	checkbutton cb_draw_ball_position, cb_draw_ball_heading, cb_draw_ball_trails;

	// mondrian parameters
  spinner<int> sp_mondrian_min_voices;
	checkbutton cb_mondrian_auto_adjust_voices;
	spinner<float> sp_mondrian_change_vol;
  spinner<float> sp_mondrian_change_attack_time;
  spinner<float> sp_mondrian_change_decay_time;
  spinner<float> sp_mondrian_change_speed;
	spinner<int> sp_mondrian_change_dir;
	spinner<float> sp_mondrian_change_slit_size;
	spinner<float> sp_mondrian_change_slit_anim_time;
	spinner<int> sp_mondrian_change_trail_size;
	spinner<float> sp_mondrian_change_note_poly_radius;
	spinner<int> sp_mondrian_change_note_poly_points;

	// mondrian auto split/delete
	checkbutton cb_auto_split_box, cb_auto_delete_box;
	spinner<float> sp_auto_split_time, sp_auto_delete_time;
	options_list ol_auto_pick_box_split, ol_auto_pick_box_delete, ol_auto_split_at, ol_auto_split_orient;
	spinner<float> sp_min_split_size;

	// mondrian ball ops
	checkbutton cb_turn;
	options_list ol_browse_balls;
	spinner<float> sp_turn_every;
	spinner<float> sp_turn_min, sp_turn_max;
	checkbutton cb_turn_sync;
	checkbutton cb_speed;
	spinner<float> sp_speed_every;
	spinner<float> sp_speed_min, sp_speed_max;
	spinner<float> sp_max_speed;
	checkbutton cb_speed_sync;
	checkbutton cb_teleport;
	spinner<float> sp_tel_every;
	spinner<float> sp_tel_radius;
	checkbutton cb_clone;
	checkbutton cb_clone_can_clone;
	spinner<float> sp_clone_every;
	spinner<float> sp_clone_offset;
	spinner<int> sp_max_clones;
	spinner<int> sp_max_balls;
	checkbutton cb_transform;
	spinner<float> sp_transform_every;
	options_list ol_bouncer, ol_healer, ol_wrecker;

	ball_ops_listener bolis;

	// mondrian params 
	ball_attack_time_listener batl;
	ball_decay_time_listener bdtl;
	ball_speed_listener bsl;
	ball_direction_listener brl;
	ball_volume_listener bvl;
	trail_length_listener tll;
	note_poly_radius_listener nprl;
	note_poly_points_listener nppl;
	slit_size_listener ssl;
	slit_anim_time_listener satl;
  mondrian_listener monl;

	// binaural drones tools 
	spinner<float> sp_bd_separation;
	button b_create_binaurals_on_notes;
  options_list ol_key_note;
	checkbutton cb_close_octave;
  checkbutton cb_resize_separation;
	button b_create_binaurals_from_pitch;
	label_field lf_bd_start_pitch;
	spinner<int> sp_bd_pairs;
	label_field lf_bd_spacing;
  options_list ol_justification;

	item_list il_binaural_drones;
	button bbd_select_all, bbd_select_none, bbd_invert_select, bbd_select2, bbd_sync, bbd_delete, bbd_modulate, bbd_flip;
	label_field lf_vol_fade_time, lf_pitch_fade_time, lf_master_volume, lf_modulation_amount, lf_l, lf_r, lf_sep, lf_vol;
	arrow_button bd_modulate_up, bd_modulate_down;
	options_list ol_select_what, ol_select_rule, ol_just;
	field bdf_value;
  
	binaural_drones_listener bdl;
	void update_binaurals_list ();

  // editor tools
  //
  arrow_button abe_left, abe_right, abe_up, abe_down;
  plus_button pb_zoom_in;
  minus_button mb_zoom_out;
  pan_zoom_listener pzl;
  void set_repeat (button** B, int n, double dt); // to enable click repeat on pan & zoom buttons
  void set_zoom_repeat (double dt);
  void set_pan_repeat (double dt);

  label l_snap;
  checkbutton cb_snapx, cb_snapy, cb_snapboth, cb_snapnone;
  snap_listener snl;
  void set_snap (int what);

  button b_pick_curve;
	
	button b_swap_curves;

  button b_insert_vertex, b_delete_vertex;
  curve_ops_listener col;

	options_list ol_mirror;
  button b_fold_tangents, b_unfold_tangents;
  checkbutton cb_selection_only; 

  options_list ol_vertices_carry_tangents; // carry tangents or not?
  void set_vertices_carry_tangents (int i);

  options_list ol_mirror_tangents; // mirrored or not?
  void set_mirror_tangents (int i);

  button b_undo, b_redo; // edit
  button b_copy, b_paste; // curve
  checkbutton cb_label_vertices;
	checkbutton cb_mark_segments;

  button b_draw_replacement_curve;

  label l_library;
  arrow_button abl_left, abl_right;
  label_field lf_curve_name;
  button b_add_curve, b_replace_curve, b_delete_curve;

  label l_capture;
  button b_start_capture, b_assign_capture;

  options_list ol_curve_style;
  spinner<float> sp_curve_rpm;
  button b_stop_rotating;

  checkbutton cb_show_waveform_samples;
  spinner<float> sp_waveform_hz;
  spinner<int> sp_waveform_periods;
  spinner<float> sp_curve_limit;

  checkbutton cb_draw_curve;
	checkbutton cb_overlay_instrument;


  // tabs
  int num_tabs;
  checkbutton cb_file, cb_instrument, cb_editors, cb_parameters, cb_mkb_drone_tools, cb_mkb_drone_parameters, cb_mkb_voice, cb_mkb_ranges, cb_mkb_misc, 
							cb_ed_tools, cb_mon_parameters, cb_mon_tools, cb_mon_misc, cb_mon_boxops, cb_mon_ballops, cb_binaural_drones_tools, cb_binaural_drones_edit;
  std::vector<checkbutton*> tabs;
  std::map< checkbutton*, std::vector<widget*> > tab_members;
  checkbutton *last_tab, *cur_tab;
  separator tab_sep;

	void setup_tabs (ui* scr);
  void setup_tabs (checkbutton** tl, int n, int clear = 0);
  void add_to_tab (checkbutton* cb, widget* w);
  void remove_from_tab (checkbutton* cb, widget* w);
  void position_tabs ();
	void position_menu_items ();

  misc_listener miscl;

	// File
	//
  button b_exit_din;
	checkbutton cb_record;
	button b_clear_record;
	label_field lf_file;
	button b_save;
	recording_listener recl;
	button b_turn_off_ui;

  // menu scroller
	void move_items (int dx, int dy);
  button b_close;
	
  int show;
  int menu_mousex, menu_mousey; // last mouse position on menu
  int screen_mousex, screen_mousey;  // mouse position on screen when menu becomes visible
	box<int> bg_extents;
	float opacity;
  void calc_bg ();

  void set_pos (int x, int y);
  void setup ();
  void update ();
  int handle_input ();
  void clicked (button& b);
  void changed (field& f);
  void changed (slider<float>& s);
  void changed (checkbutton& cb);
  void changed (tap_display& tb);
  void picked (label& lbl, int dir);

  static const int num_items = 362;
  static const int n_inst_ed = 25;
  widget* items [num_items];
  int scr_id [n_inst_ed];
  int is_ed [n_inst_ed], is_inst [n_inst_ed];

  std::vector<widget*> editors;
  void show_editors (ui* inst);
  void hide_editors ();

  menu ();

  void setup_items ();
  void draw ();
  void toggle ();

	void set_ball_ops (ball* b);
	void clear_ball_ops ();

  ~menu ();

};

#endif
