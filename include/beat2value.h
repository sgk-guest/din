/*
* beat2value.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __BEAT2VALUE
#define __BEAT2VALUE

#include "solver.h"
#include "curve_mixer.h"
#include <string>

struct beat2value : set_mixer {
	
	std::string name; 

	std::string crvname;
	multi_curve *crv;
	int crv_made_here;

	solver sol;
	std::string style; // style of continuation at end of curve (loop or pong)
	xhandler *xmin, *xmax;

	float min_bpm; // minimum bpm 
	float bpm; // current bpm
	float now; // beat position 
	float delta; // used by the solver
	float set_bpm (float n, int delta_mult = 1);
	float get_bpm ();

	curve_mixer mixer;
	void set_mix (multi_curve& crv);

	beat2value ();
  beat2value (const std::string& crvname, const std::string& fcrvname);
  void setup (int delta_mult = 1);
	~beat2value ();

  int gen_and_mix (float* soln, float* mixb, float* mixa, int n);
  int modulate_and_mix (float* in, float* mixb, float* mixa, int n, float depth); 
  int modulate_and_mix (float* in, float* mixb, float* mixa, int n, float* depth); 

};
#endif
