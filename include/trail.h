/*
* trail.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __TRAIL
#define __TRAIL

#include <list>
#include <string>
#include "point.h"

struct trail_t {

	static float* tpts; 
	static int n_tpts;
  static void alloc (int n = 256);
  static void destroy () { if (tpts) delete[] tpts; }

  static const int INITIAL_TOTAL_POINTS = 10;
  std::list< point<float> > tq;
	int num_points;
  int total_points;
	point<float> last;
	trail_t (int t = INITIAL_TOTAL_POINTS);
	void add (float x, float y);
	void draw ();
	void change_size (int delta);
};

#endif
