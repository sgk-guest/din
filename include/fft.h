/*
* fft.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __fft
#define __fft

#include <vector>
#include "arrow_button.h"
#include "label.h"
#include "levels.h"
#include "kiss_fft.h"

struct multi_curve;

struct fft : widget, click_listener {

  //
  // FFT of Bezier curve waveform using kissFFT
  //
  kiss_fft_cfg cfg;
  static const int NUM_SAMPLES = 128;
  float in [NUM_SAMPLES];
  kiss_fft_cpx cx_in [NUM_SAMPLES], cx_out [NUM_SAMPLES];

  std::vector<float> harms; // harmonics
  levels lev; // levels of harmonics

  void go (multi_curve* crv); // do fft on a multiple segment bezier curve

  float f; // audio frequency used for fft

  float x, dx; // on bezier curve, or theta/dtheta on sine.

  fft ();
  ~fft ();

  // ui
  arrow_button ab_fold;
  label l_title;
  void setup ();
  void update ();
  int handle_input ();
  void draw (); 

  void clicked (button& b);
  int folded ();


};

extern fft fft0;
#endif
