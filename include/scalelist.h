/*
* scalelist.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __SCALELIST
#define __SCALELIST

#include <vector>
#include <string>

struct tokenizer;

struct scale {
  std::string name;
  std::string notes;
  int num_notes;
  scale () : num_notes(0) {}
  bool operator== (const scale& s) { return name == s.name;}
};

struct scalelist {
  std::vector<scale> scales;
  scalelist ();
  ~scalelist ();
  void load ();
  void save ();
  int add (tokenizer& tz);
  int remove (const std::string& name);
  int get (const std::string& str, scale& s);
  void ls ();
};

#endif



