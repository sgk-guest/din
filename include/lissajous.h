/*
* lissajous.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __LISSAJOUS
#define __LISSAJOUS

#include "spinner.h"
#include "plugin.h"

struct lissajous : plugin {

  float center_x, center_y;

	float x_speed, y_speed;
	int num_points; 
  
  float theta;
  float dtheta;

	spinner<float> sp_x_speed, sp_y_speed;
  spinner<int> sp_num_points;

  lissajous ();
  ~lissajous ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();

};

#endif



