/*
* constant_radius.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __CONSTANT_RADIUS
#define __CONSTANT_RADIUS

/*
* constant_radius.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#include "funktion.h"

struct constant_radius : funktion {
  float operator() (float a, float ea) {
    return 1.0f; // for standard circle
  }
};

#endif



