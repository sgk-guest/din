/*
* midi_in.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __midi_in
#define __midi_in

#include "RtMidi.h"
#include <vector>
#include <string>

struct midi_in {

	RtMidiIn rt;

	int num_ports;
	int input_port;
	int available;
	std::vector<std::string> names;

	midi_in ();
	void probe ();
	void open ();
	void open (int _input_port);
	void handle_input ();
	std::string get_name (int i) {
		if (i > -1 && i < num_ports) {
			return names[i];
		}
		return "?";
	}

};

void run_midi_cmd (const std::string& cmd, std::vector<unsigned char>& args);

extern midi_in midiin;

#endif




