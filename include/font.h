/*
* font.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef _FONT
#define _FONT

#include <string>
#include <fstream>
#include <map>

#include "dingl.h"
#include "glyph.h"

struct font {

  std::string fname;
  std::string name;

  int nchars;
  
  int max_char_width, max_char_height;
  
  int avg_char_width, avg_char_height;

  int x_cell_size, y_cell_size;
  
  int lift;
    
  int charspc, wordspc;

  int headroom; // space above char

  std::map <char, glyph> characters; // vector desc of chars

  std::map < char, std::map<char, int> > kern; // char-char kerning

  int modified; // if modified will save

  font (const std::string& fn);
  ~font ();
  void load (const std::string& fn);
  void load (std::ifstream& file);
  void save ();

  int char_width (char c);
  int char_height (char c);

  void draw_char (char c, int x, int y, int z = 0);
  const std::string& filename () const { return fname;}

  const std::map<char, glyph>& get_chars ();
  void set_chars (const std::map<char, glyph>& chars);

	void calc_line_height ();

#ifdef __SVG_OUT__
  std::ofstream svg;
  void write_char (char c, int x, int y, int z = 0);
#endif

#ifdef __PLOTTER_OUT__
	std::ofstream hpgl;
	void plot_char (char c, int x, int y, int z = 0);
#endif
};

extern font fnt;
extern int line_height;

inline void draw_char (char c, int x, int y, int z = 0) {
  fnt.draw_char (c, x, y, z);
}

int draw_string (const std::string& s, int x, int y, int z = 0);

#ifdef __SVG_OUT__
	int write_string (const std::string& s, int x, int y, int z = 0);
#endif

#ifdef __PLOTTER_OUT__
	int plot_string (const std::string& s, int x, int y, int z = 0);
#endif

int get_char_width (const std::string& s);
int get_max_char_height (const std::string& s);

#endif
