/*
* binaural_drones.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __BINAURAL_DRONES__
#define __BINAURAL_DRONES__

#include <vector>
#include "binaural_drone.h"
#include "ui.h"
#include "instrument.h"
#include "curve_editor.h"
#include "help.h"

struct fade_info {
	std::string name;
	int active;
	int after;
	solver sol;
	float xt, dxt;
	float xu, xui;
	float duration;

	fade_info ();
	void start (const std::string& _name = "op");
	int eval (float* out);
	void set_duration (float t);
	void complete ();
	void abort ();

};

struct i_binaural_drones;
struct waveform_listener : curve_listener {
	i_binaural_drones* sb;
	void edited (curve_editor* ed, int i);
};

struct i_binaural_drones : instrument {

	std::vector<binaural_drone*> binaural_drones;
	int num_binaural_drones;

	multi_curve wav; // waveform shared by all binaural drones
	curve_editor waved; // waveform editor
	waveform_listener wavlis; // waveform edit listener

	// binaural drone fading
	multi_curve fdrcrv; // fading curve
	fade_info pitch_fader, vol_fader;

	float master_volume; // split among all binaural drones

	float modulation_amount; 

	float starting_pitch; // starting pitch from where binaural drones are made
	float separation; // in hz between l and r of a binaural drone
	int pairs; // number of binaural drones
	float spacing; // inter drone spacing 

  enum {START_PITCH=0, FROM_SCALE};
  int keynote;
	int close_octave; // close octave when making binaural drones on notes of scale?
  int change_key_note (int dir);
	int resize_separation;

	// justification
  int just;

	help hlp;

	i_binaural_drones ();
	~i_binaural_drones ();

	int add (float _lhz, float _rhz, float _vol, int just, float sep, float lx = 0, float rx = 0);
	void remove (int w);
	void sync (int n, const std::string& lst); 
	void list (); 
	void load (); 
	void save (); 
	void update_players (multi_curve& mx); 
	int handle_input ();
  int change_justification (int dir);
	int render_audio (float* L, float* R);
	void enter ();

	int abort;
	int busy ();
	int aborted ();

	std::string get_sel_vol ();

};

extern i_binaural_drones binaural_drones0;

#endif
