/*
* ball_ops.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __ballops__
#define __ballops__

#include "alarm.h"
#include "random.h"

struct ball;

struct ball_op {
	static const int NUM_OPS = 5; // change when adding new op!
	static const char* names [];
	static float TRIGGERT;
	alarm_t alarm; 
	virtual int eval (ball* b = 0);
};


struct turn : ball_op {
	static float CLOCKWISE, ANTI_CLOCKWISE;
	rnd<float> rd;
	turn ();
	int eval (ball* b);
};

struct speed : ball_op {
	static float BRAKE, ACCELERATE;
	rnd<float> rd;
	float max;
	speed ();
	int eval (ball* b);
};

struct teleport : ball_op {
	static float MAX_RADIUS;
	rnd<float> rd;
	float radius;
	teleport ();
	int eval (ball* b);
};

struct Clone : ball_op {
  int n;
  int max;
  float offset;
  int clone_can_clone;
  static int max_balls;
  Clone ();
  int eval (ball* b);
};

struct Transform : ball_op {
	static int rules [3];
	Transform ();
	int eval (ball* b);
};

#endif
