/*
* instrument.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __INSTRUMENT
#define __INSTRUMENT

#include "scale_info.h"
#include "octave_shift_data.h"

struct instrument : ui {
	scale_info scaleinfo;
	octave_shift_data osd;
	virtual void update_waveform () {}
	virtual void update_attack () {}
	virtual void update_decay () {}
	virtual void load_scale (int dummy = 0) {}
};
#endif



