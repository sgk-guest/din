/*
* din.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __din__
#define __din__

#include "curve_editor.h"
#include "listeners.h"
#include "range.h"
#include "ui.h"
#include "audio.h"
#include "play.h"
#include "oscilloscope.h"
#include "curve_library.h"
#include "beat2value.h"
#include "help.h"
#include "phrasor.h"
#include "input.h"
#include "command.h"
#include "globals.h"
#include "random.h"
#include "drone.h"
#include "gravity.h"
#include "mesh.h"
#include "scale_info.h"
#include "instrument.h"
#include "alarm.h"
#include "textboard.h"
#include "spinner.h"
#include <list>

extern int BOTTOM;
extern float VOLUME;
extern float DELTA_VOLUME;

//
// applies to microtonal-keyboard only
// in the old days, din was just the microtonal-keyboard
//

struct din_info {

  struct scroll_t {
    double rept; // key repeat time
    int rate; // scrolls per second
    int dx, dy; // x & y amount per scroll
    scroll_t () : rate (80), dx(15), dy(1) {}
    void calc_repeat_time () {if (rate) rept = 1./ rate;}
  };
  scroll_t scroll;

  int height; // din board height
  int gater; // gater?
  int delay; // delay?
  int compress; // compressor?
  int voice; // lead voice?
  int anchor; // draw drone anchor?
	int vel; // draw velocity vectors of drones?
	int accel; // draw acceleration vectors of drones?
  gravity_t gravity; // gravity vector

	// drone bounce
	struct bounce_t {
		enum {FORWARD=0, BACK};
		int style;
		int n;
		int speed;
		bounce_t () : style (FORWARD), n(1), speed (100) {}
	} bounce;


	enum {DRONE_MESH, DRONE_PENDULUM};
	int create_this;

  // drone mesh
  int rows, cols;
	struct mesh_vars_t {
		int order;
		int point;
		float duration;
		int sync;
	} mesh_vars;

	// drone pendulum
	struct drone_pend_t {
		int spacing;
		float start_depth, end_depth;
		float start_bpm, end_bpm;
		enum {AM, FM};
		int orient;
	} drone_pend;


	// show pitch & volume
	struct s_show_pitch_volume {
		int board;
		int drones;
		s_show_pitch_volume () { board = drones = 0;}
	} show_pitch_volume;

	struct s_time_range {

		float min, max;
		rnd<float> rd;

		s_time_range (float _min = 1.0f, float _max = 1.0f) : min(_min), max(_max), rd (min, max) {}

		float set_min (float m) {
			if (m < 0) return min; 
			else if (m > max) max = m;
			min = m;
			rd = rnd<float> (min, max);
			return min;
		}

		float set_max (float m) {
			if (m < min) max = min;
			else max = m;
			rd = rnd<float> (min, max);
			return max;
		}

		float operator() () { return rd(); }

	} drone_rise_time, drone_fall_time;

	struct snap_t {
		enum {FREE=0, SLIDE, LOCK, MIRROR};
		int style;
		float left, right;
	} snap;

	int set_unset_toggle; // for drone.mod_afx_vel and drone.snap

	int sel_range;
	int mark_sel_range;

	static const char* cns_styles[];
	int change_note_style;

	struct dist_t {
		int vol;
		int pitch;
		int pix;
		static const int PIX_PER_LEVEL = 5;
		dist_t () : vol(0), pitch(0), pix (PIX_PER_LEVEL) {}
	} dist;

  din_info ();
  void save ();

};

// for deltas of FM/AM
struct delta_t {
  float depth, min_depth;
  float bpm, min_bpm;
  delta_t (float _depth = 1, float _bpm = 1);
};

struct checkbutton;

// for adding mesh drone over time
struct add_mesh_drone : alarm_t {
	int i, j;
	int p, drop;
	void reset () {
		i = j = p = drop = 0;
		stop ();
	}
	add_mesh_drone () { reset (); }
};

struct din : instrument, scale_listener, region_listener {

  // waveform
  //
  multi_curve wave;
  curve_editor waved;
  wave_listener wavlis;
  solver wavsol;
  play wavplay;

  // tone ranges
  //
  std::vector<range> ranges;
  int num_ranges, last_range, current_range;
  range *firstr, *lastr;

  float step; // represents pitch (see note.h)
  float delta; // 0 - left of range, 1 - right of range

  std::string pitch_volume_info;

  int visl, visr; // visible ranges
  void find_visible_ranges (int dir = 0);

  void create_ranges (int n);
  int load_ranges ();
  void setup_ranges (int last_num_octaves, int load = 1);
  void calc_all_range_notes ();
	void calc_added_range_notes (int last_num_octaves, int first_added_range);
	void save_ranges ();
	void reset_all_ranges ();

  int range_right_changed (int r, int dx, int mr);
  int range_left_changed (int r, int dx, int mr);
  void set_range_width (int r, int w);
  void set_range_width (int s, int t, int w);
	void set_range_height (int r, int h);
	void set_range_height (int s, int t, int h);
	void init_range_mod (int s, int t);
  void default_range_to_all (int h = 0);
  void default_range_to_selected (int h = 0);
  void selected_range_to_all (int h = 0);
	void pause_resume_ran_mod ();
	void set_ran_mod (int w);
	void toggle_ran_mod ();
	void update_range_mod_solvers (int type, multi_curve& mix);
	void update_drone_mod_solvers (int type, multi_curve& mix);
	void all_ranges_width_changed ();
	void all_ranges_height_changed ();

  int find_range (int x, int r);
  void find_current_range ();

  int find_tone_and_volume ();
  void tuning_changed ();
  float get_note_value (const std::string& s);

  // microtonal_keyboard
  //
  box<int> win;
  void scroll (int dx, int dy, int warp_mouse = 1);
  int win_mousex, win_mousey; // mouse pos in win
	int prev_win_mousex, prev_win_mousey;
	int tonex, toney;
  int delta_mousex, delta_mousey;
  void calc_win_mouse ();

  void set_key_to_pitch_at_cursor ();
  void mouse2tonic ();

  void height_changed (int r, int dh);

  //
  // drones
  //

  int num_drones;
  std::list<drone*> drones;

  std::vector<drone*> selected_drones;
	int num_selected_drones;

	std::vector<drone*> browsed_drones;
	int num_browsed_drones;
	int browsed_drone;
	int last_browseable_drone;
	void select_all_browsed_drones (int bd);
	void delete_from_browsed_drones (drone* d);


  std::list<drone*> risers; // rising drones
  std::list<drone*> fallers; // falling drones
  std::list<drone*> launchers; // launch new drones
  std::list<drone*> trackers; // launchers that track other drones
  std::list<drone*> attractors; // drones that attract other drones
  std::list<drone*> gravitated; // drones that move under gravity
	std::list<drone*> satellites; // launched drones due to orbit another drone

  float drone_master_volume;
  void update_drone_master_volume (float delta);
	void mute_drones ();

  // drone waveform
  multi_curve drone_wave; 
  curve_editor droneed;
  wave_listener dronelis;

  // drone adding
  int adding_drones;
  void toggle_adding_drones ();
  drone* add_drone (int wx, int wy);

  // drone move
  int moving_drones;
  void start_moving_drones ();
  void toggle_moving_drones ();
  void set_moving_drones (int what);

  // drone set
  void set_drone (drone& d, int wx, int wy, int init = 0, int shift = 0, int ctrl = 0);

  // drone selection
  //
	void clear_selected_drones ();
  int select_all_drones ();
  void invert_selected_drones ();
  void add_drone_to_selection (drone* d);
  void remove_drone_from_selection (drone* d);
  std::string get_selected_drones ();
  void pick_drone ();
	void browse_drone (int db);
  int is_drone_hit (drone& di, const box<int>& _rgn);
  void calc_selector_range (const box<int>& _rgn, int& sell, int& selr);
	void find_selected_drones (const box<int>& _rgn);
  void print_selected_drones ();
	int select_launchers ();
	void select_launched ();

	// drone xform

	enum {NO_CHANGE, ALIGN, PERP1, PERP2}; // what happens to velocity vector of rotated drones
	int vel_effect;

	float cenx, ceny;
	std::vector< point<float> > rvec, svec;
	int update_xform_params, update_rotation_vectors, update_scale_vectors;
	float angle;
	point<float> scl;
	void calc_xform_params (int n);
	void calc_xform_vectors (std::vector<point<float> >& V, int n, int& flag);
	void rotate_selected_drones (float da);
	void scale_selected_drones (float ds);

  void delete_drone (drone& ds);
	int delete_all_drones ();
	void delete_selected_drones ();

  // drone load & save
  void load_drones ();
  void save_drones ();

  // drone tone
  void update_drone_tone ();
  void update_drone_x (int i, int j);
  void update_drone_players ();
  void update_drone_solvers (multi_curve& crv);
  void update_drone_ranges ();
  void update_drone_anchors ();
	void reposition_all_drones ();
	void reposition_drones (int r1, int r2);
	void reposition_drones (int r);

  // drone orbit
  void orbit_selected_drones ();
	// void orbit_reciprocal ();
  void attract_drones ();
  void select_attractors ();
  void select_attractees ();
  void remove_attractee (drone* d);
  void change_drone_accel (spinner<float>& s);
	void change_drone_vel (spinner<float>& s);
	void rotate_drone_vel (spinner<int>& s);
	void reset_drone_vel ();

	// modulation
  enum {MODULATE_DRONES, MODULATE_VOICE}; // target
	int modulate_what;
	enum {AM, FM}; // type
  void change_drone_bpm (int what, float delta);
	void change_drone_bpm (int what, spinner<float>& s);
  void change_drone_depth (int what, float delta);
	void change_drone_depth (int what, spinner<float>& s);

  void change_drone_trail_points (spinner<int>& s);
  void change_drone_handle_size (spinner<int>& s);
  void modulate_drones ();
	void calc_drone_handles (int r1, int r2);

	void modulate_ranges ();


  int rising, falling;
  void rise_drones ();
  void fall_drones ();

	void trail_drones ();

	void sync_drones ();

	void freeze_drones ();
	void thaw_drones ();
	void toggle_freeze_drones ();

	void snap_drones (int v);
	void mod_afx_vel (int v);

  // launchers
  //
	int auto_select_launched;
  void launch_drones ();
	void make_launcher (drone* pd);
  void make_launchers ();
  void destroy_launchers ();
  void toggle_launchers ();
  void change_drones_per_min (spinner<int>& s);

  void make_trackers ();
  void remove_tracker (drone* ptd);
  void track_drones ();
  void select_tracked_drones ();

	void set_targets ();
	void clear_targets ();
	void remove_drone_from_targets (drone* T);
	void carry_satellites_to_orbit ();
	void kill_old_drones ();
	void change_drone_lifetime (spinner<float>& s);
	void change_orbit_insertion_time (spinner<float>& s);

  // gravity
  //
  void set_drones_under_gravity ();
  void move_drones_under_gravity ();
  void set_gravity_to_track_drone ();

  // mesh
  //
  int create_mesh;
  void toggle_create_mesh ();
  void create_drone_mesh ();
	void stop_creating_mesh ();
  int num_meshes;
  std::list<mesh> meshes;
  void remove_drone_from_mesh (drone* pd);
	void remove_drone_from_pre_mesh (drone* d);
	add_mesh_drone amd;

	// drone pendulum
	int create_drone_pend;
	void create_drone_pendulum ();
	void toggle_create_drone_pendulum ();
	void stop_creating_drone_pendulum ();

	void toggle_create_this ();

	textboard tb_hz_vol;
  void draw_drones ();
  void set_drone_volume (int i, float v);
  drone* get_drone (int id);

  // modulation
  //
	// voice
	float fm_step, fm_depth;
  float am_depth, am_vol;
  beat2value fm, am;

	// voice + drones
	void change_depth (int type, float d);
	void change_bpm (beat2value& which, float amt);
	void change__bpm (int type, beat2value& bv2, float amt);
	void change__depth (int drone_arg1, float amt1, int voice_arg2, float amt2);

  // am & fm modulation editors
  curve_editor moded;
  beat2value_listener fmlis, amlis;

	// deltas
  delta_t am_delta, fm_delta, gater_delta;
  delta_t dam_delta, dfm_delta;
  delta_t* p_am_delta;
  delta_t os_delta; 
  void raise_delta (float& d, float dd, const std::string& mesg);
  void lower_delta (float& d, float dd, const std::string& mesg, float minn = 1);

	// gater
	//
	beat2value gatr;
  int calc_am_fm_gater ();

  // gater editor
  curve_editor gated;
  curve_library gatlib;
  beat2value_listener gatrlis;

	float fdr_gater_prev_amount;

  // phrasor - record/play/scratch a phrase
  phrasor phrasor0;
  void do_phrase_recording ();
  int finish_phrase_recording ();
  void clear_all_phrases (int quiet = 0);

  din (cmdlist& cmdlst);
  ~din ();
  void setup ();

  int handle_input ();

	void tonic_changed ();
	void scale_changed ();
	void scale_loaded ();
  void load_scale (int _load_drones = 0, int _load_ranges = 1);
  void save_scale ();

  void enter ();
  void bg (); // background execution
  void draw ();

  // din saved info
  din_info dinfo;

  // help
  help helptext;

  void notate_ranges ();

  void sample_rate_changed ();
  void samples_per_channel_changed ();

  int render_audio (float* out0, float* out1);

  // modulation
  void switch_modulation (); // voice or drones
  void prep_modulate (int op_code);
  void change_am_depth (float d);
  void change_fm_depth (float d);
  void change_am_bpm (float b);
  void change_fm_bpm (float b);

  // OpenGL 
  int* dvap; int n_dvap;
  int* dap; int n_dap;
  int gl_pts[8];

  // ui
  void toggle_this (int& what, checkbutton& cb);

	box<int> rgn;
	void region_begin ();
	void region_end ();
	void region_abort ();
	const box<int>& region_update ();

	void change_range_note (int i, int d);
	scale_info* ptr_scaleinfo;



	void draw_vol_levs ();
	void draw_pitch_levs ();

};

#define NUM_ORDERERS 7
#define LAST_ORDERER (NUM_ORDERERS - 1) 
struct mkb_selector_t : box_selector, mesh_data {
	ascending_rows_orderer of_asc;
	ascending_cols_orderer of_asc_cols;
	descending_rows_orderer of_desc;
	descending_cols_orderer of_desc_cols;
	random_orderer of_rnd;
	proximity_orderer of_prox_near, of_prox_far;
	orderer_t* orderers [NUM_ORDERERS];
	mkb_selector_t ();
	void draw (const box<int>& region);
};

extern mkb_selector_t mkb_selector; 
extern din din0;
#endif
