/*
* button.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __button
#define __button

#include "widget.h"
#include "label.h"

#include <string>

struct button;

struct click_listener {
  virtual void clicked (button& b) = 0;
};

struct button : label {

  int click;

  int click_repeat;
  double start_time, repeat_time;
	double first_repeat_time, subsequent_repeat_time;

  click_listener* lsnr;

  button ();

  int handle_input ();
  void draw ();

  void set_listener (click_listener* l) {lsnr = l;}

  void call_listener () {if (lsnr) lsnr->clicked (*this);}

};

#endif
