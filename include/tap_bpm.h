/*
* tap_bpm.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __tap_bpm
#define __tap_bpm

#include "widget.h"

#include <string>

struct tap_display : widget {
  box<int> inner;
  float startt, flasht, tapt;
  int lmb_clicked;
  int flash;
  tap_display (int sz);
  void set_pos (int x, int y);
  int handle_input ();
  void draw ();
  float bpm;
  void calc_bpm ();
  void set_bpm (float b, int reset = 0);
  change_listener<tap_display>* lis;
  void set_listener (change_listener<tap_display>* l);
};

#endif


