/*
* utils.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/
#ifndef _UTILS
#define _UTILS

#define SIGN(x) ((x) < 0 ? -1 : (x) > 0)

template <typename T> inline bool inrange (const T& min, const T& val, const T& max) {
  return ((val >= min) && (val <= max));
}

template<typename T> inline int clamp (const T& lo, T& val, const T& hi) {
  // ensures lo <= val <= hi
  if (val < lo) {
    val = lo; 
    return -1;
  } else if (val > hi) {
    val = hi; 
    return 1;
  } 
  return 0;
}

template<typename T> inline T& wrap (const T& lo, T& val, const T& hi) {
  // val stays between lo and hi but wraps
  if (val < lo) val = hi; else if (val > hi) val = lo;
  return val;
}

template <typename T> int equals (T b, T a, T e = 0.0001) {
	T ba = b - a;
	T ba2 = ba * ba;
	T e2 = e * e;
	if (ba2 < e2) return 1; else return 0;
}

struct item_op {
	virtual int operator()(int) const = 0;
};

struct sel : item_op {
	int operator()(int) const { return 1;}
};

struct desel : item_op {
	int operator()(int) const { return 0;}
};

struct togg : item_op {
	int operator()(int i) const { return !i;}
};

void multiply (float* out, float* mul, int n); // multiply n muls with n outs; store in out
void multiply (float* out, int n, float d); // multiply n outs with d; store in out
void fill (float* buf, float s, float e, int n); // fill buf with values interpolated from s to e
void tween (float* buf1, float* buf2, int n, float amount); // interpolate buf2 -> buf1 by amount; store in buf1
void tween (float* buf1, float* buf2, int n, float* pamount); // interpolate buf2 -> buf1 by amount array; store in buf1

#endif
