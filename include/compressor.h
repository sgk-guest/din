/*
* compressor.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __compressor
#define __compressor

#include "curve_editor.h"
#include "multi_curve.h"
#include "solver.h"

struct compressor;

struct compressor_listener : curve_listener {
  compressor& c;
  compressor_listener (compressor& cc): c(cc) {}
  void edited (curve_editor* ed, int i);
};

struct compressor { // dynamic range compression using a bezier curve based transfer function

  std::string fname;
  multi_curve crv;
  solver apply;

  compressor_listener lis;
  compressor (const std::string& f);
  ~compressor ();

};

#endif
