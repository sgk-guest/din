/*
* phrasor.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __phrasor
#define __phrasor

#include <vector>
#include "box.h"
#include "point.h"

struct phrasor {

  std::vector < point<int> > data;

  int size, last;

  float last_1;
  float amount; // cur as [0,1]

  int state;
  enum {stopped, recording, playing, paused};

  int cur;

  phrasor () {
    clear ();
  }

  void add (point<int>& p) {
    data.push_back (p);
  }

  void get (int& win_mousex, int& win_mousey) {
    point<int>& pt = data[cur];
    win_mousex = pt.x;
    win_mousey = pt.y;
  }
  
  void clear () {
    state = stopped;
    data.clear ();
    cur = 0;
    size = 0; 
    last = -1; 
    last_1 = 0;
    amount = 0;
  }

  void play ();

  void draw ();
  void draw_marker (int x, int y);

  int validate ();
  int next ();
  void set_cur (float amt);

};

#endif



