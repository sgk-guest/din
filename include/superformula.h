/*
* superformula.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __SUPERFORMULA
#define __SUPERFORMULA

#include "spinner.h"
#include "plugin.h"

struct superformula : plugin {

  float center_x, center_y;

	float m, n1, n2, n3;

	int num_points; 
  
  float theta;
  float dtheta;

	spinner<float> sp_m, sp_n1, sp_n2, sp_n3;
  spinner<int> sp_num_points;

  superformula ();
  ~superformula ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();

};

#endif



