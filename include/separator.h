/*
* separator.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __separator
#define __separator

#include <string>
#include "widget.h"

struct separator : widget {

	enum {horizontal = 1, vertical = 2};
	int type;

	separator (int _type = horizontal) {
		type = _type;
	}

	void set_extents (int sz) {
		if (type == horizontal) 
			widget::set_extents (extents.left, extents.bottom, extents.left + sz, extents.bottom + 1);
		else
			widget::set_extents (extents.left, extents.bottom, extents.left + 1, extents.bottom + sz); 
	}

	void draw ();
	~separator () {}

};

#endif



