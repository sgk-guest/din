/*
* plus_button.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __plus_button
#define __plus_button

#include "button.h"

struct plus_button : button {
  int size;
  plus_button (int sz = 12) {
    set_size (sz);
  }
  void set_size (int sz) {
    size = sz;
    set_extents (posx, posy, posx + size, posy + size);
  }
	void update () {
		set_size (size);
	}
  void draw ();
};

#endif



