/*
* chrono.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __chrono
#define __chrono

#include <ctime>
#include <time.h>

#include <boost/date_time/posix_time/posix_time.hpp>

struct audio_clock {
  double secs;
	double delta_secs;
  double ticks;
  int delta_ticks;
  audio_clock ();
	~audio_clock ();
  audio_clock& operator++ ();
};

struct ui_clock { // uses boost
  boost::posix_time::ptime start, now; // start time (set at reset), current time
  boost::posix_time::time_duration elapsed;
  double secs_; // elapsed time since start in seconds
  ui_clock ();
	~ui_clock ();
  void find_elapsed ();
  void reset ();
  void tick ();
  double operator() ();
};

extern ui_clock ui_clk;
extern audio_clock clk;

#endif
