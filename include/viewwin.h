/*
* viewwin.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __viewwin
#define __viewwin

#include "box.h"

struct viewport { // opengl viewport

  int width, height;
	float width_change, height_change;
  int xmax, ymax;
  float xmax_1, ymax_1; // 1.0 / xmax , 1.0 / ymax

  // for constant size handle of vertex/tangent in curve editors
  //
  static int handle_radius;
  static float handle_factor;

  viewport ();
	~viewport ();
  void operator() (int width, int height);
  void calc (int w, int h);
  bool inside (int x, int y);

};

struct window : box<float> { // curve editor

  float mousex, mousey; // mouse in window co-ords
  float mousex_prev, mousey_prev; // prev mouse in window co-ords
  int prev_mousex, prev_mousey; // prev mouse in mouse co-ords

  void locate_mouse (); // find mouse in window co-ords
  void update_mouse (); // update mouse in window co-ords
	int diff_mouse (float& dx, float& dy); // true if mouse now is different from mouse prev; in window co-ords

  float vwx, vwy; // for view-win transform; to locate mouse in window
  void calc_viewwin ();

  static int PAN_RATE, ZOOM_RATE; // num pan, zooms per second
  static float PAN_AMOUNT, ZOOM_AMOUNT; // pan, zoom amount per key press
  static double PAN_REPEAT, ZOOM_REPEAT; // key repeat time
  static void calc_repeats ();

  float pan_x, pan_y;
  void calc_panxy ();
  void panx (int dir);
  void pany (int dir);

  float aspect_ratio;

  window ();
  window (float l, float b, float r, float t);
  void set (float l, float b, float r, float t);
  void get (float& l, float& b, float& r, float& t);

  void zoom (int dir);

  void project ();

  float handle_radius, handle_radius2;
  float sus_handle_radius;
  void calc_handle_radius ();

  void calc ();

};

void view2win (int vx, int vy, float& wx, float& wy, viewport& view, window& win);
void win2view (float wx, float wy, int& vx, int& vy, window& win, viewport& view);

extern viewport view;
#endif



