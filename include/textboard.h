/*
* textboard.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __textboard
#define __textboard

#include <list>
#include <string>
#include <fstream>

#include "ui.h"

struct text {

  std::string txt; // actual text

  float wx, wy; // window co-ords

  float r, g, b; // color

  enum {permanent, temporary};
  int state;

	// hline - label on horizontal line, vline = label on vertical line
  enum {hline=0, vline=1, normal};
  int type; 

  int vx, vy; // view co-ords

  text (const std::string& t, float xx = 0, float yy = 0, float rr=0.25, float gg=0.25, float bb=0.25, int s = text::temporary, int y = text::normal, float vxx = 0, float vyy = 0) 
  : txt(t), wx(xx), wy (yy), r(rr), g(gg), b(bb), state (s), type (y) , vx (vxx), vy (vyy) {}

	bool operator== (const text& t) {return txt == t.txt;}

};

struct basic_editor;
struct mondrian;

struct textboard {
  std::list<text> texts;
  void draw (int shapeform = 0);
  void draw_line (int x1, int y1, int x2, int y2);
  void add (const text& t) { texts.push_back (t);}
  void clear ();
  void refresh (basic_editor* b, float dwx = 0, float dwy = 0, float dvx = 0, float dvy = 0);
  void refresh (mondrian* m);
  void load (std::ifstream& file);
  void save (std::ofstream& file);
};

#endif
