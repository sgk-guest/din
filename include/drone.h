/*
* drone.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __drone__
#define __drone__

#include "fader.h"
#include "solver.h"
#include "play.h"
#include "trail.h"
#include "alarm.h"
#include "modulator.h"
#include <string>

extern int DRONE_HANDLE_SIZE;
extern int TRAIL_LENGTH;
extern int BOTTOM;
extern float DELTA_VOLUME;

struct din;
struct drone;

struct attractee {
	int id;
	drone* d;
	attractee (int _id = -1, drone* _d = 0) {id = _id; d = _d;}
  bool operator== (const attractee& ae) {return (d == ae.d);}
};

struct drone {

  static int UID; // unique drone id
	int id; // id of this drone

  play player; // player for this drone
  solver sol; // solver for this drone

  float step; // determines pitch (see note.h)
  float vol; // actual volume
	
  int range; // range number on microtonal-keyboard
  float pos; // position in range (0 - left, 1 - right, can go < 0 & > 1 too)

  int sel; // selected?

  // visual
  //
  int cx, cy; // unmodulated position
  int x, y; // current position
	int sx; // snapped x (used for note snap)
	int snap;
  int dy; // height from microtonal-keyboard bottom
  int xv, yv; // position of velocity vector tip
  int xa, ya; // position of acceleration vector tip
  float r, g, b; // color
	trail_t trail; // trail points

	box<int> handle; // for picking
	int handle_size;

	// states
  enum {DEAD=0, RISING, FALLING, ACTIVE}; 
  int state; // current state
	int frozen; // frozen?
	double froze_at; // time when frozen
	fader fdr; // for rising/falling -> fade in/out

  modulator mod; // modulation

	// orbitry & rocketry
	//

	int attractor; // attractor?
	int orbiting; // orbiting?
	std::list<attractee> attractees; // list of drones attracted by this drone
	float V; // speed 
  float vx, vy; // velocity vector
	float v_mult; // multiplier for velocity vector used in orbit insertion
	float A; // acceleration
	float ax, ay; // acceleration unit vector
	int xi, yi; // interim position (see update_attractees)

  int gravity; // accelerated by gravity?

  int bounces; // number of bounces from bottom of microtonal-keyboard

  int launcher; // launches drones?
  alarm_t launch_every; // alarm time in seconds
  int dpm; // drones per minute
	int tracking; // is the launcher tracking a drone?
	drone* tracked_drone; // the tracked drone

	// for launched drone
	static double LIFE_TIME, INSERT_TIME;
	double birth; // time at birth
	double life; // time until death
	double insert; // time to insert into orbit
	drone* launched_by; 

	// targets for launchers to launch drones to
	std::vector<drone*> targets; 
	int cur_target;
	int num_targets;
	void clear_targets ();

	drone* target;

	static int ref; // for debug

	drone (int _bottom = 0);
	~drone();

  void set_xy (int x, int y);
	void set_center (int _cx, int _cy);
	void move_center ();

	void change_depth (int i, int what, float delta);
  void change_bpm (int i, int what, float delta);
	void change_bpm (mod_params& mp, float delta);

	enum {INTERPOLATE=1, EMPLACE};
	int update_pv;
	void update_pitch_volume (float dmv);

	void calc_handle ();

	void handle_time_pass ();

};

#endif
