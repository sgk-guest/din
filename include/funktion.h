/*
* funktion.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __FUNKTION
#define __FUNKTION

/*
* funktion.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


struct funktion {
  virtual float operator() (float ra = 0, float rea = 0) = 0;
};

#endif



