/*
* curve_library.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __CURVE_LIBRARY
#define __CURVE_LIBRARY

#include <string>
#include <vector>
#include "multi_curve.h"

struct curve_editor;
struct curve_library;

struct curve_library {
  // a library of multi curves loaded from file

  std::string filename;
  std::vector <multi_curve> curves;
  std::vector <float> sustains;
  int cur;
  int has_sustain; // only curve library of keyboard-keyboard attack has sustain
  curve_library (const std::string& fname, int _has_sustain = 0);
  ~curve_library ();
  void load (const std::string& fname);
  void add (const multi_curve& m);
  void del ();
  void replace (const multi_curve& m);
  void insert (const multi_curve& m);
  const multi_curve& prev ();
  const multi_curve& next ();
  void move (int i);
  int num_curves () { return (int) curves.size (); }
  void save ();
};
#endif



