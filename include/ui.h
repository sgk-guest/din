/*
* ui.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __UI
#define __UI

#include <string>

struct ui { // or screen

	std::string name; 
	int ed; // editor?
	int prev_mousex, prev_mousey; // mouse in device space co-ords

  virtual void enter ();
  virtual void leave ();
  virtual int handle_input() {return 1;}
  virtual void calc_win_mouse () {}
  virtual void draw() {}
  virtual void bg () {}
  ui () { prev_mousex = prev_mousey = -1; ed = 0;}
  virtual ~ui () {}

};
#endif



