/*
* curve_picker.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __curve_picker
#define __curve_picker

#include "label.h"
#include "options_list.h"

struct curve_picker_t : widget, click_listener, option_listener {

  label l_title;
  button b_pick, b_cancel;

  options_list ol_components;
  int id; // of picked component
	int n_1; // n components - 1

  void setup ();
  void update ();

  int handle_input ();

  void show ();
  void hide ();

  box<int> bg;
  void calc_bg ();
  void draw ();

  void clicked (button& b);
  void picked (label& l, int dir);

  ~curve_picker_t ();

};

extern curve_picker_t curve_picker;

#endif
