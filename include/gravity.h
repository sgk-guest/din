/*
* gravity.h
* DIN Is Noise is copyright (c) 2006-2019 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __GRAVITY
#define __GRAVITY

#include "widget.h"
#include "point.h"

struct drone;
struct gravity_t : widget {

  int handle_size;
  point<int> base;
  point<int> tip;
  point<int> base2tip;
  point<int> p_base2tip;
  point<int> bottomleft, topright;
  point<int> textpos;

  int hitt;
  enum {NEXT_TO_NOTHING = -1, NOTHING, BASE, TIP};
  int hit (const point<int>& p, int mx, int my);
  int lmb_clicked;

  float strength, strength0;
  float gx, gy;

  drone* tracked_drone;
  double mag0, mag;

	int keep_size;

  void track ();

  gravity_t ();
  void load (std::ifstream& file);
  void save (std::ofstream& file);
  int handle_input ();
  void set (point<int>& p, int mx, int my, int calc_mag = 1);
  void calc (int calc_mag = 1);
	void reset (int keep_size = 0);

  int gl_base [8];
  int gl_arrow[16];
  void draw ();

};
#endif



